### Данная программа позволяет: ###
* Конвертировать дат файл в текстовые(или несколько тхт) или базу данных по C++ структуре(которую для серверной части легко можно скопировать)
* Собирать их обратно
* Контролировать изменения в файлах и конвертировать только необходимое

### Поддерживаемые платформы: ###
* Windows
* Mac OS

### Build requirements: ###
* Qt 5.x

### Контакты: ###
* nikita.besshaposhnikov@gmail.com - Никита