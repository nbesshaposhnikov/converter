#ifndef BASICTYPES_H
#define BASICTYPES_H

namespace Converter {

    enum ConvertType
    {
        _ConvNone = -1,
        Dat = 0,
        Txt  = 1,
        SQL  = 2,
        Excel  = 3,
        SourceNum = 4
    };

    enum DataType
    {
        _none,
        _char,
        _uchar,
        _xchar,
        _char_bitfield,
        _char_calcfield,
        _short,
        _ushort,
        _xshort,
        _short_bitfield,
        _short_calcfield,
        _int,
        _uint,
        _xint,
        _int_bitfield,
        _int_calcfield,
        _longlongint,
        _uint64,
        _xint64,
        _int64_bitfield,
        _int64_calcfield,
        _float,
        _double,
        _blob,
        _string,
        _struct,
        _enum
   };
}


#endif // BASICTYPES_H
