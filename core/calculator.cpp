#include "calculator.h"

using namespace Converter;

int priority(QChar c)
{
    if(c=='^')
        return 2;
    else if(c=='*'||c=='/')
        return 1;
    else
        return 0;

    return 0;
}

bool isOperator(QChar c)
{
    return (c == QChar('+') || c==QChar('-') || c==QChar('*') || c==QChar('/') || c==QChar('^'));
}

bool Converter::isOperator(const QString &c)
{
    return (c == "+" || c=="-" || c=="*" || c=="/" || c=="^");
}

QStack <QString> Converter::getPolandStack(const QString &line){
    QStack <QString> output;
    QStack <QChar> stack;

    QString param;

    for(int i=0;i<line.size();++i) {
        if(isOperator(line[i])) {
            while(!stack.isEmpty() && (priority(line[i])<=priority(stack.top())))
                output.push(stack.pop());

            stack.push(line[i]);
        } else if(line[i] == '(') {
            stack.push(line[i]);
        } else if(line[i] == ')') {
            while(!stack.isEmpty() && stack.top() != '(')
                output.push(stack.pop());
            if(stack.isEmpty())
                throw StructException(QObject::tr("Некорректная математическая операция: %1"),line);
            stack.pop();
        } else {
            param = QString();
            while (i<line.size() && (!isOperator(line[i]) && line[i]!='(' && line[i]!=')'))
                param+=line[i++];
            --i;
            output.push(param);
        }
    }

    while(!stack.isEmpty()) {
        if(stack.top() == '(' || stack.top() == ')')
            throw StructException("Некорректная математическая операция: %1",line.toStdString().c_str());

        output.push(stack.pop());
    }

    return (QStack <QString> (output));
}
