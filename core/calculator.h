#ifndef CALCULATOR_H
#define CALCULATOR_H

#include <QStack>
#include <QString>
#include "exception.h"

namespace Converter {

QStack <QString> getPolandStack(const QString &line);

bool isOperator(const QString &c);

}

#endif // CALCULATOR_H
