
#include "converter.h"
#include <QStringList>
#include <QFileInfo>
#include <QDir>
#include "calculator.h"


using namespace Converter;


//GlobalQueueList *BaseConverter::queueList()
//{
//    if(!m_QueueList)
//        m_QueueList = new GlobalQueueList();
//    return m_QueueList;
//}

void BaseConverter::readStruct()
{
    StructReader strReader(this,type,strPath,globQueueList);

    strReader.readStruct();
    strReader.link();

    mainStruct = strReader.structure();
}

void BaseConverter::convert()
{
    try
    {
        readStruct();
//        link();

        if(mainStruct != 0) {
            QString n = destination+"_"+mainStruct->getName();
            mainStruct->prepare(source,n,options);
            //mainStruct->startConvert();
            mainStruct->convertData();
            mainStruct->end();
        } else {
            emit convertError(StructException(tr("Ошибка чтения структуры!")).toException(),type);
        }
    }
    catch (StructException &e)
    {
        emit convertError(e.toException(),type);
    }
    catch (FileException &e)
    {
        emit convertError(e.toException(),type);
    }
    catch (ConvertException &e)
    {
        emit convertError(e.toException(),type);
    }

}

void BaseConverter::deconvert()
{

    try
    {
        readStruct();

        if(mainStruct != 0) {
            QString n = destination+"_"+mainStruct->getName();
            mainStruct->prepare(source,n,options,IODevice::Output);
            //mainStruct->startDeconvert();
            mainStruct->deconvertData();
            mainStruct->end();
        } else {

            emit convertError(StructException(tr("Ошибка чтения структуры!")).toException(),type);
        }
    }
    catch (StructException &e)
    {
        if(mainStruct !=0 )
            mainStruct->end();
        emit convertError(e.toException(),type);
    }
    catch (FileException &e)
    {
        if(mainStruct !=0 )
            mainStruct->end();
        emit convertError(e.toException(),type);
    }
    catch (ConvertException &e)
    {
        if(mainStruct !=0 )
            mainStruct->end();
        emit convertError(e.toException(),type);
    }
}



