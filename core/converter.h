#ifndef CONVERTER_H
#define CONVERTER_H
#include <QObject>
#include <QMetaType>
#include "core_global.h"
#include "structure.h"
#include "exception.h"
#include "globalqueuelist.h"
#include "converterenum.h"
#include "structreader.h"


namespace Converter {



class CORESHARED_EXPORT BaseConverter : public QObject
{
    Q_OBJECT
private:
    ConvertType type;
    Structure *mainStruct;
//    unsigned int linesRead;
//    int mainIndex;
//    unsigned int strNum;

    QString strPath;
    QString source;
    QString destination;
    QString options;

//    //For reading
//    QList <Structure *> strList;
//    QList <ConverterEnum *> enumList;
//    //Reading struct from .h
//    void readStruct(QString strPath) throw (FileException,StructException);
//    void readFields(QFile *strFile,Structure *mStr) throw (StructException);
//    void readEnum(QFile *strFile,ConverterEnum *mEnum) throw (StructException);

//    void link() throw (StructException);//link all structs for convertion
//    void findLink(Structure * str) throw (StructException);//find for child
//    Structure* allocStruct();//alloc Sturcture child for converter type
//    Structure* cloneStruct(Structure *s);//clone Sturcture child for converter type

    GlobalQueueList *globQueueList;


    void readStruct();
public:
    explicit BaseConverter(QObject *parent = 0):
        QObject(parent),
        type(_ConvNone),
        mainStruct(0),
//        linesRead(0),
//        mainIndex(-1),
//        strNum(0),
        strPath(QString()),
        source(QString()),
        destination(QString()),
        options(QString()),
        globQueueList(0)
    {
        qRegisterMetaType <ConvertType> ("ConvertType");
    }
    explicit BaseConverter(ConvertType type,
                        const QString &strPath,const QString &source,
                        const QString &destination,const QString &options,QObject *parent = 0):
        QObject(parent),
        type(type),
        mainStruct(0),
//        linesRead(0),
//        mainIndex(-1),
//        strNum(0),
        strPath(strPath),
        source(source),
        destination(destination),
        options(options),
        globQueueList(new GlobalQueueList)
    {
        qRegisterMetaType <ConvertType> ("ConvertType");
        qRegisterMetaType <Exception> ("Exception");
    }

    ~BaseConverter() {
        if(mainStruct)
            delete mainStruct;

        if(globQueueList)
            delete globQueueList;
    }

    void convert();
    void deconvert();

    GlobalQueueList *queueList() {
        return globQueueList;
    }

//    void registerGlobal(const QString &name) {
//        queueList()->add(name);
//    }

signals:
    void convertError(Exception e,ConvertType t);
    void blockCreated(QString block,ConvertType type);
public:
    void emitBlockCreated(const QString &block,ConvertType type){
        emit blockCreated(block,type);
    }


public slots:

};

}

#endif // CONVERTER_H
