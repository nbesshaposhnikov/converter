#ifndef CONVERTERENUM_H
#define CONVERTERENUM_H

#ifndef LONG_LONG_MAX
#define LONG_LONG_MAX _I64_MAX
#endif

#include <QList>
#include <QString>
#include "exception.h"

namespace Converter {

    struct EnumMember
    {
        EnumMember():
            value(0),
            name(QString())
        {}

        EnumMember(qint64 value,const QString &name):
            value(value),
            name(name)
        {}

        qint64 value;
        QString name;
    };


    class ConverterEnum
    {
    public:

        enum EnumType {
            _char,
            _short,
            _int,
            _longlongint
        };

        ConverterEnum():
            members(QList <EnumMember> ()),
            name(QString())
        {}

        ConverterEnum(const QString &name,const QString &textType):
            members(QList <EnumMember> ()),
            name(name),
            type(getType(textType))
        {}

        ConverterEnum(const ConverterEnum &e):
            members(e.members),
            name(e.name),
            type(e.type)
        {}

        void push(const EnumMember &m) {
            members.append(m);
        }

        EnumMember at(int i) const{
            return members.at(i);
        }

        EnumMember &at(int i){
            return members[i];
        }

        QString atValue(qint64 value) const{
            for(int i=0;i<members.size();++i) {
                if(members[i].value == value)
                    return members[i].name;
            }
            return QString();
        }

        qint64 atName(const QString &name) {
            for(int i=0;i<members.size();++i) {
                if(members[i].name == name)
                    return members[i].value;
            }
            return LONG_LONG_MAX;
        }

        int count() {
            return members.size();
        }

        QString getName() const {
            return name;
        }

        EnumType getType() {
            return type;
        }

        int size() {
            switch(type) {
            case ConverterEnum::_char: return sizeof(qint8);
            case ConverterEnum::_short: return sizeof(qint16);
            case ConverterEnum::_int: return sizeof(qint32);
            case ConverterEnum::_longlongint: return sizeof(qint64);
            }
            return -1;
        }

    private:
        QList <EnumMember> members;
        QString name;
        EnumType type;

        EnumType getType(const QString &textType){
            if(textType == "char_enum")
                return ConverterEnum::_char;
            if(textType == "short_enum")
                return ConverterEnum::_short;
            if(textType == "int_enum")
                return ConverterEnum::_int;
            if(textType == "int64_enum")
                return ConverterEnum::_longlongint;

            throw StructException(QObject::tr("Неизвестный тип enum: %1"),textType);
        }
    };

}
#endif // CONVERTERENUM_H
