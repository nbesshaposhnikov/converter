#include "converterthread.h"

using namespace Converter;

ConverterThread::ConverterThread(QObject *parent) :
    QThread(parent),
    direction(0),
    converter(0)
{
}

void ConverterThread::setConverter(ConvertType type,int dir,
                                 const QString &str,const QString &source,
                                 const QString &dest,const QString &options)
{
    direction = dir;

    if(converter)
        delete converter;

    converter = new BaseConverter(type,str,source,dest,options);
    converter->moveToThread(this);
//    connect(converter,SIGNAL(blockCreated(QString,ConvertType)),this,SLOT(emitBlockCreated(QString,ConvertType)),
//            Qt::DirectConnection);
}


void ConverterThread::run()
{
    if(!direction)
        converter->convert();
    else
        converter->deconvert();
}

