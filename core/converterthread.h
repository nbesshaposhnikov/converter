#ifndef CONVERTERTHREAD_H
#define CONVERTERTHREAD_H
#include "converter.h"
#include "core_global.h"

#include <QThread>

namespace Converter
{

   #ifndef STATIC_BUILD
    class   CORESHARED_EXPORT  ConverterThread : public QThread
   #else
    class  ConverterThread : public QThread
   #endif
    {
        Q_OBJECT
    public:
        ConverterThread(QObject *parent = 0);

        void setConverter(ConvertType type,int dir,
                                 const QString &str,const QString &source,
                                 const QString &dest,const QString &options);

        ~ConverterThread() {
            if(converter)
                delete converter;
        }

        BaseConverter *getConverter() {
            return converter;
        }

    private:

        int direction;
        BaseConverter *converter;
        virtual void run();

    signals:
        void convertError(Exception e,ConvertType t);
        void blockCreated(QString block,ConvertType type);

    public slots:
        void emitBlockCreated(QString block,ConvertType type) {
            emit blockCreated(block,type);
        }
    };
}
#endif // CONVERTERTHREAD_H
