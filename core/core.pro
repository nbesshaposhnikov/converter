#-------------------------------------------------
#
# Project created by QtCreator 2011-08-05T21:21:16
#
#-------------------------------------------------
include(../main/main.pri)
include(qtxlsx/xlsx/qtxlsx.pri)

QT       += core gui sql

#win32:LIBS += QAxContainer.lib

TARGET = Converter
TEMPLATE = lib

DEFINES += CORE_LIBRARY

CONFIG(release,declarative_debug|release):{
DEFINES += RELEASE
#win32:CONFIG += static
}

target.path = $${LIBDIR}
INSTALLS += target
macx {
    DEFINES += MAC_X
    DESTDIR = ../../bin/Converter.app/Contents/Frameworks
    QMAKE_LFLAGS_SONAME = -Wl,-install_name,@executable_path/../Frameworks/
} else {
    DESTDIR = ../bin
}
contains(QT_CONFIG, reduce_exports): CONFIG += hide_symbols
contains(CONFIG, static) {
    DEFINES += STATIC_BUILD
}

#DLLDESTDIR = ../bin

OBJECTS_DIR = .obj
win32 {

DEFINES += WIN32
QT  += axcontainer
MOC_DIR = .moc
UI_DIR = .uic
RCC_DIR = .rcc
OBJECTS_DIR = .obj
}


SOURCES += \
    converter.cpp \
    structure.cpp \
    fielddata.cpp \
    globalqueuelist.cpp \
    globalqueue.cpp \
    calculator.cpp \
    converterthread.cpp \
    converterenum.cpp \
    databasemanager.cpp \
    structreader.cpp \
    txtinputdevice.cpp \
    txtoutputdevice.cpp \
    sqlinputdevice.cpp \
    sqloutputdevice.cpp \
    excelinputdevice.cpp \
    exceloutputdevice.cpp

HEADERS += \
    core_global.h \
    converter.h \
    structure.h \
    exception.h \
    fielddata.h \
    globalqueuelist.h \
    globalqueue.h \
    calculator.h \
    converterthread.h \
    converterenum.h \
    databasemanager.h \
    structreader.h \
    basictypes.h \
    iodevice.h \
    txtinputdevice.h \
    txtoutputdevice.h \
    sqlinputdevice.h \
    sqloutputdevice.h \
    excelinputdevice.h \
    exceloutputdevice.h

macx {

    contains(QT_CONFIG, ppc):CONFIG += x86 \
	ppc
}

OTHER_FILES +=





































