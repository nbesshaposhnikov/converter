#include "databasemanager.h"



using namespace Converter;

DatabaseManager *DatabaseManager::instanse = 0;

DatabaseManager::DatabaseManager():
    db(QSqlDatabase()),
    q(QSqlQuery())
{

}

DatabaseManager *DatabaseManager::getInstanse()
{
    if(instanse == 0 )
        instanse = new DatabaseManager;

    return instanse;
}

void DatabaseManager::deleteInstanse()
{
    if(instanse != 0) {
        delete instanse;
        instanse = 0;
        QSqlDatabase::removeDatabase("Convert");
    }
}

void DatabaseManager::open()
{
    if(!db.open()) {
        QSqlError error = db.lastError();
        deleteInstanse();
        throw(ConvertException("%1",error.text()));
    }
}

void DatabaseManager::connect(const QString& dbName,const QString& uName,const QString& uPassword,
                              const QString& host,const QString& dbOptions,const QString& dbType,IODevice::Direction direction)
{

    QString appDir = QCoreApplication::applicationDirPath();

    #ifdef MAC_X
    QDir dir(appDir);
    dir.cdUp();dir.cdUp();dir.cdUp();
    appDir = dir.absolutePath();
    #endif


    //QSqlDatabase::removeDatabase("Convert");
    db = QSqlDatabase::addDatabase(dbType,"Convert");

    db.setUserName(uName);
    db.setPassword(uPassword);
    db.setHostName(host);
    db.setDatabaseName(dbOptions);
    if(direction == IODevice::Input) {

        if(dbType!="QSQLITE") {

            if(!db.open()) {
                QSqlError error = db.lastError();
                deleteInstanse();
                throw(ConvertException("%1",error.text()));
            }
            //clear and create database
            q = QSqlQuery("",db);
            if(dbType == "QODBC") {
                q = db.exec("IF EXISTS (SELECT * FROM sysdatabases WHERE name='"+dbName+"')"+
                            "DROP DATABASE "+dbName+" CREATE DATABASE "+dbName);

            } else if(dbType == "QPSQL") {
                q = db.exec("select count(1) from pg_catalog.pg_database where datname ='"+dbName.toLower()+"'");
                q.next();
                if(q.value(0).toInt())
                    q = db.exec("DROP DATABASE "+dbName.toLower());

                q = db.exec("CREATE DATABASE "+dbName);
            }

            if(q.lastError().isValid()) {
                QSqlError lastError = q.lastError();
                QString lastQuery  = q.lastQuery();

                deleteInstanse();
                throw(ConvertException("%1",lastError.text()+
                               QString("(")+lastQuery+QString(")")));
            }

            db.close();
        } else {
            if(QFile::exists(appDir+"/"+dbName+".sqlite"))
                QFile::remove(appDir+"/"+dbName+".sqlite");
        }
    }

    if(dbType == "QODBC") {
        db.setDatabaseName(dbOptions + "DATABASE=" + dbName + ";");
    } else if(dbType == "QPSQL") {
        db.setDatabaseName(dbName.toLower());
    } else if(dbType == "QSQLITE") {
        db.setDatabaseName(appDir+"/"+dbName+".sqlite");
    }

}

void DatabaseManager::connect(const QString& dbName, IODevice::Direction direction)
{
    if(direction == IODevice::Input) {
        if(QFile::exists(dbName))
            QFile::remove(dbName);
    }

    db = QSqlDatabase::addDatabase("QODBC","Convert");
    db.setDatabaseName("DRIVER={Microsoft Excel Driver (*.xls, *.xlsx, *.xlsm, *.xlsb)};READONLY=FALSE;DSN='';DBQ=" + dbName);

    //test Connection
    if(!db.open()) {
        deleteInstanse();
        throw(ConvertException("%1",db.lastError().text()));
    }

    db.close();
}

QSqlQuery &DatabaseManager::exec(const QString& query)
{
   q = db.exec(query);

   if(q.lastError().isValid()) {
       QSqlError lastError = q.lastError();

       deleteInstanse();
       qDebug()<<lastError.text()<<query;
       throw(ConvertException("%1",lastError.text() + QString("(") + query + QString(")")));
   }

   return q;
}


