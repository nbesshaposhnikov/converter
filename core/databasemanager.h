#ifndef DATABASEMANAGER_H
#define DATABASEMANAGER_H

#include <QtCore>
#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>

#include "exception.h"
#include "iodevice.h"

namespace Converter {

class DatabaseManager
{
public:
    DatabaseManager();
    ~DatabaseManager() {
        q.finish();
        db.close();
    }

    void connect(const QString& dbName,const QString& uName,const QString& uPassword,
                    const QString& host,const QString& dbOptions,const QString& dbType,IODevice::Direction direction); //for SQL

    void connect(const QString& dbName,IODevice::Direction direction); // for Excel

    QSqlQuery &exec(const QString& query);

    void open();

    bool isOpen() {
        return db.isOpen();
    }

    QSqlDatabase &database() {
        return db;
    }

    static DatabaseManager *getInstanse();
    static void deleteInstanse();
    static DatabaseManager *instanse;


private:
    QSqlDatabase db;
    QSqlQuery q;


};

}
#endif // DATABASEMANAGER_H
