#include "emitter.h"

using namespace Converter;

void Emitter::emitBlockCreated(const QString &block,ConvertType type)
{
    emit blockCreated(block,type);
}
