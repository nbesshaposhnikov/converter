#ifndef EMITTER_H
#define EMITTER_H

#include <QObject>



namespace Converter {



    class Emitter : public QObject
    {
        Q_OBJECT
    public:
        Emitter(QObject *p=0):
            QObject(p)
        {}

    signals:
        void blockCreated(QString block,ConvertType type);
    public:
        void emitBlockCreated(const QString &block,ConvertType type);
    };

}
#endif // EMITTER_H
