﻿#include "excelinputdevice.h"
#include "structure.h"

using namespace Converter;
using namespace QXlsx;

QByteArray ExcelInputDevice::read(quint64 size)
{
    return in->read(size);
}

void ExcelInputDevice::write(const QByteArray &ba,quint64 /*size*/)
{
    if(ba == QByteArray("\n")) {
        QList <QVariant> outData;
        while(!dataQueue.isEmpty())
            outData << QVariant(QObject::tr(dataQueue.dequeue().data()));

        insert(outData);
        dataQueue.clear();        
    } else {
        dataQueue.enqueue(ba);
    }
}

void ExcelInputDevice::prepareDevice(const QString &source,const QString &dest,const QString &options)
{
    baseName = dest;

    tableName = baseName;
    tableName.replace('.','_');

    in = new QFile(source);
    if(!in->open(QIODevice::ReadOnly))
        throw FileException(QObject::tr("Ошибка открытия файла %1:\n%2"),in->fileName(),in->errorString());

    //intialize activeX
    QString fp = options + "/" + dest.left(dest.lastIndexOf(QChar('_'))) + ".xlsx";

    filePath = fp;
    filePath.replace('/',"\\");
/*#ifdef WIN32
    CoInitialize (NULL);
    excel = new QAxObject( "Excel.Application"); //получаем указатьтель на excel

    if(excel == NULL)
        throw FileException(QObject::tr("Не найден драйвер Excel."));

    excel->dynamicCall( "SetVisible(bool)", false ); //делаем его невидимым
    excel->dynamicCall( "SetDisplayAlerts(bool)", false );

    workbooks = excel->querySubObject( "Workbooks" );
    workbook = workbooks->querySubObject( "Add()");
    sheets = workbook->querySubObject( "Sheets" );
    firstSheet = sheets->querySubObject( "Item (QVariant)",QVariant(1));//get first sheet

    sheet = sheets->querySubObject( "Add" );
    sheet->setProperty("Name", tableName);



#endif*/
    excelDocument = new Document();
    excelDocument->addSheet(tableName);
    excelDocument->selectSheet(tableName);
    curSheet = excelDocument->currentWorksheet();

    //create insert header
    QString insertHeader = parent->getFieldNames("");
    insertHeader.chop(1);

    QList <QString> headerList = insertHeader.split(',',QString::SkipEmptyParts);
    QList <QVariant> headerData;

    colNum = headerList.size();

    for(int i=0; i < headerList.size();++i) {
        headerData << headerList[i];
    }

    insert(headerData);
}

void ExcelInputDevice::closeDevice()
{

    if(!isChildren) {
        in->close();
        write("\n");
        //insert(QList <QVariant>() , true);

//        workbook->dynamicCall("SaveAs(const QVariant&,const QVariant&)",
//                              QVariant(filePath),
//                             QVariant(56));
//        workbook->dynamicCall("Close(const QVariant &)",true);
//        excel->dynamicCall("Quit()");

        excelDocument->saveAs(filePath);
    }
//    else
//        insert(QList <QVariant>() , true);

    //delete sheet;
}

void ExcelInputDevice::prepareDevice(IODevice *other,const QString &newPath)
{
    baseName = ((ExcelInputDevice *)other)->baseName+"_"+newPath;

    tableName = baseName;
    tableName.replace('.','_');

    isChildren=true;

    in = ((ExcelInputDevice *)other)->in;

    excelDocument = ((ExcelInputDevice *)other)->excelDocument;

//    sheets = ((ExcelInputDevice *)other)->sheets;
//    firstSheet = ((ExcelInputDevice *)other)->firstSheet;

//    firstSheet->dynamicCall("Select");//setting at first list
//    sheet = sheets->querySubObject( "Add" );
//    sheet->setProperty("Name", tableName);

    excelDocument->addSheet(tableName);
    excelDocument->selectSheet(tableName);
    curSheet = excelDocument->currentWorksheet();

    QString insertHeader = parent->getFieldNames("");
    insertHeader.chop(1);

    QList <QString> headerList = insertHeader.split(',',QString::SkipEmptyParts);
    QList <QVariant> headerData;

    colNum = headerList.size();

    for(int i=0; i < headerList.size();++i) {
        headerData << headerList[i];
    }

    insert(headerData);
}


void ExcelInputDevice::insert(const QList <QVariant> data,bool ignore)
{
    ++rowNum;
    for(int i = 0; i < data.size(); ++i)
    {
        curSheet->write(rowNum,i+1,data.at(i));
    }

//#ifdef WIN32
//    //QList <QVariant> insertData;

//    QAxObject* cell1;
//    QAxObject* cell2;
//    QAxObject* range;

//    if(colNum > 255) {
//        int realColNum = colNum;
//        int pos = 0;
//        int writeColNum = (realColNum > 255) ? 255 : realColNum;

//        while(realColNum > 0) {
//            ++rowNum;
//            cell1 = sheet->querySubObject("Cells(QVariant&,QVariant&)", rowNum , 1);
//            cell2 = sheet->querySubObject("Cells(QVariant&,QVariant&)",rowNum, writeColNum);

//            // получение указателя на целевую область
//            range = sheet->querySubObject("Range(const QVariant&,const QVariant&)", cell1->asVariant(), cell2->asVariant() );

//            range->setProperty("NumberFormat", "@" );

//            insertData << QVariant(data.mid(pos,writeColNum));

//            range->dynamicCall("SetValue(const QVariant&)", QVariant(insertData));

//            realColNum -= writeColNum;
//            pos += writeColNum;
//            writeColNum = (realColNum > 255) ? 255 : realColNum;


//            delete range;
//            delete cell2;
//            delete cell1;

//            insertData.clear();
//        }
//    } else {

//            if(insertRowNum == MAX_ROW_NUM || (ignore && insertRowNum > 0)) {
//                ++rowNum;
//                cell1 = sheet->querySubObject("Cells(QVariant&,QVariant&)", rowNum , 1);
//                cell2 = sheet->querySubObject("Cells(QVariant&,QVariant&)",rowNum + insertRowNum - 1, colNum);

//                // получение указателя на целевую область
//                range = sheet->querySubObject("Range(const QVariant&,const QVariant&)", cell1->asVariant(), cell2->asVariant() );

//                range->setProperty("NumberFormat", "@" );

//                range->dynamicCall("SetValue(const QVariant&)", QVariant(insertData));

//                delete range;
//                delete cell2;
//                delete cell1;

//                insertData.clear();
//                rowNum += insertRowNum -1;
//                insertRowNum = 0;

//            }

//            ++insertRowNum;
//            insertData << QVariant(data);

//    }
//#endif
}


