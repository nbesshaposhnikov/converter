#ifndef EXCELINPUTDEVICE_H
#define EXCELINPUTDEVICE_H

#include "iodevice.h"
#include "exception.h"
#include "databasemanager.h"

#include "qtxlsx/xlsx/xlsxdocument.h"
#include "qtxlsx/xlsx/xlsxworksheet.h"
#ifdef WIN32
#include <ActiveQt/qaxobject.h>
#include <windows.h>
#endif

namespace Converter {

    class Structure;

    class ExcelInputDevice:public IODevice
    {
    private:
        QFile *in;
        QString tableName;
        QXlsx::Document *excelDocument;
        QXlsx::Worksheet *curSheet;
        int rowNum;
        int colNum;

        int insertRowNum;
        QList <QVariant> insertData;

        QString filePath;


    public:
        ExcelInputDevice():
            IODevice(),
            in(0),
            tableName(QString()),
            excelDocument(0),
            curSheet(0),
            rowNum(0),
            colNum(0),
            insertRowNum(0),
            insertData(QList <QVariant>()),
            filePath(QString())
        {}

        ExcelInputDevice(Structure *parent):
            IODevice(parent),
            in(0),
            tableName(QString()),
            excelDocument(0),
            curSheet(0),
            rowNum(0),
            colNum(0),
            insertRowNum(0),
            insertData(QList <QVariant>()),
            filePath(QString())
        {}

        ~ExcelInputDevice()
        {
            if(!isChildren) {
                if(in != 0)
                    delete in;
                delete excelDocument;
            }


        }

        void insert(const QList <QVariant> data,bool ignore = false);

        virtual QByteArray read(quint64 size);
        virtual void write(const QByteArray &ba,quint64 size = 0);
        virtual void getChar(char *c){
            in->getChar(c);
        }

        virtual void prepareDevice(const QString &source,const QString &dest,const QString &options);
        virtual void prepareDevice(IODevice *other,const QString &newPath);
        virtual void closeDevice();



    };

}
#endif // EXCELINPUTDEVICE_H
