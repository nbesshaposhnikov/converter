﻿#include "exceloutputdevice.h"
#include "structure.h"

using namespace Converter;

QByteArray ExcelOutputDevice::read(quint64 size)
{
    if(size == 0) {
        dataQueue.clear();
        QList <QVariant> ret = select();
        for(int i=0; i < ret.size(); ++i)
            dataQueue.enqueue(to_1251(ret.at(i).toString()));
    }
    else {
        if(dataQueue.isEmpty())
            throw ConvertException(QObject::tr("Недостаточно данных!"));
        return dataQueue.dequeue();
    }
    return QByteArray();
}

void ExcelOutputDevice::write(const QByteArray &ba,quint64 /*size*/)
{
    out->write(ba.data(),ba.size());
}

void ExcelOutputDevice::prepareDevice(const QString &source,const QString &dest,const QString &options)
{
    baseName = dest;

    tableName = baseName;
    tableName.replace('.','_');

    out = new QFile(source);
    if(!out->open(QIODevice::WriteOnly))
        throw FileException(QObject::tr("Ошибка открытия файла %1:\n%2"),out->fileName(),out->errorString());

    //intialize activeX
    filePath = options + "/" + dest.left(dest.lastIndexOf(QChar('_'))) + ".xlsx";
    filePath.replace('/',"\\");
#ifdef WIN32
    CoInitialize (NULL);
    excel = new QAxObject( "Excel.Application"); //получаем указатьтель на excel

    if(excel == NULL)
        throw FileException(QObject::tr("Не найден драйвер Excel."));

    excel->dynamicCall( "SetVisible(bool)", false ); //делаем его невидимым
    excel->dynamicCall( "SetDisplayAlerts(bool)", false );

    workbooks = excel->querySubObject( "Workbooks" );
    workbook = workbooks->querySubObject( "Open(const QString&)",QVariant(filePath));

    if(workbook == NULL)
        throw FileException(QObject::tr("Невозможно открыть файл."));

    sheets = workbook->querySubObject( "Sheets" );
    sheet = sheets->querySubObject( "Item(const QVariant&)", QVariant(tableName) );

#endif
    //get colnum
    QString insertHeader = parent->getFieldNames("");
    insertHeader.chop(1);

    colNum = insertHeader.split(',',QString::SkipEmptyParts).size();

    read();//read header
    read();//read first data
}

void ExcelOutputDevice::closeDevice()
{
    if(!isChildren) {
        out->close();
        workbook->dynamicCall("Close(const QVariant &)",true);
        excel->dynamicCall("Quit()");
    }

    delete sheet;
}

void ExcelOutputDevice::prepareDevice(IODevice *other,const QString &newPath)
{
    baseName = ((ExcelOutputDevice *)other)->baseName+"_"+newPath;

    tableName = baseName;
    tableName.replace('.','_');

    isChildren=true;

    out = ((ExcelOutputDevice *)other)->out;

    sheets = ((ExcelOutputDevice *)other)->sheets;

    sheet = sheets->querySubObject( "Item(const QVariant&)", QVariant(tableName) );



    //get colnum
    QString insertHeader = parent->getFieldNames("");
    insertHeader.chop(1);

    colNum = insertHeader.split(',',QString::SkipEmptyParts).size();

    read();//read header
    read();//read first data
}


QList <QVariant> ExcelOutputDevice::select()
{
#ifdef WIN32
    QList <QVariant> ret;

    QAxObject* cell1;
    QAxObject* cell2;
    QAxObject* range;

    if(colNum > 255) {

        int realColNum = colNum;
        int pos = 0;
        int readColNum = (realColNum > 255) ? 255 : realColNum;

        while(realColNum > 0) {
            ++rowNum;
            cell1 = sheet->querySubObject("Cells(QVariant&,QVariant&)", rowNum , 1);
            cell2 = sheet->querySubObject("Cells(QVariant&,QVariant&)",rowNum, readColNum);

            // получение указателя на целевую область
            range = sheet->querySubObject("Range(const QVariant&,const QVariant&)", cell1->asVariant(), cell2->asVariant() );

            ret << range->property("Value").toList().at(0).toList();

            realColNum -= readColNum;
            pos += readColNum;
            readColNum = (realColNum > 255) ? 255 : realColNum;


            delete range;
            delete cell2;
            delete cell1;
        }
    } else {
        if(selectRowNum == MAX_ROW_NUM) {
            selectedData.clear();
            ++rowNum;
            cell1 = sheet->querySubObject("Cells(QVariant&,QVariant&)", rowNum , 1);
            cell2 = sheet->querySubObject("Cells(QVariant&,QVariant&)",rowNum + selectRowNum - 1, colNum);

            // получение указателя на целевую область
            range = sheet->querySubObject("Range(const QVariant&,const QVariant&)", cell1->asVariant(), cell2->asVariant() );


            selectedData << range->property("Value").toList();

            delete range;
            delete cell2;
            delete cell1;

            rowNum += selectRowNum - 1;
            selectRowNum = 0;

        }
        if(selectRowNum > selectedData.size())
            throw ConvertException(QObject::tr("Недостаточно данных!"));

        ret << selectedData.at(selectRowNum).toList();
        ++selectRowNum;
    }

    return ret;
#endif
}


