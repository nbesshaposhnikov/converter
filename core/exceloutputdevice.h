#ifndef EXCELOUTPUTDEVICE_H
#define EXCELOUTPUTDEVICE_H

#include "iodevice.h"
#include "exception.h"
#include "databasemanager.h"

#ifdef WIN32
#include <ActiveQt/qaxobject.h>
#include <windows.h>
#endif

namespace Converter {

    class Structure;

    class ExcelOutputDevice:public IODevice
    {
    private:
        QFile *out;
        QString tableName;

        QAxObject* excel;
        QAxObject *workbooks;
        QAxObject *workbook;
        QAxObject *sheets;
        QAxObject *sheet;

        int rowNum;
        int colNum;

        int selectRowNum;
        QList <QVariant> selectedData;

        QString filePath;
    public:
        ExcelOutputDevice():
            IODevice(),
            out(0),
            tableName(QString()),
            excel(0),
            workbooks(0),
            workbook(0),
            sheets(0),
            sheet(0),
            rowNum(0),
            colNum(0),
            selectRowNum(MAX_ROW_NUM)
        {}

        ExcelOutputDevice(Structure *parent):
            IODevice(parent),
            out(0),
            tableName(QString()),
            excel(0),
            workbooks(0),
            workbook(0),
            sheets(0),
            sheet(0),
            rowNum(0),
            colNum(0),
            selectRowNum(MAX_ROW_NUM)
        {}

        ~ExcelOutputDevice()
        {
            if(!isChildren) {
                if(out != 0)
                    delete out;

                delete sheets;
                delete workbook;
                delete workbooks;
                delete excel;
            }


        }

        QList <QVariant> select();

        virtual QByteArray read(quint64 size = 0);
        virtual void write(const QByteArray &ba,quint64 size = 0);
        virtual void getChar(char *c){

        }

        virtual void prepareDevice(const QString &source,const QString &dest,const QString &options);
        virtual void prepareDevice(IODevice *other,const QString &newPath);
        virtual void closeDevice();



    };

}
#endif // EXCELOUTPUTDEVICE_H
