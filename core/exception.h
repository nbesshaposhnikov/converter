﻿#ifndef EXCEPTION_H
#define EXCEPTION_H

#include <QString>
#include <QVariant>

//#pragma warning ( disable : 4290 )//for disabling strange warnings about exceptions and virtual functions
//#pragma warning ( disable : 4100 )

namespace Converter {

class Exception {
public:
    Exception():
        header(QString()),
        text(QString())
    {}

    Exception(QString h,QString t):
        header(h),
        text(t)
    {}

    QString header;
    QString text;

};

class StructException:public Exception {
public:
    StructException(QString t,QVariant argument = QVariant())
    {
        header=QObject::tr("Ошибка структуры");
        text = t.arg(argument.toString());
    }

    StructException(QString t,QVariant argument1,QVariant argument2)
    {
        header=QObject::tr("Ошибка структуры");
        text = t.arg(argument1.toString(),argument2.toString());
    }

    Exception toException() {
        return(*(Exception *)this);
    }
};

class FileException:public Exception {
public:
    FileException(QString t,QVariant argument = QVariant())
    {
        header=QObject::tr("Ошибка файла");
        text = t.arg(argument.toString());

    }

    FileException(QString t,QVariant argument1,QVariant argument2)
    {
        header=QObject::tr("Ошибка файла");
        text = t.arg(argument1.toString(),argument2.toString());

    }

    Exception toException() {
        return(*(Exception *)this);
    }
};

class ConvertException:public Exception {
public:
    ConvertException(QString t,QVariant argument = QVariant())
    {
        header=QObject::tr("Ошибка конвертирования");
        text = t.arg(argument.toString());

    }

    ConvertException(QString t,QVariant argument1,QVariant argument2)
    {
        header=QObject::tr("Ошибка конвертирования");
        text = t.arg(argument1.toString(),argument2.toString());

    }

    Exception toException() {
        return(*(Exception *)this);
    }
};


}
#endif // EXCEPTION_H
