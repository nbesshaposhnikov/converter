#include "fielddata.h"
#include <QBitArray>
#include <QByteArray>
#include "calculator.h"
#include "structure.h"
#include "converter.h"
using namespace Converter;

FieldData::~FieldData() {
    if(child)
        delete child;
    child =0;
}


void FieldData::setType(const QString &dataType,bool isUnsigned)
{
    if(dataType == "char") {
        if(!b_isArray)
            if(isUnsigned)
               type = _uchar;
            else
               type = _char;
        else
            type = _string;
    } else if(dataType == "short") {
        if(isUnsigned)
            type = _ushort;
        else
            type = _short;
    } else if(dataType == "int") {
        if(isUnsigned)
            type = _uint;
        else
            type = _int;
    } else if(dataType == "__int64") {
        if(isUnsigned)
            type = _uint64;
        else
            type = _longlongint;
    } else if(dataType == "xchar") {
        type = _xchar;
    } else if(dataType == "xshort") {
        type = _xshort;
    } else if(dataType == "xint") {
        type = _xint;
    } else if(dataType == "xint64") {
        type = _xint64;
    } else if(dataType == "float") {
        type = _float;
    } else if(dataType == "double") {
        type = _double;
    } else if(dataType == "string") {
        type = _string;
    } else if(dataType == "char_bitfield") {
        type = _char_bitfield;
    }  else if(dataType == "short_bitfield") {
        type = _short_bitfield;
    }  else if(dataType == "int_bitfield") {
        type = _int_bitfield;
    }  else if(dataType == "int64_bitfield") {
        type = _int64_bitfield;
    }  else if(dataType == "char_calcfield") {
        type = _char_calcfield;
    }  else if(dataType == "short_calcfield") {
        type = _short_calcfield;
    }  else if(dataType == "int_calcfield") {
        type = _int_calcfield;
    }  else if(dataType == "int64_calcfield") {
        type = _int64_calcfield;
    } else if(dataType == "blob") {
        type = _blob;
    } else if(dataType == "enum") {
        type = _enum;
    } else {
        type = _struct;
        childName = dataType;
    }

}




int FieldData::getSize()
{
    switch(type) {
    case _char:case _char_bitfield:case _char_calcfield: case _uchar:case _xchar: return sizeof(qint8);
    case _short:case _short_bitfield:case _short_calcfield: case _ushort:case _xshort: return sizeof(qint16);
    case _int:case _int_bitfield:case _int_calcfield: case _uint:case _xint:case _float: return sizeof(qint32);
    case _longlongint:case _int64_bitfield:case _int64_calcfield: case _uint64:case _xint64:case _double: return sizeof(qint64);
    case _enum: return enumData->size();
    default: return 0;
    }
}

QByteArray from_1251(const QByteArray &data)
{
    QTextCodec *codec = QTextCodec::codecForName("cp1251");
    QTextCodec::setCodecForLocale(codec);
    QString string = codec->toUnicode(data);

    QByteArray ba;
    ba += string;

    return ba;
}

QByteArray to_1251(const QByteArray &data)
{
    static QTextCodec *codec=QTextCodec::codecForName("Windows-1251");
    QByteArray ba=codec->fromUnicode(QString(data));

    return ba;
}

void FieldData::setDataIn(QByteArray newData)
{
    //newData = newData.trimmed();
    switch(type) {
    case _char: data=QByteArray::number(*(qint8 *)(newData.data()));break;
    case _short: data=QByteArray::number(*(qint16 *)(newData.data()));break;
    case _int: data=QByteArray::number(*(qint32 *)(newData.data()));break;
    case _longlongint: data=QByteArray::number(*(qint64 *)(newData.data()));break;
    case _uchar:case _char_bitfield:
    case _xchar: data=QByteArray::number(*(quint8 *)(newData.data()));break;
    case _ushort:case _short_bitfield:
    case _xshort: data=QByteArray::number(*(quint16 *)(newData.data()));break;
    case _uint:case _int_bitfield:
    case _xint: data=QByteArray::number(*(quint32 *)(newData.data()));break;
    case _uint64:case _int64_bitfield:
    case _xint64: data=QByteArray::number(*(quint64 *)(newData.data()));break;
    case _float: data=QByteArray::number(*(float *)(newData.data()),'f');break;
    case _double: data=QByteArray::number(*(double *)(newData.data()),'f');break;
    //calcfields
    case _char_calcfield:case _short_calcfield:
    case _int_calcfield:case _int64_calcfield:data = QByteArray::number(calcData());break;
    case _string:case _blob: data = from_1251(newData);break;
    //enum
    case _enum : {
        switch(enumData->getType()) {
        case ConverterEnum::_char: data=QByteArray::number(*(char *)(newData.data()));break;
        case ConverterEnum::_short: data=QByteArray::number(*(short *)(newData.data()));break;
        case ConverterEnum::_int: data=QByteArray::number(*(int *)(newData.data()));break;
        case ConverterEnum::_longlongint: data=QByteArray::number(*(long long int *)(newData.data()));break;
        }
    }break;

    default: data = QByteArray();break;
    }
}


QByteArray FieldData::getDataIn()
{
    QByteArray ret = QByteArray();

    switch(type) {
    case _char_bitfield: ret = QByteArray::number(data.toLongLong(),2);ret.prepend(QByteArray().fill('0',8-ret.size()));break;
    case _short_bitfield: ret = QByteArray::number(data.toLongLong(),2);ret.prepend(QByteArray().fill('0',16-ret.size()));break;
    case _int_bitfield:ret = QByteArray::number(data.toLongLong(),2);ret.prepend(QByteArray().fill('0',32-ret.size()));break;
    case _int64_bitfield: ret = QByteArray::number(data.toLongLong(),2);ret.prepend(QByteArray().fill('0',64-ret.size()));break;
    case _string: ret=data;
        ret.replace('\n',"\\n");
        ret.replace('\t',"\\t");
        ret.replace('\r',"\\r");
        ret.replace(QByteArray("'"),QByteArray("''"));
        ret.resize(strlen(ret.data()));
        break;
    case _xchar:case _xshort:
    case _xint: case _xint64:
        ret =  "0x"+QByteArray::number(data.toLongLong(),16).toUpper();break;
    case _blob: ret = data.toHex();break;
    case _enum : {
        ret = enumData->atValue(data.toLongLong()).toLatin1();
        if(ret==QString())
            ret = data;
    }break;

    default:ret=data;break;
    }

    return ret;
}

QString FieldData::getSqlType(ConvertType convType)
{
    if(convType == SQL){
        switch(type){
            case _char:case _uchar:
            case _short:case _ushort:
            case _char_calcfield:case _short_calcfield:
            case _int: return "int";break;

            case _uint:case _longlongint:
            case _int_calcfield:case _int64_calcfield:
            case _uint64: return "bigint";break;

            case _float: return "float(24)";break;
            case _double: return "float(53)";break;
            //+2 for 0x
            case _xchar:return "varchar(4)";break;
            case _xshort:return "varchar(6)";break;
            case _xint:return "varchar(10)";break;
            case _xint64:return "varchar(18)";break;

            case _char_bitfield:case _short_bitfield:
            case _int_bitfield:case _int64_bitfield: return "varchar(64)";break;

            case _string:case _blob: return "varchar(4096)";break;

            case _enum:return "varchar(4096)";break;
        }
    } else {
        return "string";
    }
    return QString();
}


QByteArray FieldData::getDataOut()
{
    QByteArray ret = QByteArray();

    switch(type) {
    case _char:case _uchar:case _xchar:case _char_bitfield: case _char_calcfield:{
        quint8 c = data.toShort();
        ret.append((char *)&c,sizeof(qint8));
        break;
    }
    case _ushort:case _xshort:case _short_bitfield: case _short_calcfield:{
        quint16 us = data.toUShort();
        ret.append((char *)&us,sizeof(qint16));
        break;
    }
    case _short: {
        quint16 s = data.toShort();
        ret.append((char *)&s,sizeof(qint16));
        break;
    }
    case _uint:case _xint:case _int_bitfield: case _int_calcfield:{
        quint32 uin = data.toUInt();
        ret.append((char *)&uin,sizeof(qint32));
        break;
    }
    case _int: {
        quint32 in = data.toInt();
        ret.append((char *)&in,sizeof(qint32));
        break;
    }
    case _uint64:case _xint64:case _int64_bitfield: case _int64_calcfield:{
        quint64 ui64 = data.toULongLong();
        ret.append((char *)&ui64,sizeof(quint64));
        break;
    }
    case _longlongint:{
        quint64 i64 = data.toLongLong();
        ret.append((char *)&i64,sizeof(quint64));
        break;
    }
    case _float: {
        float f = data.toFloat();
        ret.append((char *)&f,sizeof(float));
        break;
    }
    case _double: {
        double d = data.toDouble();
        ret.append((char *)&d,sizeof(double));
        break;
    }
    //calcfields
    case _string: {
        ret = data;
        quint64 destSize = size();

        if(ret.size() > destSize)
            ret.resize(destSize);

        for(quint64 j = ret.size(); j< destSize ; ++j)
            ret.append('\0');

        ret[ret.size()-1] = '\0';

        break;
    }
    case _blob:ret = data;break;
    //enum
    case _enum :{
        switch(enumData->getType()) {
            case ConverterEnum::_char:{
                quint8 ec = data.toShort();
                ret.append((char *)&ec,sizeof(quint8));
                break;
            }
            case ConverterEnum::_short:{
                quint16 es = data.toUShort();
                ret.append((char *)&es,sizeof(quint16));
                break;
            }
            case ConverterEnum::_int:{
                quint32 ein = data.toUInt();
                ret.append((char *)&ein,sizeof(quint32));
                break;
            }
            case ConverterEnum::_longlongint:{
            quint64 ei64 = data.toULongLong();
            ret.append((char *)&ei64,sizeof(quint64));
                break;
            }
        }
        break;
    }

    }
    return  ret;
}


void FieldData::setDataOut(QByteArray newData)
{
    newData = newData.trimmed();
    switch(type) {
    case _char_bitfield: data = QByteArray::number(newData.toUShort(0,2));break;
    case _short_bitfield: data = QByteArray::number(newData.toUShort(0,2));break;
    case _int_bitfield: data = QByteArray::number(newData.toUInt(0,2));break;
    case _int64_bitfield: data = QByteArray::number(newData.toULongLong(0,2));break;
    case _string: data = newData;
        data.replace("\\n","\n");
        data.replace("\\t","\t");
        data.replace("\\r","\r");
        data.replace("''","'");
        break;
    case _xchar:case _xshort:
    case _xint: case _xint64:
        data = QByteArray::number(newData.toLongLong(0,16));break;

    case _char_calcfield:case _short_calcfield:
    case _int_calcfield:case _int64_calcfield:
        data = QByteArray::number(calcData());break;
    case _blob: data = QByteArray::fromHex(newData);break;
    case _enum: {
        qint64 value = enumData->atName(QString(newData));
        if(value == LONG_LONG_MAX)
            data = newData;
        else
            data = QByteArray::number(value);
    }break;
    default:data=to_1251(newData);break;
    }
}



quint64 FieldData::calcData()
{
    QList <QString> line = countData.toList();
    QStack <quint64> stack;
    QList <QString>::iterator i;

    for(i=line.begin();i!=line.end();++i) {
        if(!isOperator(*i)) {

            QString label = (*i).right((*i).size()-1);

            if((*i)[0] == 'i')
                stack.push((label).toLongLong());
            else if((*i)[0] == 'l')
                stack.push(parent->at(label.toULongLong()).getCalcData().toULongLong());
            else if((*i)[0] == 'g')
                stack.push(parent->getParent()->queueList()->getQueue(label).getData().toULongLong());
        } else {

        switch((*i)[0].toLatin1()) {
        case '+':
        {
        quint64 x=stack.pop();
        quint64 y=stack.pop();
            stack.push(y+x);
            break;
        }
        case '-':
        {
        quint64 x=stack.pop();
        quint64 y=stack.pop();
            if(y-x<0)
                throw ConvertException(QObject::tr("Отрицательное число"));

            stack.push(y-x);
            break;
        }
        case '*':
        {
        quint64 x=stack.pop();
        quint64 y=stack.pop();
            stack.push(y*x);
            break;
        }
        case '/':
        {
        quint64 x=stack.pop();
        quint64 y=stack.pop();
            if(x==0)
                throw ConvertException(QObject::tr("Деление на ноль"));

            stack.push(y/x);
            break;
        }
        }

        }
    }

    return stack.top();
}

void FieldData::markCountData()
{
    QStack <QString> newStack;

    QList <QString> line = countData.toList();

    QList <QString>::iterator i;

    QString label;
    int index;
    for(i=line.begin();i!=line.end();++i) {
        if(!isOperator(*i)) {
            bool ok = false;
            QString(*i).toInt(&ok);

            if(ok){
                label = "i" + *i;
            } else if((index=parent->find(*i)) != -1) {
                label = "l" + QString::number(index);
            } else {
                parent->getParent()->queueList()->getQueue(*i);

                label = "g" + *i;
            }
            newStack.push(label);
        } else {
            newStack.push(*i);
        }
    }

    countData = newStack;

}
