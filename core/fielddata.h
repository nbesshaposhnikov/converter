#ifndef FIELDDATA_H
#define FIELDDATA_H

#include <QByteArray>
#include "exception.h"
#include "converterenum.h"
#include "basictypes.h"

#include <QStack>

namespace Converter{

class Structure;



class FieldData
{
private:
    QString name;
    DataType type;
    quint64 nSize;
    QStack <QString>  countData;
    QString countString;
    bool b_isGlobal;
    bool b_isArray;
    bool b_isExternal;
    bool b_isNumberCount;
    QByteArray data;
    QString childName;
    Structure *child;
    Structure *parent;
    ConverterEnum *enumData;
    void setType(const QString &dataType,bool isUnsigned);




public:
    FieldData():
        name(QString()),
        type(_none),
        nSize(0),
        countData(QStack <QString>()),
        countString(QString()),
        b_isGlobal(false),
        b_isArray(false),
        b_isExternal(false),
        b_isNumberCount(false),
        data(QByteArray()),
        childName(QString()),
        child(0),
        parent(0),
        enumData(0)
    {}

    FieldData(const FieldData &f):
        name(f.name),
        type(f.type),
        nSize(f.nSize),
        countData(f.countData),
        countString(f.countString),
        b_isGlobal(f.b_isGlobal),
        b_isArray(f.b_isArray),
        b_isExternal(f.b_isExternal),
        b_isNumberCount(f.b_isNumberCount),
        data(f.data),
        childName(f.childName),
        child(f.child),
        parent(f.parent),
        enumData(f.enumData)
    {}

    FieldData(QString n,QString t,bool isu,QStack <QString> cd,const QString &cs,bool isg,bool isa,bool ise,bool isn):
        name(n),
        type(_none),
        nSize(0),
        countData(cd),
        countString(cs),
        b_isGlobal(isg),
        b_isArray(isa),
        b_isExternal(ise),
        b_isNumberCount(isn),
        data(QByteArray()),
        childName(QString()),
        child(0),
        parent(0),
        enumData(0)
    {
        setType(t,isu);
        nSize = getSize();
    }

    ~FieldData();

    bool operator == (const FieldData& f) {
        return (name==f.name&&type==f.type&&nSize==f.nSize&&countData==f.countData&&b_isGlobal==f.b_isGlobal&&
                b_isArray==f.b_isArray&&b_isExternal==f.b_isExternal&&data==f.data&&childName==f.childName&&
                child==f.child&&parent==f.parent && countString == f.countString);
    }

    quint64 size() {
        if(type == _string || type == _blob)
            return calcData();

        return nSize;
    }

    int getSize();
    void setSize(int size) {
        nSize = size;
    }

    void setChild(Structure *newChild) {
        child = newChild;
    }

    void setParent(Structure *newParent) {
        parent = newParent;
    }

    void setEnum(ConverterEnum *newEnum) {
        enumData = newEnum;
    }

    Structure *getParent() {
        return parent;
    }

    Structure *getChild() const{
        return child;
    }

    ConverterEnum *getEnum() const{
        return enumData;
    }

    QString &getChildName() {
        return childName;
    }

    QString getChildName() const{
        return childName;
    }

    QString &getName() {
        return name;
    }

    QString getName() const{
        return name;
    }

    QStack<QString> &getCountData() {
        return countData;
    }

    QString &getCountString() {
        return countString;
    }

    bool isArray() const{
        return b_isArray;
    }

    bool &isExternal(){
        return b_isExternal;
    }

    bool isGlobal() const{
        return b_isGlobal;
    }

    bool isNumberCount() const {
        return b_isNumberCount;
    }

    bool isRepeatedType() {
        return (type == _string || type==_char_calcfield ||
                type==_short_calcfield || type==_int_calcfield ||
                type==_int64_calcfield || type == _blob);
    }

    quint64 calcData();



    quint64 count() {
        if(isRepeatedType())// for non-repeat values
            return 1;
        else if(!isArray())//if not repeated
            return 1;

        return calcData();//other(if repeated)
    }


    DataType getType() const
        { return type;  }
    void setType(DataType t){
        type = t;
    }
    //for convert
    void setDataIn(QByteArray newData);
    QByteArray getDataIn();

    //for deconvert
    void setDataOut(QByteArray newData);
    QByteArray getDataOut();
    // for Ariphmetic
    QByteArray& getCalcData(){
        if(type==_float||type==_double||type==_string||type==_struct||type==_enum||type == _blob)
            throw StructException(QObject::tr("Нельзя использовать данные такого типа %1"),name);

        return data;
    }

    QByteArray& getData(){
        return data;
    }

    QString getSqlType(ConvertType convType);

    QByteArray &getBits(const QByteArray &b,int size);


    void markCountData();

};

}
#endif // FIELDDATA_H
