#ifndef GLOBALQUEUE_H
#define GLOBALQUEUE_H
#include <QList>
#include <QByteArray>
#include "exception.h"
namespace Converter {


class GlobalQueue
{
private:
    QList <QByteArray> data;
    int curInd;

public:
    GlobalQueue():
        data(QList <QByteArray> ()),
        curInd(0)
    {}

    void start() {
        curInd = 0;
    }

    void push(QByteArray d) {
        data.push_back(d);
    }

    QByteArray &getData(){
        if(curInd >= data.size())
            throw ConvertException(QObject::tr("������ � ������� ���������� ��������� �����������"));

        return data[curInd++];
    }
};

}
#endif // GLOBALQUEUE_H
