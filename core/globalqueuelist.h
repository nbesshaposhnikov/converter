#ifndef GLOBALQUEUELIST_H
#define GLOBALQUEUELIST_H

#include <QHash>
#include <QString>
#include "globalqueue.h"

namespace Converter {

class GlobalQueueList
{
private:
    QHash <QString,GlobalQueue> list;

public:
    GlobalQueueList();

    void add(const QString &name){
        if(list.contains(name))
            throw StructException(QObject::tr("���������� �������� ''%1'' ��� ����������������"),name);

        list.insert(name,GlobalQueue());
    }

    GlobalQueue &getQueue(const QString &name) {
        if(!list.contains(name))
            throw StructException(QObject::tr("���� �� �������:%1"),name);

        return list.find(name).value();
    }

};

}
#endif // GLOBALQUEUELIST_H
