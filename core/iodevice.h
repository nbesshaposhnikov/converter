#ifndef IODEVICE_H
#define IODEVICE_H

#include <QtCore>
#include "basictypes.h"

namespace Converter {

    const int MAX_ROW_NUM = 64;

    class Structure;

    class IODevice
    {
    public:
        Structure *parent;
        QQueue <QByteArray> dataQueue;
        QString baseName;
        bool isChildren;

        IODevice():
            parent(0),
            dataQueue(QQueue <QByteArray> ()),
            baseName(QString()),
            isChildren(false)
        {}

        IODevice(Structure *parent):
            parent(parent),
            dataQueue(QQueue <QByteArray> ()),
            baseName(QString()),
            isChildren(false)
        {}

        virtual ~IODevice()
        {}

        enum Direction
        {
            Input,
            Output
        };

        virtual QByteArray read(quint64 size = 0) = 0;
        virtual void write(const QByteArray &ba,quint64 size  = 0) = 0;
        virtual void getChar(char *c) = 0;

        virtual void prepareDevice(const QString &source,const QString &dest,const QString &options) = 0;
        virtual void prepareDevice(IODevice *other,const QString &newPath) = 0;
        virtual void closeDevice() = 0;

        QString name()
        {
            return baseName.replace('_',"::");
        }
    };

}
#endif // IODEVICE_H
