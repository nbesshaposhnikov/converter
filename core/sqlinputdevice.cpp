﻿#include "sqlinputdevice.h"
#include "structure.h"


using namespace Converter;

static DatabaseManager *db = 0;



QByteArray SQLInputDevice::read(quint64 size)
{
    return in->read(size);
}

void SQLInputDevice::write(const QByteArray &ba,quint64 /*size*/)
{
    if(ba == QByteArray("\n")) {
        QByteArray outData;
        while(!dataQueue.isEmpty())
            outData.append("'" + dataQueue.dequeue() + "',");

        outData.chop(1);
        db->exec(insertHeader.arg(QObject::tr(outData.data())));
        dataQueue.clear();
    } else
        dataQueue.enqueue(ba);
}

void SQLInputDevice::prepareDevice(const QString &source,const QString &dest,const QString &options)
{
    baseName = dest;

    tableName = baseName;
    tableName.replace('.','_');

    in = new QFile(source);

    if(!in->open(QIODevice::ReadOnly))
        throw FileException(QObject::tr("Ошибка открытия файла %1:\n%2"),in->fileName(),in->errorString());
    //connect to database
    QStringList list = options.split('\t'/*,QString::SkipEmptyParts*/);


    QString dbName = list.at(0);
    db = DatabaseManager::getInstanse();
    db->connect(dbName,list.at(1),list.at(2),list.at(3),list.at(4),list.at(5),IODevice::Input);
    db->open();
    //create table for insert
    createTable();
    //create insert header
    insertHeader = parent->getFieldNames("");
    insertHeader.chop(1);
    insertHeader = "INSERT INTO "+tableName+" ("+insertHeader+") VALUES (%1)";
}

void SQLInputDevice::closeDevice()
{
    if(!isChildren) {
        in->close();
        write("\n");
    }
}

void SQLInputDevice::prepareDevice(IODevice *other,const QString &newPath)
{
    baseName = ((SQLInputDevice *)other)->baseName+"_"+newPath;

    tableName = baseName;
    tableName.replace('.','_');

    isChildren=true;

    in = ((SQLInputDevice *)other)->in;
    //create table for insert
    createTable();
    //create insert header
    insertHeader = parent->getFieldNames("");
    insertHeader.chop(1);
    insertHeader = "INSERT INTO "+tableName+" ("+insertHeader+") VALUES (%1)";
}


void SQLInputDevice::createTable()
{
    QString queryParams = parent->getTableHeader("");
    queryParams.chop(1);

    db->exec("CREATE TABLE "+tableName+" ("+queryParams+" )");

}


