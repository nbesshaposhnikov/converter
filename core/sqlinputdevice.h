#ifndef SQLINPUTDEVICE_H
#define SQLINPUTDEVICE_H

#include "iodevice.h"
#include "exception.h"
#include "databasemanager.h"

namespace Converter {

    class Structure;

    class SQLInputDevice:public IODevice
    {
    private:
        QFile *in;
        QString tableName;
        QString insertHeader;
    public:
        SQLInputDevice():
            IODevice(),
            in(0),
            tableName(QString()),
            insertHeader(QString())
        {}

        SQLInputDevice(Structure *parent):
            IODevice(parent),
            in(0),
            tableName(QString()),
            insertHeader(QString())
        {}

        ~SQLInputDevice()
        {
            if(!isChildren) {
                if(in != 0)
                    delete in;
                DatabaseManager::deleteInstanse();
            }
        }

        void createTable();

        virtual QByteArray read(quint64 size);
        virtual void write(const QByteArray &ba,quint64 size = 0);
        virtual void getChar(char *c){
            in->getChar(c);
        }

        virtual void prepareDevice(const QString &source,const QString &dest,const QString &options);
        virtual void prepareDevice(IODevice *other,const QString &newPath);
        virtual void closeDevice();



    };

}
#endif // TXTINPUTDEVICE_H
