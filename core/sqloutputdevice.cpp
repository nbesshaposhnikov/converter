﻿#include "sqloutputdevice.h"
#include "structure.h"


using namespace Converter;

static DatabaseManager *db = 0;
static QSqlQuery query;


QByteArray SQLOutputDevice::read(quint64 size)
{
    if(size == 0) {
        dataQueue.clear();
        query.next();

//        if(!query.isValid())
//            throw ConvertException(QObject::tr("Недостаточно данных!"));

        int i = 0;

        while(query.value(i).isValid()) {
            dataQueue.enqueue(to_1251(query.value(i).toString()));
            ++i;
        }
    }
    else {
        if(dataQueue.isEmpty()) {
            throw ConvertException(QObject::tr("Недостаточно данных!"));
        }
        return dataQueue.dequeue();
    }
    return QByteArray();
}

void SQLOutputDevice::write(const QByteArray &ba,quint64 /*size*/)
{
    out->write(ba.data(),ba.size());
}

void SQLOutputDevice::prepareDevice(const QString &source,const QString &dest,const QString &options)
{
    baseName = dest;

    tableName = baseName;
    tableName.replace('.','_');

    out = new QFile(source);

    if(!out->open(QIODevice::WriteOnly))
        throw FileException(QObject::tr("Ошибка открытия файла %1:\n%2"),out->fileName(),out->errorString());
    //connect to database
    QStringList list = options.split('\t'/*,QString::SkipEmptyParts*/);


    QString dbName = list.at(0);
    db = DatabaseManager::getInstanse();
    db->connect(dbName,list.at(1),list.at(2),list.at(3),list.at(4),list.at(5),IODevice::Output);
    db->open();
    //get data
    query = db->exec("SELECT * FROM "+tableName);
    //read first data
    read();
}

void SQLOutputDevice::closeDevice()
{
    if(!isChildren) {
        out->close();
    }
    query = QSqlQuery();

}

void SQLOutputDevice::prepareDevice(IODevice *other,const QString &newPath)
{
    baseName = ((SQLOutputDevice *)other)->baseName+"_"+newPath;

    tableName = baseName;
    tableName.replace('.','_');

    isChildren=true;

    out = ((SQLOutputDevice *)other)->out;
    //get data
    query = db->exec("SELECT * FROM "+tableName);
    //read first data
    read();
}



