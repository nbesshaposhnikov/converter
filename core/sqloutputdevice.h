#ifndef SQLOUTPUTDEVICE_H
#define SQLOUTPUTDEVICE_H

#include "iodevice.h"
#include "exception.h"
#include "databasemanager.h"

namespace Converter {

    class Structure;

    class SQLOutputDevice:public IODevice
    {
    private:
        QFile *out;
        QString tableName;
    public:
        SQLOutputDevice():
            IODevice(),
            out(0),
            tableName(QString())
        {}

        SQLOutputDevice(Structure *parent):
            IODevice(parent),
            out(0),
            tableName(QString())
        {}

        ~SQLOutputDevice()
        {
            if(!isChildren) {
                if(out != 0)
                    delete out;
                DatabaseManager::deleteInstanse();
            }
        }


        virtual QByteArray read(quint64 size = 0);
        virtual void write(const QByteArray &ba,quint64 size = 0);
        virtual void getChar(char *c)
        {}

        virtual void prepareDevice(const QString &source,const QString &dest,const QString &options);
        virtual void prepareDevice(IODevice *other,const QString &newPath);
        virtual void closeDevice();

    };

}
#endif // SQLOUTPUTDEVICE_H
