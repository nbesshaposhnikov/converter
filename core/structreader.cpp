﻿#include "structreader.h"


using namespace Converter;

StructReader::~StructReader()
{

    if(strFile) {
        if(strFile->isOpen())
            strFile->close();

        delete strFile;
    }

//    if(mainStruct)
//        delete mainStruct;

    for(int i=0;i<strList.size();++i)
        delete strList[i];

    for(int i=0;i<enumList.size();++i)
        delete enumList[i];
}

Structure* StructReader::allocStruct()
{
//    switch(type) {
//    case _ConvNone: return 0;
//    case Txt:return (new TxtStructure(parent));
//    case SQL:return (new SQLStructure(parent));
//    //case Excel:return (new ExcelStructure(this));   after TXT make...
//    }
//    return 0;
    return new Structure(parent,type);
}

Structure* StructReader::cloneStruct(Structure* s)
{
//    switch(type) {
//    case _ConvNone: return 0;
//    case Txt:return (new TxtStructure(*(TxtStructure *)s));
//    case SQL:return (new SQLStructure(*(SQLStructure *)s));
//    //case Excel:return (new ExcelStructure());   after TXT make...
//    }
//    return 0;
    return new Structure(*s);
}

void StructReader::readStruct()
{
    strList.clear();
    enumList.clear();

    if(!strFile->open(QIODevice::ReadOnly|QIODevice::Text))
        throw FileException(QObject::tr("Ошибка открытия файла %1 :\n%2"),strPath,strFile->errorString());

    QString lineString;
    QStringList splitData;

    while(!strFile->atEnd()) {
       lineString = QString(strFile->readLine(255));
       ++linesRead;

       splitData = lineString.split(QRegExp("\\s+"),QString::SkipEmptyParts);

       //Check for comment
       if(lineString.trimmed().startsWith("//")||lineString.trimmed().isEmpty())
           continue;

       if(splitData.startsWith("#include"))
       {
            if(splitData.count()>1) {
                QString dir = QFileInfo(strPath).absoluteDir().absolutePath();

                StructReader subReader(parent,type,dir+"/"+QStringRef(&splitData.at(1),1,splitData.at(1).size()-2).toString(),queueList,mainIndex);

                subReader.readStruct();

                for(int i = 0; i < subReader.getStrList().size(); ++i)
                    strList.append(cloneStruct(subReader.getStrList().at(i)));

                strNum += subReader.getStrList().size();

                for(int i = 0; i < subReader.getEnumList().size(); ++i)
                    enumList.append(new ConverterEnum(*(subReader.getEnumList().at(i))));

            } else {
                throw StructException(QObject::tr("Ошибка чтения строки включения файла, строка:%1"),linesRead);
            }
       } else if(splitData.startsWith("struct") || splitData.startsWith("mainstruct")) {

           if(splitData.size() < 2) {
               throw StructException(QObject::tr("Ошибка чтения имени структуры, строка:%1"),linesRead);
           }

           if((!splitData.at(1).endsWith('{'))&&!(splitData.size() > 2 && splitData.at(2)=="{")) {//Check for open "{"
               bool found=false;
               while(!strFile->atEnd()) {
                   lineString = QString(strFile->readLine(255));
                   ++linesRead;
                   if(lineString.trimmed() == "{") {
                       found = true;
                       break;
                   }
               }
               if(!found) {
                   throw StructException(QObject::tr("Невозможно найти ''{'' для структуры:%1") ,splitData.at(1));
               }
           }
           if(splitData.startsWith("mainstruct")) {
               if(mainIndex == -1)
                    mainIndex = strNum;
               else {
                   throw StructException(QObject::tr("Прочитана более чем одна главная структура, строка:%1") ,linesRead);
               }
           }

           QString strName=splitData.at(1);
           strName.replace(QChar('{'),QChar('\0'));
           Structure *mStr = allocStruct();
           mStr->setName(strName);

           readFields(mStr);

           strList.push_back(mStr);
           strNum++;
       } else if(splitData.startsWith("char_enum")||splitData.startsWith("short_enum")||splitData.startsWith("int_enum")
                 || splitData.startsWith("int64_enum")) {
           if(splitData.size() < 2) {
               throw StructException(QObject::tr("Ошибка чтения имени enum, строка:%1"),linesRead);
           }

           if((!splitData.at(1).endsWith('{'))&&!(splitData.size() > 2 && splitData.at(2)=="{")) {//Check for open "{"
               bool found=false;
               while(!strFile->atEnd()) {
                   lineString = QString(strFile->readLine(255));
                   ++linesRead;
                   if(lineString.trimmed() == "{") {
                       found = true;
                       break;
                   }
               }
               if(!found) {
                   throw StructException(QObject::tr("Невозможно найти ''{'' для enum:"), splitData.at(1));
               }
           }


           QString enumName=splitData.at(1);
           enumName.replace(QChar('{'),QChar('\0'));
           ConverterEnum *mEnum = new ConverterEnum(enumName,splitData.at(0));
           readEnum(mEnum);

           enumList.push_back(mEnum);
       }
   }

    strFile->close();
}

void StructReader::readFields(Structure *mStr)
{
    QString lineString;
    QStringList splitData;
    bool endRead = false;

    while(!strFile->atEnd()) {
        if(endRead)
            break;

       lineString = QString(strFile->readLine(255));
       ++linesRead;

       //Check for comment
       if(lineString.trimmed().startsWith("//")||lineString.trimmed().isEmpty())
           continue;
        //chop comment
       if(lineString.contains("//"))
           lineString.chop(lineString.size()-lineString.indexOf("//"));

       if(lineString.trimmed()=="};")
           break;

       if(lineString.endsWith("};")) {//if end of struct in the end of line
           endRead = true;
           lineString.replace("};","");// delete end of struct
       }

       splitData = lineString.split(';',QString::SkipEmptyParts);//Get all types from line
       if(splitData.size() >0 && splitData.at(0) == lineString) {//if ';' not fonded it's true
           throw StructException(QObject::tr("'';'' не найдена, строка:%1"),linesRead);
       }

       for(int i = 0; i < splitData.size(); ++i) {
           if(splitData.at(i).trimmed().isEmpty())
               continue;
            QStringList lineParts = splitData.at(i).trimmed().split(QRegExp("\\s+"),QString::SkipEmptyParts);//@lineParts - field name && it's type && atributes

            if(lineParts.size()<2) {
                throw StructException(QObject::tr("Не достаточно данных для поля, строка:%1"),linesRead);
            }

            bool isGlobal = false,isExternal = false,isArray = false,isUnsigned = false;
            bool isNumberCount = false;
            QString countData = QString(),name,type;

            //get field atributes
            if(lineParts.at(0) == "global") {
                isGlobal = true;
                lineParts.takeAt(0);
            }
            if(lineParts.at(0) == "external") {
                isExternal = true;
                lineParts.takeAt(0);
            }
            if(lineParts.at(0) == "unsigned") {
                isUnsigned = true;
                lineParts.takeAt(0);
            }
            type = lineParts.at(0);
            lineParts.takeAt(0);

            QString nameString = QString();

            for(int j=0;j<lineParts.size();++j) {
                nameString+=lineParts.at(0);
            }
           if(nameString.contains("[")){
                if(!nameString.endsWith("]")) {//search ]
                    throw StructException(QObject::tr("'']'' не найдена, строка:%1"),linesRead);
                }

                isArray = true;
                name = nameString.left(nameString.indexOf("["));
                countData = nameString.mid(nameString.indexOf("[")+1,lineString.indexOf("]")-lineString.indexOf("[")-1);
           }else {
               if(lineParts.size() > 1) {
                   throw StructException(QObject::tr("Слишком много данных, строка:%1"),linesRead);
               }

               name = nameString;
           }
           //register global field
           if(isGlobal)
                registerGlobal(name);

           countData.toULongLong(&isNumberCount);//check fo is number
           mStr->pushField(FieldData(name,type,isUnsigned,getPolandStack(countData),countData,
                                     isGlobal,isArray,isExternal,isNumberCount),linesRead);
       }

   }
}

void StructReader::readEnum(ConverterEnum *mEnum)
{
    QString lineString;
    QStringList splitData;
    bool endRead = false;

    qint64 previousValue = -1;

    while(!strFile->atEnd()) {
        if(endRead)
            break;

        lineString = QString(strFile->readLine(255));
        ++linesRead;

       //Check for comment
       if(lineString.trimmed().startsWith("//")||lineString.trimmed().isEmpty())
           continue;
        //chop comment
       if(lineString.contains("//"))
           lineString.chop(lineString.size()-lineString.indexOf("//"));

       if(lineString.trimmed()=="};")
           break;

       if(lineString.endsWith("};")) {//if end of struct in the end of line
           endRead = true;
           lineString.replace("};","");// delete end of struct
       }

       splitData = lineString.split(',',QString::SkipEmptyParts);//Get all types from line

       for(int i = 0; i < splitData.size(); ++i) {
           if(splitData.at(i).trimmed().isEmpty())
               continue;




            QStringList lineParts = splitData.at(i).trimmed().split(QRegExp("="),QString::SkipEmptyParts);
            if(lineParts.size() > 2) {
                throw StructException(QObject::tr("Не достаточно данных для поля, строка:%1"),linesRead);
            }

            if(lineParts.at(0).trimmed().contains(QRegExp("\\s+")) || (lineParts.size()==2 && lineParts.at(1).trimmed().contains(QRegExp("\\s+")))) {
                throw StructException(QObject::tr("Поле в enum содержит проблеьный символ, строка:%1"),linesRead);
            }

            QString fieldName = lineParts.at(0);
            qint64 value;

            if(lineParts.size() == 1) {
                value = ++previousValue;
            } else {
                bool ok = false;
                value  = lineParts.at(1).toLongLong(&ok);
                if(!ok) { // if not converted from decimal system
                    value = lineParts.at(1).toLongLong(&ok,16);
                    if(!ok) {// if not converted from hexadecimal system - error
                        throw StructException(QObject::tr("Поле enum не является целочисленным, строка:%1"),linesRead);
                    }
                }

                previousValue = value;
            }

            mEnum->push(EnumMember(value,fieldName));
       }

   }
}


void StructReader::link()
{
    if(mainIndex == -1)
        throw StructException(QObject::tr("Отсуствует mainstruct!"));

    mainStruct = cloneStruct(strList[mainIndex]);

    findLink(mainStruct);
    //setting main struct
    for(unsigned int i=0;i < mainStruct->size(); ++i) {
        if(mainStruct->at(i).getType() == _struct)
            mainStruct->at(i).isExternal() = true;
    }
//    //clear after reading
//    for(int i=0;i<strList.size();++i) {
//        delete strList[i];
//    }

//    for(int i=0;i<enumList.size();++i) {
//        delete enumList[i];
//    }

//    strList.clear();
//    enumList.clear();
//    mainIndex = -1;
//    strNum = 0;
//    linesRead = 0;
}

void StructReader::findLink(Structure * str)
{
    for(unsigned int i=0; i < str->size(); ++i) {
        str->at(i).setParent(str);//set parent for fields
        str->at(i).markCountData();
        if(str->at(i).getType() == _struct) {
            bool found = false;
            for(int j=0; j < strList.size(); ++j) {
                if(strList.at(j)->getName() == str->at(i).getChildName()) {
                    found = true;
                    str->at(i).setChild(cloneStruct(strList.at(j)));

                    findLink(str->at(i).getChild());
                    break;
                }//end if
            }// end j loop
            if(!found) { //search for enum
                bool found = false;
                for(int j=0; j < enumList.size(); ++j) {
                    if(enumList.at(j)->getName() == str->at(i).getChildName()) {
                        found = true;
                        str->at(i).setEnum(new ConverterEnum(*enumList.at(j)));
                        str->at(i).setType(_enum);
                        str->at(i).setSize(enumList.at(j)->size());
                        break;
                    }//end if
                }// end j loop


                if(!found) //enum not found
                    throw StructException(QObject::tr("Неизвестный тип данных : %1 "),str->at(i).getChildName());
            }
        }//end if
    }//end i loop
}
