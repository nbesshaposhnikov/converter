#ifndef STRUCTREADER_H
#define STRUCTREADER_H

#include "basictypes.h"
#include "converterenum.h"
#include "exception.h"
#include "structure.h"
#include "globalqueuelist.h"
#include "calculator.h"

#include <QtCore>

namespace Converter {

    class BaseConverter;

    class StructReader
    {
    private:
        BaseConverter *parent;

        ConvertType type;
        //return;
        Structure *mainStruct;
        GlobalQueueList *queueList;

        unsigned int linesRead;
        int mainIndex;
        unsigned int strNum;

        QString strPath;
        QFile *strFile;
        //For reading
        QList <Structure *> strList;
        QList <ConverterEnum *> enumList;

        void readFields(Structure *mStr);
        void readEnum(ConverterEnum *mEnum);


        void findLink(Structure * str);//find for child
        Structure* allocStruct();//alloc Sturcture child for converter type
        Structure* cloneStruct(Structure *s);//clone Structure child for converter type

        void registerGlobal(const QString &name) {
            queueList->add(name);
        }

    public:
        StructReader():
            parent(0),
            type(_ConvNone),
            mainStruct(0),
            queueList(0),
            linesRead(0),
            mainIndex(0),
            strNum(0),
            strPath(QString()),
            strFile(0),
            strList(QList <Structure *>()),
            enumList(QList <ConverterEnum *>())
        {}

        StructReader(BaseConverter *p,ConvertType type,const QString &strPath,GlobalQueueList *queueList,int mainIndex = -1):
            parent(p),
            type(type),
            mainStruct(0),
            queueList(queueList),
            linesRead(0),
            mainIndex(mainIndex),
            strNum(0),
            strPath(strPath),
            strFile(new QFile(strPath)),
            strList(QList <Structure *>()),
            enumList(QList <ConverterEnum *>())
        {}

        ~StructReader();

        void readStruct();
        void link();//link all structs for convertion

        Structure *structure() {
            return mainStruct;
        }

        QList <Structure *> getStrList() {
            return strList;
        }

        QList <ConverterEnum *> getEnumList() {
            return enumList;
        }
    };

}
#endif // STRUCTREADER_H
