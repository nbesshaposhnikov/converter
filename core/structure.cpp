﻿#include "structure.h"
#include "converter.h"

using namespace Converter;

QByteArray Converter::to_1251(QString s)
{
    static QTextCodec *codec=QTextCodec::codecForName("Windows-1251");
    QByteArray ba=codec->fromUnicode(s);
    return ba;
}

void Structure::pushField(const FieldData &data, int row)
{
    QString fName = data.getName();
    if(contains(fName))
        throw StructException("Field %1 already exists! Line : %2", fName,row);

    fields.push_back(data);
}

QByteArray Structure::getHeader() {
    QList <FieldData>::iterator i;
    QByteArray header = QByteArray();

    for(i = fields.begin(); i!=fields.end(); ++i) {
        FieldData& data = (*i);

        quint64 count;
        if(data.getCountString() == QString() || data.isRepeatedType())
            count = 1;
        else {
            if(data.isNumberCount())
                count = data.getCountString().toULongLong();
            else
                count = 1;
        }

        if(data.getType() != _struct) {
            for(quint64 j=0;j<count;++j)
                header+=data.getName() + '\t';
        } else if(!data.isExternal()) {
            QString temp = data.getChild()->getHeader();

            for(quint64 j=0;j<count;++j)
                header += temp;
        }

    }
    return header;
}

IODevice *Structure::getDevice(IODevice::Direction dir)
{
    switch(dir)
    {
        case IODevice::Input : {
            switch(type)
            {
            case Txt:return new TxtInputDevice(this);break;
            case SQL:return new SQLInputDevice(this);break;
            case Excel:return new ExcelInputDevice(this);break;
            }
            break;
        }
        case IODevice::Output : {
            switch(type)
            {
            case Txt:return new TxtOutputDevice(this);break;
            case SQL:return new SQLOutputDevice(this);break;
            case Excel:return new ExcelOutputDevice(this);break;
            }
            break;
        }
    }

    return 0;
}

void Structure::prepare(const QString &source,const QString &dName,const QString &destOptions,IODevice::Direction dir)
{
    options = destOptions;
    destName = dName;
    direction = dir;
    device = getDevice(dir);
    device->prepareDevice(source,destName,destOptions);
}

void Structure::end()
{

    if(device != 0) {
        device->closeDevice();
        delete device;
        device = 0;
    }
}

//Get & set data by child IO Device
void Structure::convertData(int index)
{
    QList <FieldData>::iterator i;

    for(i = fields.begin(); i!=fields.end(); ++i) {
        FieldData& data = (*i);
        if(data.getType() == _struct) {
            if(data.isExternal()) {
                IODevice *newDevice = data.getChild()->getDevice(direction);
                newDevice->prepareDevice(device,data.getName() + QByteArray::number(index)/*options+"/"+outName+"_"+data.getName()+".txt"*/);

                data.getChild()->setDevice(newDevice,direction); //TODO: get out of here direction

                for(quint64 j=0;j<data.count();++j) {
                    data.getChild()->convertData(j);
                    newDevice->write("\n");
                }

                parent->emitBlockCreated(newDevice->name(),type);

                newDevice->closeDevice();
                delete newDevice;
            } else {
                data.getChild()->setDevice(device,direction);

                for(quint64 j=0;j<data.count();++j) {
                    data.getChild()->convertData();
                }

            }


        } else if(data.getType() == _string && data.getCountString() == QString())  {
            char c=-1;
            QByteArray txtData = QByteArray();

            while(c!=0) {
                device->getChar(&c);
                txtData+=c;
            }
            data.setDataIn(txtData);
            device->write(data.getDataIn());
        } else {
            for(quint64 j=0;j<data.count();++j) {
                data.setDataIn(device->read(data.size()));
                device->write(data.getDataIn());
                if(data.isGlobal())
                    parent->queueList()->getQueue(data.getName()).push(data.getData());
            }
        }
    }
}

void Structure::deconvertData(int index)
{
    QList<FieldData>::iterator j;

    for(j=fields.begin();j!=fields.end();++j) {

        FieldData& data = (*j);
        if(data.getType() == _struct) {
            if(data.isExternal()) {
                IODevice *newDevice = data.getChild()->getDevice(direction);
                newDevice->prepareDevice(device,data.getName() + QByteArray::number(index)/*options+"/"+outName+"_"+data.getName()+".txt"*/);

                data.getChild()->setDevice(newDevice,direction); //TODO: get out of here direction

                for(quint64 k=0;k<data.count();++k) {
                    data.getChild()->deconvertData(k);
                    newDevice->read(0);
                }

                parent->emitBlockCreated(newDevice->name(),type);
                newDevice->closeDevice();
                delete newDevice;
            } else {
                data.getChild()->setDevice(device,direction);

                for(quint64 k=0;k<data.count();++k) {
                    data.getChild()->deconvertData();
                }
            }


        } else if(data.getType() == _string && data.getCountString() == QString())  {
            QByteArray ba = device->read(1);
            data.setDataOut(ba);
            data.getCountString() = QString::number(ba.size());
            data.getCountData() = getPolandStack(data.getCountString());
            data.markCountData();

            device->write(data.getDataOut());
        } else {

            for(quint64 k=0;k<data.count();++k) {
                data.setDataOut(device->read(1));
                device->write(data.getDataOut());
                if(data.isGlobal())
                    parent->queueList()->getQueue(data.getName()).push(data.getData());
            }
        }
    }
}


QString Structure::getTableHeader(const QString &parentName,int n)
{
    QList <FieldData>::iterator i;
    QString header = QString();

    for(i = fields.begin(); i!=fields.end(); ++i) {
        FieldData& data = (*i);

        quint64 count;

        if(data.getCountString() == QString() || data.isRepeatedType())
            count = 1;
        else {
            if(data.isNumberCount())
                count = data.getCountString().toULongLong();
            else
                count = 1;
        }

        if(data.getType() != _struct) {
            QString gNumber,number;

            if(n == -1 )
                gNumber = "";
            else
                gNumber = "_"+QString::number(n);

            for(quint64 j=0;j<count;++j) {
                if(count == 1 )
                    number = "";
                else
                    number = "_"+QString::number(j);

                header+=QString("[") + parentName + to_1251(data.getName())+number+ gNumber +"] " + data.getSqlType(type);

                if(type == SQL)
                    header += " NOT NULL,";
                else
                    header += " ,";
            }
        } else if(!data.isExternal()) {
            for(quint64 j=0;j<count;++j)
                header+=data.getChild()->getTableHeader(parentName+data.getName()+"_",j);
        }

    }
    return header;
}

QString Structure::getFieldNames(const QString &parentName, int n)
{
    QList <FieldData>::iterator i;
    QString header = QString();

    for(i = fields.begin(); i!=fields.end(); ++i) {
        FieldData& data = (*i);

        quint64 count;

        if(data.getCountString() == QString() || data.isRepeatedType())
            count = 1;
        else {
            if(data.isNumberCount())
                count = data.getCountString().toULongLong();
            else
                count = 1;
        }

        if(data.getType() != _struct) {
            QString gNumber,number;

            if(n == -1 )
                gNumber = "";
            else
                gNumber = "_"+QString::number(n);

            for(quint64 j=0;j<count;++j) {
                if(count == 1 )
                    number = "";
                else
                    number = "_"+QString::number(j);

                header+=QString("[")+parentName + to_1251(data.getName())+number+ gNumber+"],";
            }
        } else if(!data.isExternal()) {
            for(quint64 j=0;j<count;++j)
                header+=data.getChild()->getFieldNames(parentName+data.getName()+"_",j);
        }

    }
    return header;
}
