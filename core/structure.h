#ifndef STRUCTURE_H
#define STRUCTURE_H

#include <QList>
#include <QFile>
#include <QByteArray>
#include "fielddata.h"
#include "exception.h"
#include "txtinputdevice.h"
#include "txtoutputdevice.h"
#include "sqlinputdevice.h"
#include "sqloutputdevice.h"
#include "excelinputdevice.h"
#include "exceloutputdevice.h"

namespace Converter {

QByteArray to_1251(QString s);

class BaseConverter;

class Structure
{
protected:
    QString name;
    ConvertType type;
    QList <FieldData> fields;

    BaseConverter *parent;

    IODevice::Direction direction;
    QString destName;
    QString options;
    IODevice *device;
private:


public:
    Structure(BaseConverter *p,ConvertType type=_ConvNone):
        name(QString()),
        type(type),
        fields(QList <FieldData>()),
        parent(p),
        direction(IODevice::Input),
        destName(QString()),
        options(QString()),
        device(0)
    {}

    Structure(const Structure &s):
        name(s.name),
        type(s.type),
        fields(s.fields),
        parent(s.parent),
        direction(s.direction),
        destName(s.destName),
        options(s.options),
        device(s.device)
    {}

    ~Structure()
    {
    }


    BaseConverter *getParent() {
        return parent;
    }

    unsigned int size() {
        return fields.size();
    }

    void pushField(const FieldData &data, int row);

    void setName(const QString &n) {
        name = n;
    }

    QString &getName() {
        return name;
    }

    FieldData &at(const int i) {
        return fields[i];
    }

    FieldData &at(const QString &n) {
        QList <FieldData>::iterator i;

        for(i = fields.begin();i!=fields.end();++i) {
            if((*i).getName() == n)
                return (*i);
        }

        throw StructException(QObject::tr("���� %1 �� �������!"), n);
        /*FieldData &f=FieldData();
        return f;*/
    }


    int find(const QString &n) {
        QList <FieldData>::iterator i;
        int j=00;
        for(i = fields.begin(),j=0;i!=fields.end();++i,++j) {
            if((*i).getName() == n)
                return j;
        }
        return -1;
    }


    bool contains(QString &n) {
        QList <FieldData>::iterator i;
        for(i=fields.begin(); i!=fields.end(); ++i){
            if((*i).getName()==n)
                return true;
        }
        return false;
    }

    QByteArray getHeader();
    QString getTableHeader(const QString &parentName, int n = -1);
    QString getFieldNames(const QString &parentName, int n = -1);

    IODevice *getDevice(IODevice::Direction dir);

    void setDevice(IODevice *dev,IODevice::Direction dir)
    {
        device = dev;
        direction = dir;
    }

    void prepare(const QString &source,const QString &dName,const QString &destOptions,IODevice::Direction dir= IODevice::Input);
    //Get & set data by child IO Device
    void convertData(int index = -1);
    void deconvertData(int index = -1);
    void end();
//    void startConvert() throw (StructException,ConvertException,FileException);
//    void startDeconvert() throw (StructException,ConvertException,FileException);
};

}
#endif // STRUCTURE_H
