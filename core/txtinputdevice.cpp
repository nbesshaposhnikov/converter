﻿#include "txtinputdevice.h"
#include "structure.h"


using namespace Converter;

QByteArray TxtInputDevice::read(quint64 size)
{
    return in->read(size);
}

void TxtInputDevice::write(const QByteArray &ba,quint64 /*size*/)
{
    if(ba == QByteArray("\n")) {
        QByteArray outData;
        while(!dataQueue.isEmpty())
            outData.append(dataQueue.dequeue()+"\t");

        outData.append('\n');
        out->write(outData.data());
        dataQueue.clear();
    } else
        dataQueue.enqueue(ba);
}

void TxtInputDevice::prepareDevice(const QString &source,const QString &dest,const QString &options)
{
    basePath = options;
    baseName = dest;

    in = new QFile(source);
    out = new QFile(options+"/"+dest+".txt");

    if(!in->open(QIODevice::ReadOnly))
        throw FileException(QObject::tr("Ошибка открытия файла %1:\n%2"),in->fileName(),in->errorString());

    if(!out->open(QIODevice::WriteOnly|QIODevice::Text))
        throw FileException(QObject::tr("Ошибка открытия файла %1:\n%2"),out->fileName(),out->errorString());

    out->write(parent->getHeader().data());
    out->write("\n");
}

void TxtInputDevice::closeDevice()
{
    if(!isChildren) {
        in->close();
        write("\n");
    }

    out->close();
}

void TxtInputDevice::prepareDevice(IODevice *other,const QString &newPath)
{
    basePath = ((TxtInputDevice *)other)->basePath;
    baseName = ((TxtInputDevice *)other)->baseName+"_"+newPath;
    isChildren=true;

    in = ((TxtInputDevice *)other)->in;
    out = new QFile(basePath+"/"+baseName+".txt");

    if(!out->open(QIODevice::WriteOnly|QIODevice::Text))
        throw FileException(QObject::tr("Ошибка открытия файла %1:\n%2"),out->fileName(),out->errorString());

    out->write(parent->getHeader().data());
    out->write("\n");
}
