#ifndef TXTINPUTDEVICE_H
#define TXTINPUTDEVICE_H

#include "iodevice.h"
#include "exception.h"

namespace Converter {

    class Structure;

    class TxtInputDevice:public IODevice
    {
    private:
        QFile *in;
        QFile *out;
        QString basePath;
    public:
        TxtInputDevice():
            IODevice(),
            in(0),
            out(0),
            basePath(QString())
        {}

        TxtInputDevice(Structure *parent):
            IODevice(parent),
            in(0),
            out(0),
            basePath(QString())
        {}

        ~TxtInputDevice()
        {
            if(!isChildren)
                if(in != 0)
                    delete in;
            if(out != 0)
                delete out;
        }

        virtual QByteArray read(quint64 size);
        virtual void write(const QByteArray &ba,quint64 size = 0);
        virtual void getChar(char *c){
            in->getChar(c);
        }

        virtual void prepareDevice(const QString &source,const QString &dest,const QString &options);
        virtual void prepareDevice(IODevice *other,const QString &newPath);
        virtual void closeDevice();

    };

}
#endif // TXTINPUTDEVICE_H
