﻿#include "txtoutputdevice.h"
#include "structure.h"


using namespace Converter;

QByteArray TxtOutputDevice::read(quint64 size)
{
    if(size == 0) {
        dataQueue.clear();
        QByteArray ba = inStream->readLine(100000).toLocal8Bit();
        dataQueue.append(ba.split('\t'));
    }
    else {
        if(dataQueue.isEmpty())
            throw ConvertException(QObject::tr("Недостаточно данных!"));
        return dataQueue.dequeue();
    }
    return QByteArray();
}

void TxtOutputDevice::write(const QByteArray &ba,quint64 /*size*/)
{
    out->write(ba.data(),ba.size());
}

void TxtOutputDevice::prepareDevice(const QString &source,const QString &dest,const QString &options)
{
    basePath = options;
    baseName = dest;

    inFile = new QFile(options+"/"+dest+".txt");
    out = new QFile(source);

    if(!inFile->open(QIODevice::ReadOnly|QIODevice::Text))
        throw FileException(QObject::tr("Ошибка открытия файла %1:\n%2"),inFile->fileName(),inFile->errorString());

    if(!out->open(QIODevice::WriteOnly))
        throw FileException(QObject::tr("Ошибка открытия файла %1:\n%2"),out->fileName(),out->errorString());

    inStream = new QTextStream(inFile);
    inStream->setCodec("UTF-8");

    read();
    dataQueue.clear();//read header
    read();//read first data
}

void TxtOutputDevice::closeDevice()
{
    if(!isChildren) {
        out->close();
    }

    inFile->close();

}

void TxtOutputDevice::prepareDevice(IODevice *other,const QString &newPath)
{
    basePath = ((TxtOutputDevice *)other)->basePath;
    baseName = ((TxtOutputDevice *)other)->baseName+"_"+newPath;
    isChildren=true;

    out = ((TxtOutputDevice *)other)->out;

    inFile = new QFile(basePath+"/"+baseName+".txt");

    if(!inFile->open(QIODevice::ReadOnly))
        throw FileException(QObject::tr("Ошибка открытия файла %1:\n%2"),inFile->fileName(),inFile->errorString());

    inStream = new QTextStream(inFile);
    inStream->setCodec("UTF-8");

    read();
    dataQueue.clear();//read header
    read();//read first data
}
