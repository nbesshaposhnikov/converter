#ifndef TXTOUTPUTDEVICE_H
#define TXTOUTPUTDEVICE_H

#include "iodevice.h"
#include "exception.h"

namespace Converter {

    class Structure;

    class TxtOutputDevice:public IODevice
    {
    private:
        QTextStream *inStream;
        QFile *inFile;
        QFile *out;
        QString basePath;
    public:
        TxtOutputDevice():
            IODevice(),
            inStream(0),
            inFile(0),
            out(0),
            basePath(QString())
        {}

        TxtOutputDevice(Structure *parent):
            IODevice(parent),
            inStream(0),
            inFile(0),
            out(0),
            basePath(QString())
        {}

        ~TxtOutputDevice()
        {
            if(!isChildren)
                if(out != 0)
                    delete out;
            if(inFile != 0)
                delete inFile;

            if(inStream != 0)
                delete inStream;
        }


        virtual QByteArray read(quint64 size = 0);
        virtual void write(const QByteArray &ba,quint64 size = 0);
        virtual void getChar(char *c){
            char temp;
            *inStream >> temp;
            *c = temp;
        }

        virtual void prepareDevice(const QString &source,const QString &dest,const QString &options);
        virtual void prepareDevice(IODevice *other,const QString &newPath);
        virtual void closeDevice();

    };

}
#endif // TXTOUTPUTDEVICE_H
