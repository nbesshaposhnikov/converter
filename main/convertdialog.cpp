#include "convertdialog.h"
#include "ui_convertdialog.h"

#include <QDateTime>
#include <QDesktopServices>
#include <QMessageBox>
#include <QInputDialog>
#include <QTextCodec>

using namespace Converter;
using namespace Converter::Interface;

ConvertDialog::ConvertDialog(Interface::Group *g,int dir,QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ConvertDialog),
    group(g),
    convertedNum(0),
    direction(dir),
    type(_ConvNone),
    finishedNum(0),
    maxNum(0)
{
    ui->setupUi(this);
    this->setLayout(ui->mainLayout);

    ui->txtTab->setLayout(ui->txtLayout);
    ui->sqlTab->setLayout(ui->sqlLayout);
    ui->excelTab->setLayout(ui->excelLayout);
    ui->datTab->setLayout(ui->datLayout);

    connect(ui->start,SIGNAL(clicked()),this,SLOT(start()));

    for(int i=0;i < SourceNum; ++i) {
        threads[i] = new ConverterThread(this);
        errors[i] = false;
    }

    connect(threads[Dat],SIGNAL(finished()),this,SLOT(onDatThreadFinished()));
    connect(threads[Txt],SIGNAL(finished()),this,SLOT(onTxtThreadFinished()));
    connect(threads[SQL],SIGNAL(finished()),this,SLOT(onSQLThreadFinished()));
    connect(threads[Excel],SIGNAL(finished()),this,SLOT(onExcelThreadFinished()));

    if(!direction) {
        ui->tabwidget->removeTab(0);
    } else {
        //this->setWindowTitle(tr("Деконвертирование"));
        ui->tabwidget->removeTab(1);
        ui->tabwidget->removeTab(1);
        ui->tabwidget->removeTab(1);

        bool ok;
        QStringList list;
        QString source;
        list<<tr("Txt");
        list<<tr("SQL");
        list<<tr("Excel");

        source = QInputDialog::getItem(this,tr("Выберите источник"),tr("Источник: "),list,0,false,&ok);

        if(ok && !source.isEmpty()) {
            if(source=="Txt")
                type=Txt;
            else if(source=="SQL")
                type=SQL;
            else if(source=="Excel")
                type=Excel;
        } else {
            exit(0);
        }
    }

    updateTitle();
}

void ConvertDialog::updateTitle()
{
    QString title("%1 (%2/%3)");

    if(!direction)
        this->setWindowTitle(title.arg(tr("Конвертирование")).arg(convertedNum).arg(group->size()));
    else
        this->setWindowTitle(title.arg(tr("Деконвертирование")).arg(convertedNum).arg(group->size()));
}

void ConvertDialog::start()
{
    ui->start->setDisabled(true);

    if(group->getFileList().size()>0) {
        if(!direction) {
            while(!convert());
        } else {
            while(!deconvert());
        }
    }

}


ConvertDialog::~ConvertDialog()
{
    delete ui;

    for(int i=0;i < SourceNum; ++i) {
        threads[i]->terminate();
        //threads[i]->wait();
        delete threads[i];
    }
}

void ConvertDialog::logError(Exception e,ConvertType t)
{
    errors[t] = true;

    QTextEdit *edit;

    if(!direction) {
        edit = getEditor(t);
    } else {
        edit = ui->datLog;
    }

    edit->setTextColor(QColor(255,0,0));
    edit->append(QTime::currentTime().toString(Qt::SystemLocaleShortDate));


    edit->insertPlainText(" : "+e.header+": ");
    edit->setTextColor(QColor(0,0,0));
    edit->insertPlainText(e.text);

}

QTextEdit *ConvertDialog::getEditor(ConvertType t)
{
    switch(t) {
        case Txt : return ui->txtLog; break;
        case SQL : return ui->sqlLog; break;
        case Excel : return ui->excelLog; break;
        case Dat:case _ConvNone:case SourceNum: return 0;break;
    }
    return 0;
}

void ConvertDialog::logBlockCreated(QString block,ConvertType t)
{
    mutex.lock();
    QTextEdit *edit;

    if(!direction) {
        edit = getEditor(t);
    } else {
        edit = ui->datLog;
    }

    logText(edit,tr("Создан блок: ")+block);
    mutex.unlock();
}


void ConvertDialog::logText(QTextEdit *edit,const QString &text)
{
    edit->setTextColor(QColor(255,0,0));
    edit->append(QTime::currentTime().toString(Qt::SystemLocaleShortDate));

    edit->moveCursor(QTextCursor::End);
    edit->setTextColor(QColor(0,0,0));
    edit->insertPlainText(" : "+text);
}

bool ConvertDialog::convert()
{
    if(convertedNum>=group->getFileList().size()) {
        //give one more chance
        convertedNum = 0;
        finishedNum = 0;
        maxNum= 0;
        ui->start->setDisabled(false);
        return true;
    }

    if(!group->getFileAt(convertedNum).isSelected) {
        ++convertedNum;
        return false;
    }


    return runConvertThreads();
}

bool ConvertDialog::deconvert()
{
    if(convertedNum>=group->getFileList().size()) {
        //give one more chance
        convertedNum = 0;
        finishedNum = 0;
        maxNum= 0;
        ui->start->setDisabled(false);
        return true;
    }
    if(!group->getFileAt(convertedNum).isSelected) {
        ++convertedNum;
        return false;
    }


    return runDeconvertThreads();
}

bool ConvertDialog::runConvertThreads()
{
    maxNum=0;
    finishedNum = 0;
    group->getFileAt(convertedNum)[Converter::Dat].check(true);

    bool partsConverted =0;
    for(int i=0; i < 3; ++i) {
        ConvertType t = static_cast<ConvertType>(i+1);

        if(!group->getFileAt(convertedNum)[Converter::Dat].exists()) {
            logText( getEditor(t),tr("Dat файл ")+group->getFileNames().at(convertedNum)+tr(" не существует."));
            continue;
        }

        if(group->getFileAt(convertedNum)[t].selected() ) {

            if(!ui->ignoreState->isChecked() && group->getFileAt(convertedNum)[t].exists()) {
                bool outStatus = group->getFileAt(convertedNum)[t].check(true);
                bool datStatus = group->getFileAt(convertedNum)[Dat].check(true);
                if(datStatus) {
                    QTextEdit *edit = getEditor(t);
                    //check out file if replace
                    if(outStatus ) {

                        logText(edit,tr("Файл ")+group->getFileNames().at(convertedNum)+tr(" не нуждается в обновлении."));
                        continue;
                    } else {
                        if(QMessageBox::information(this,tr("Внимание"),tr("Заменить файл(группу файлов,БД) ")+group->getFileNames().at(convertedNum)+" ?"
                                                              ,QMessageBox::Yes|QMessageBox::No) != QMessageBox::Yes)
                        {
                            logText(edit,tr("Файл ")+group->getFileNames().at(convertedNum)+tr(" не нуждается в обновлении."));
                            continue;
                        }
                    }
                }
            }
            ++partsConverted;
            runConvertThread(t);
        }
    }
    /*else if(group->getFileAt(convertedNum).txtInfo.exists() && !group->getFileAt(convertedNum).txtInfo.check(true)){
           if(QMessageBox::information(0,tr("Внимание"),tr("Заменить файлы ")+group->getFileNames().at(convertedNum)+" ?"
                                       ,QMessageBox::Ok|QMessageBox::Cancel,QMessageBox::Cancel) == QMessageBox::Ok)
           {
               runThread();
           }
       }*/
    if(partsConverted == 0) {
        ++convertedNum;
        return false;
    }
    return true;
}

bool ConvertDialog::runDeconvertThreads()
{
    /*bool state = */group->getFileAt(convertedNum)[type].check(true);

    if(!group->getFileAt(convertedNum)[type].exists())
    {
        logText(ui->datLog,tr("Источника для файла ")+group->getFileNames().at(convertedNum)+tr(" не существует."));
        ++convertedNum;
        return false;
    }

    if(!group->getFileAt(convertedNum)[type].selected()) {
        ++convertedNum;
        return false;
    }

    if(QFile::exists(group->getFileAt(convertedNum).outPath) && ui->ignoreState->checkState() != Qt::Checked ) {

        if(QMessageBox::information(this,tr("Внимание"),tr("Заменить файл ")+group->getFileNames().at(convertedNum)+" ?"
                                              ,QMessageBox::Yes|QMessageBox::No) != QMessageBox::Yes)
        {
            logText(ui->datLog,tr("Файл ")+group->getFileNames().at(convertedNum)+tr(" не нуждается в обновлении."));
            ++convertedNum;
            return false;
        }
    }

    runDeconvertThread();

    return true;
}

void Converter::systemExec(const QString &text)
{
    static QTextCodec *codec=QTextCodec::codecForName("Windows-1251");
    QByteArray ba=codec->fromUnicode(text);
    char ascii[255];
    memcpy(ascii,ba.data(),ba.size());
    ascii[ba.size()]=0;

    system(ascii);
}

void ConvertDialog::runConvertThread(ConvertType t)
{
        ++maxNum;

        QTextEdit *edit = getEditor(t);
        logText(edit,tr("-----------------------------------"));
        logText(edit,tr("Начало конвертирования файла: ")+group->getFileNames().at(convertedNum));

//        if(threads[t])
//            delete threads[t];

        if(group->getFileAt(convertedNum).headerIndex == -1) {
            logText(edit,tr("Не выбрана структура!"));
            logText(edit,tr("Завершение конвертирования файла: ")+group->getFileNames().at(convertedNum));
            return;
        }

        if(t==SQL && group->getSqlProperties() == SqlProperties()) {
            logText(edit,tr("Не выбрано соединение!"));
            logText(edit,tr("Завершение конвертирования файла: ")+group->getFileNames().at(convertedNum));
        }

        QString strPath = group->getHeaderList().at(group->getFileAt(convertedNum).headerIndex).getFilePath();
        QString source = group->getFileAt(convertedNum).inPath;
        //Decripting

        if(group->getFileAt(convertedNum).isInEdf) {
            source = edfDeCrypt(source);
            if(source == QString()) {
                logText(edit,tr("Невозможно декриптовать файл: ")+group->getFileNames().at(convertedNum));
                return;
            }

        }

        QString destOptions = group->getFileAt(convertedNum).options[t];

        QString dest = group->getFileNames().at(convertedNum);
        //dest.chop(4);

        threads[t]->setConverter(t,0,strPath,source,dest,destOptions);

        if(t==Txt) {
            QDir dir(destOptions);
            dir.cdUp();
            dir.mkpath(destOptions);
        }

//        switch(t) {
//            case Txt : connect(threads[t],SIGNAL(finished()),this,SLOT(onTxtThreadFinished()));break;
//            case SQL : connect(threads[t],SIGNAL(finished()),this,SLOT(onSQLThreadFinished()));break;
//            case Excel : connect(threads[t],SIGNAL(finished()),this,SLOT(onExcelThreadFinished()));break;
//            case Dat:case _ConvNone:case SourceNum: break;
//        }

        connect(threads[t]->getConverter(),SIGNAL(convertError(Exception,ConvertType)),
                this,SLOT(logError(Exception,ConvertType)));
        connect(threads[t]->getConverter(),SIGNAL(blockCreated(QString,ConvertType)),
                this,SLOT(logBlockCreated(QString,ConvertType)));
        threads[t]->start();
}



void ConvertDialog::runDeconvertThread()
{
    logText(ui->datLog,tr("-----------------------------------"));
    logText(ui->datLog,tr("Начало деконвертирования файла: ")+group->getFileNames().at(convertedNum));

    if(group->getFileAt(convertedNum).headerIndex == -1) {
        logText(ui->datLog,tr("Не выбрана структура!"));
        logText(ui->datLog,tr("Завершение деконвертирования файла :")+group->getFileNames().at(convertedNum));
        return;
    }

    QString strPath = group->getHeaderList().at(group->getFileAt(convertedNum).headerIndex).getFilePath();
    QString source= group->getFileAt(convertedNum).outPath;
    QString destOptions = group->getFileAt(convertedNum).options[type];

    QString dest = group->getFileNames().at(convertedNum);
    //dest.chop(4);

    threads[Dat]->setConverter(type,1,strPath,source,dest,destOptions);

//    connect(threads[Dat],SIGNAL(finished()),this,SLOT(onDatThreadFinished()));
    connect(threads[Dat]->getConverter(),SIGNAL(convertError(Exception,ConvertType)),
            this,SLOT(logError(Exception,ConvertType)));
    connect(threads[Dat]->getConverter(),SIGNAL(blockCreated(QString,ConvertType)),
            this,SLOT(logBlockCreated(QString,ConvertType)));

    threads[Dat]->start();
}

void ConvertDialog::onTxtThreadFinished()
{
    mutex.lock();
    logText(ui->txtLog,tr("Завершение конвертирования файла: ")+group->getFileNames().at(convertedNum));
    ++finishedNum;

    if(group->getFileAt(convertedNum).isInEdf) {
        QFile::remove(group->getFileAt(convertedNum).inPath + ".decrypt");
    }

    if(!errors[Txt]) {
        group->getFileAt(convertedNum)[Txt].upadateOldDate();
        group->getFileAt(convertedNum)[Dat].upadateOldDate();
    } else {
        removeFolder(QDir(group->getFileAt(convertedNum).options[Txt]));
    }

    if(ui->openFolder->isChecked() && !errors[Txt])
        QDesktopServices::openUrl(QUrl(QString("file:///")+group->getFileAt(convertedNum).options[Txt]));

    errors[Txt] = false;

    if(finishedNum==maxNum) {
        ++convertedNum;
        updateTitle();
        while(!convert());
    }

    mutex.unlock();
}

void ConvertDialog::onSQLThreadFinished()
{
    mutex.lock();
    logText(ui->sqlLog,tr("Завершение конвертирования файла: ")+group->getFileNames().at(convertedNum));
    ++finishedNum;

    if(group->getFileAt(convertedNum).isInEdf) {
        QFile::remove(group->getFileAt(convertedNum).inPath + ".decrypt");
    }

    if(!errors[SQL]) {
        group->getFileAt(convertedNum)[SQL].upadateOldDate();
        group->getFileAt(convertedNum)[Dat].upadateOldDate();
    }

    errors[SQL] = false;

    if(finishedNum==maxNum) {
        ++convertedNum;
        updateTitle();
        while(!convert());
    }

    mutex.unlock();
}

void ConvertDialog::onExcelThreadFinished()
{
    mutex.lock();
    logText(ui->excelLog,tr("Завершение конвертирования файла: ")+group->getFileNames().at(convertedNum));
    ++finishedNum;

    if(group->getFileAt(convertedNum).isInEdf) {
        QFile::remove(group->getFileAt(convertedNum).inPath + ".decrypt");
    }

    if(!errors[Excel]) {
        group->getFileAt(convertedNum)[Excel].upadateOldDate();
        group->getFileAt(convertedNum)[Dat].upadateOldDate();
    } else {
        QFile::remove(group->getFileAt(convertedNum).options[Excel]+group->getFileAt(convertedNum).name+".xlsx");
    }

    if(ui->openFolder->isChecked() && !errors[Excel])
        QDesktopServices::openUrl(QUrl(QString("file:///")+group->getFileAt(convertedNum).options[Excel]+group->getFileAt(convertedNum).name+".xlsx"));

    errors[Excel] = false;

    if(finishedNum==maxNum) {
        ++convertedNum;
        updateTitle();

        while(!convert());
    }

    mutex.unlock();
}

void ConvertDialog::onDatThreadFinished()
{
    mutex.lock();

    logText(ui->datLog,tr("Завершение деконвертирования файла: ")+group->getFileNames().at(convertedNum));
    if(!errors[type]) {
        if(group->getFileAt(convertedNum).isOutEdf) {
            if(edfCrypt(group->getFileAt(convertedNum).outPath) == QString())
                logText(ui->datLog,tr("Не возможно закриптовать файл: ")+group->getFileNames().at(convertedNum));

        } else {
            QString datFileName  = group->getFileAt(convertedNum).outPath;

            datFileName.chop(3);
            datFileName+="dat";
            QFile::rename(group->getFileAt(convertedNum).outPath,datFileName);
        }
        group->getFileAt(convertedNum)[type].upadateOldDate();
    }

    errors[type] = false;

    if(ui->openFolder->isChecked())
        QDesktopServices::openUrl(QUrl(QString("file:///")+
                                       QFileInfo(group->getFileAt(convertedNum).outPath).absoluteDir().absolutePath()));

    ++convertedNum;
    updateTitle();

    while(!deconvert());

    mutex.unlock();
}
