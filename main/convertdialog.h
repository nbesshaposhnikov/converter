#ifndef CONVERTDIALOG_H
#define CONVERTDIALOG_H

#include <QDialog>
#include <QTextEdit>
#include <QMutex>

#include "converterthread.h"
#include "group.h"
#include "edftools.h"

namespace Ui {
class ConvertDialog;
}

namespace Converter {

    void systemExec(const QString &text);

    class ConvertDialog : public QDialog
    {
        Q_OBJECT

    public:
        explicit ConvertDialog(Interface::Group *g,int dir,QWidget *parent = 0);
        ~ConvertDialog();



    private:
        Ui::ConvertDialog *ui;

        Interface::Group *group;
        int convertedNum;
        int direction;
        ConvertType type;
        //threads


        int finishedNum;
        int maxNum;

        QMutex mutex;


        ConverterThread *threads[Converter::SourceNum];
        bool errors[Converter::SourceNum];


        void logText(QTextEdit *edit,const QString &text);
        QTextEdit *getEditor(ConvertType t);



        bool convert();
        bool deconvert();
        bool runConvertThreads();
        bool runDeconvertThreads();

        void updateTitle();

        void runConvertThread(ConvertType t);
        void runDeconvertThread();
//        void runTxtThread();
//        void runSqlThread();
//        void runExcelThread();
//        void runDatThread();
    private slots:

        void onTxtThreadFinished();
        void onSQLThreadFinished();
        void onExcelThreadFinished();
        void onDatThreadFinished();

        void logError(Exception e,ConvertType t);
        void logBlockCreated(QString block,ConvertType type);

        void start();
    };
}
#endif // CONVERTDIALOG_H
