#include "edftools.h"

//using namespace Converter::Interface;



const char  cryptBytes [] = {0x10,0x57,0x4F,0x74,0x67,0x6D,0x43,0x67,0x4E,0x6C,0x45,0x74,0x55,0x66,0x54,0x71,
                           0x52,0x39,0x41,0x4C,0x4C,0x5A,0x42,0x72,0x6E,0x69,0x6F,0x4F,0x69,0x7A,0x66,
                           0x63,0x76,0x37,0x38,0x42,0x47,0x48,0x57,0x6E,0x2B,0x50,0x4B,0x48,0x49,0x79,
                           0x4B,0x63,0x39,0x67,0x74,0x39,0x73,0x47,0x36,0x58,0x73,0x4C,0x65,0x35,0x55,
                           0x51,0x59,0x52,0x38,0x50,0x58,0x6D,0x6F,0x47,0x4C,0x66,0x6F,0x80,0xA6,0x90,
                           0xCD,0x8E,0x20,0x71,0x54,0x4F,0x18,0x98,0x30,0x57,0x40,0x53,0x48,0x6E,0x51,
                           0x6A,0x4C,0x4B,0x4D,0x74,0x67,0x46,0x4B,0x63,0x51,0x4A,0x4B,0x6D,0x50,0x51,
                           0x32,0x51,0x55,0x55,0x46,0x57,0x34,0x6A,0x79,0x68,0x6B,0x33,0x50,0x70,0x55,
                           0x58,0x31,0x72,0x4B,0x67,0x6B,0x45,0x74,0x4F,0x61,0x6B,0x42,0x49,0x34,0x36,
                           0x2E,0x5C,0x6D,0x61,0x70,0x5C,0x4E,0x65,0x75,0x74,0x72,0x61,0x6C,0x42,0x5C,
                           0x4E,0x65,0x75,0x74,0x72,0x61,0x6C,0x42,0x2E,0x42,0x53,0x50,0x50,0x44,0x41,
                           0x50,0x6A,0x32,0x47,0x75,0x54,0x70,0x52,0x69,0x53,0x75,0x41,0x66,0x56,0x49,
                           0x5A,0x67,0x63,0x4E,0x65,0x35,0x6C,0x5A,0x67,0x46,0x6D,0x54,0x4E,0x47,0x72,
                           0x30,0x32,0x79,0x53,0x34,0x61,0x75,0x61,0x51,0x72,0x77,0x4B,0x34,0x67,0x48,
                           0x6B,0x49,0x59,0x6F,0x54,0x61,0x79,0x34,0x68,0x4C,0x63,0x33,0x4D,0x66,0x4E,
                           0x6D,0x57,0x7A,0x44,0x65,0x4B,0x4A,0x74,0x37,0x51,0x35,0x52,0x39,0x79,0x63,
                           0x75,0x74,0x66,0x6D,0x55,0x75,0x53,0x2B,0x62,0x59,0x2B,0x39,0x39,0x7A,0x41,0xBC,0x24};

const char key [] = {0x1,0x2,0x4,0x8,0x10,0x20,0x40,0x80};

const char *copyright = "RF Online by OdinTeam s(^O^)z";

const char *CRYPT_EXTISNSION = ".decrypt";

QString edfCrypt(const QString &fileName)
{    
    QFile inFile(fileName);
    inFile.rename(fileName+CRYPT_EXTISNSION);

    if(!inFile.open(QIODevice::ReadOnly))
        return QString();

    int fileSize = inFile.size();

    QByteArray inFileData = inFile.readAll();
    QByteArray cryptData(cryptBytes);

    int i=0;

    do
    {
        char c = cryptData[(i+1)%256];
        if ( i & 1 )
           inFileData[i] = inFileData[i] - c;
        else
           inFileData[i] = inFileData[i] + c;

        ++i;
    }
    while ( i < fileSize );

    i=0;

    do
    {
        char temp = cryptData[i];
        cryptData[i] = cryptData[i+1];
        cryptData[i+1] = temp;
        i += 2;
    }
    while ( i < 0x100 );


    QByteArray finishData(0x100,0);
    i = 0;
    int j = 255;

    do
    {
        if ( i & 1 )
            cryptData[j] = cryptData[j]- key[(i+1) & 7];
        else
            cryptData[j] = cryptData[j]+ key[(i+1) & 7];

        finishData[i] = cryptData[j];

        if ( (i-1) & 1 )
            cryptData[j-1] =cryptData[j-1]- key[(i+2) & 7];
        else
            cryptData[j-1] =cryptData[j-1]+ key[(i+2) & 7];

        finishData[i+1] = cryptData[j-1];

        if ( (i&1) == 1 )
            cryptData[j-2] = cryptData[j-2] - key[(i+3) & 7];
        else
            cryptData[j-2] =cryptData[j-2]+ key[(i+3) & 7];

        finishData[i+2] = cryptData[j-2];

        if ( ((i-1)&1) == 1 )
            cryptData[j-3] =cryptData[j-3]- key[(i-4) & 7];
        else
            cryptData[j-3] =cryptData[j-3]+ key[(i-4) & 7];

        finishData[i+3] = cryptData[j-3];

        j-=4;
        i+=4;
    }
    while ( i < 0x100 );


    inFile.close();

    QFile outFile(fileName);

    if(!outFile.open(QIODevice::WriteOnly))
        return QString();

    outFile.write(copyright,29);
    outFile.write((char *)&fileSize,4);
    outFile.write(inFileData);
    outFile.write(finishData);

    outFile.close();

    //deleting encript file
    inFile.remove();

    return outFile.fileName();
}

QString edfDeCrypt(const QString &fileName)
{
    QFile inFile(fileName);
    if(!inFile.open(QIODevice::ReadOnly))
        return QString();

    //int fileSize = inFile.size();

    inFile.read(29);//reading odin header


    int blockSize = *(int *)(inFile.read(sizeof(int)).data());

    QByteArray inFileData = inFile.read(blockSize);//read main part

    QByteArray finishData = inFile.read(256);//read finish Data
    QByteArray decryptData(256,0);

    int  i = 0;
    int j = 255;
    //decripting decryptData
     do
     {
       if ( i & 1 )
         finishData[i] = finishData[i]+ key[(i+1) & 7];
       else
         finishData[i] = finishData[i]- key[(i+1) & 7];

       decryptData[j] = finishData[i];

       if ( (i-1) & 1 )
         finishData[i+1] =finishData[i+1]+ key[(i+2) & 7];
       else
         finishData[i+1] =finishData[i+1]- key[(i+2) & 7];

       decryptData[j-1] = finishData[i+1];

       if ( (i&1) == 1 )
         finishData[i+2] = finishData[i+2] + key[(i+3) & 7];
       else
         finishData[i+2] =finishData[i+2]- key[(i+3) & 7];

       decryptData[j-2] = finishData[i+2];

       if ( ((i-1)&1) == 1 )
         finishData[i+3] =finishData[i+3]+ key[(i-4) & 7];
       else
         finishData[i+3] =finishData[i+3]- key[(i-4) & 7];

       decryptData[j-3] = finishData[i+3];

       j-=4;
       i+=4;
     }
     while ( i < 0x100);


    i=0;

    do
    {
    char temp = decryptData[i];
    decryptData[i] = decryptData[i+1];
    decryptData[i+1] = temp;
    i += 2;
    }
    while ( i < 0x100 );


    i=0;
    //decrypt file
     do
     {
       char c = decryptData[(i+1)%256];
       if ( i & 1 )
           inFileData[i] = inFileData[i] + c;
       else
           inFileData[i] = inFileData[i] - c;
       ++i;
     }
     while ( i < blockSize );

    inFile.close();

    QFile outFile(fileName+CRYPT_EXTISNSION);

    if(!outFile.open(QIODevice::WriteOnly))
        return QString();

    outFile.write(inFileData);

    outFile.close();

    return outFile.fileName();

}
