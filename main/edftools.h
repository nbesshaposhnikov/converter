#ifndef EDFTOOLS_H
#define EDFTOOLS_H

#include <QtCore>



//namespace Converter {
//    namespace Interface {


    extern QString edfCrypt(const QString &fileName);
    extern QString edfDeCrypt(const QString &fileName);

//    }//end namespace Converter::Interface
//}//end namespace Converter
#endif // EDFTOOLS_H
