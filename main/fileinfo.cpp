﻿#include "fileinfo.h"
#include "sqlmanager.h"

#include <QFileInfo>
#include <QDir>
#include <QFile>
#include <QTextStream>
#include <QDateTime>
#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>
#include <QMessageBox>
#include <QDebug>
#include <QCoreApplication>

const QString VERSIONS_FOLDER = "Versions";

using namespace Converter;
using namespace Converter::Interface;

bool ResourceInfo::check(bool reload)
{
    if(reload) {
        loadDate();
        loadOldDate();
    }

    if(!exists())
        return false;

    return date==oldDate;
    //return false;
}

void ResourceInfo::loadDate()
{
    date.clear();

    switch(type) {
    case Dat : loadFileDates();break;
    case Excel : loadFileDates();break;
    case Txt: loadTxtDates();break;
    case SQL :loadSQLDates(); break;
    case _ConvNone:case SourceNum: break;
    }
}

void ResourceInfo::loadOldDate()
{
    oldDate.clear();

    switch(type) {
    case Dat : loadFileOldDates();break;
    case Excel : loadFileOldDates();break;
    case Txt: loadTxtOldDates();break;
    case SQL :loadSQLOldDates(); break;
    case _ConvNone:case SourceNum: break;
    }
}

void ResourceInfo::clearVerison()
{
    switch(type) {
    case Dat: clearFileVerison();break;
    case Excel: clearFileVerison();break;
    case Txt: clearTxtVerison();break;
    case SQL: clearSQLVerison(); break;
    case _ConvNone:case SourceNum: break;
    }
}

void ResourceInfo::upadateOldDate()
{
    if(date==oldDate && (!date.isEmpty() || !oldDate.isEmpty()))
        return;

    loadDate();

    clearVerison();

    loadOldDate();
}

void ResourceInfo::loadFileOldDates()
{
    if(!isExist)
        return;

    QString folder = QFileInfo(source).absoluteDir().absolutePath();
    folder+="/" + VERSIONS_FOLDER;

    if(!QDir(folder).exists()) {
        QDir(QFileInfo(source).absoluteDir()).mkdir(VERSIONS_FOLDER);
    }

    QString fileName = QFileInfo(source).fileName();
    QString verFileName = folder+"/"+fileName+".ver";

    QFile verFile(verFileName);

    if(!verFile.exists()) {
        verFile.open(QIODevice::WriteOnly);
        QTextStream stream(&verFile);

        QDateTime time = QFileInfo(source).lastModified();

        stream << time.toString(Qt::ISODate);
        oldDate << time.toTime_t();

        verFile.close();
    } else {
        verFile.open(QIODevice::ReadOnly);

        QString timeString = QString(verFile.readLine(10000));

        if(!timeString.trimmed().isEmpty())
            oldDate << QDateTime::fromString(timeString.trimmed(),Qt::ISODate).toTime_t();

        verFile.close();
    }
}

void ResourceInfo::loadSQLOldDates()
{
    oldDate.append(0); //HOW TO DO I DON'T KNOW
}

void ResourceInfo::loadTxtOldDates()
{
    if(!isExist)
        return;

    QString folder = source + "/" + VERSIONS_FOLDER;

    if(!QDir(folder).exists()) {
        QDir(QDir(source).absolutePath()).mkdir(VERSIONS_FOLDER);
    }

    QString fileName = QFileInfo(source).fileName();
    QString verFileName = folder+"/"+fileName+".ver";

    QFile verFile(verFileName);



    if(!verFile.exists()) {
        verFile.open(QIODevice::WriteOnly);
        QTextStream stream(&verFile);

        QDir dir(source,fileName+"*.txt",QDir::Name,QDir::Files);
        QFileInfoList fileList = dir.entryInfoList();

        for(int i=0;i < fileList.size(); ++i) {
            QDateTime time = fileList.at(i).lastModified();

            stream /*<< fileList.at(i).fileName() << "\t" */<< time.toString(Qt::ISODate) << "\n";
            oldDate << time.toTime_t();
        }

        verFile.close();
    } else {
        verFile.open(QIODevice::ReadOnly);

        while(!verFile.atEnd()) {
            QString timeString = QString(verFile.readLine(10000));

            if(!timeString.trimmed().isEmpty())
                oldDate << QDateTime::fromString(timeString.trimmed(),Qt::ISODate).toTime_t();
        }

        verFile.close();
    }

}

void ResourceInfo::loadFileDates()
{
    isExist = QFile::exists(source);

    if(isExist)
        date << QFileInfo(source).lastModified().toTime_t();
}

void ResourceInfo::loadSQLDates()
{
    if(sql == SqlProperties())
        return;

    SQLManager manager(0,sql);

    isExist = manager.databaseExists(source);
    date.append(1);

}

void ResourceInfo::loadTxtDates()
{
    isExist = QDir(source).exists();

    if(isExist) {
        QString fileName = QFileInfo(source).fileName();
        QDir dir(source,fileName+"*.txt",QDir::Name,QDir::Files);
        QFileInfoList fileList = dir.entryInfoList();

        for(int i=0;i < fileList.size(); ++i) {
            date << fileList.at(i).lastModified().toTime_t();
        }

    }
}

void ResourceInfo::clearFileVerison()
{
    QString fileName = QFileInfo(source).absoluteDir().absolutePath()
            + "/" + VERSIONS_FOLDER + "/" + QFileInfo(source).fileName()+".ver";

    QFile::remove(fileName);
}

void ResourceInfo::clearTxtVerison()
{
    QString fileName = source + "/" + VERSIONS_FOLDER + "/" + QFileInfo(source).fileName()+".ver";
    QFile::remove(fileName);
}

void ResourceInfo::clearSQLVerison()
{

}

void ResourceInfo::remove()
{
    if(!isExist)
        return;

    switch(type) {
    case Dat :
    case Excel : QFile::remove(source);break;
    case Txt: removeFolder(source);break;
    case SQL : {
        if(sql == SqlProperties())
            return;

        SQLManager manager(0,sql);
        manager.removeDatabase(source);

    }
        break;
    case _ConvNone:case SourceNum: break;
    }
}

//Функция удаления папки
int Converter::Interface::removeFolder(QDir dir)
{
   int res = 0;
   //Получаем список каталогов
   QStringList lstDirs  = dir.entryList(QDir::Dirs  |
                                   QDir::AllDirs |
                                   QDir::NoDotAndDotDot);
   //Получаем список файлов
   QStringList lstFiles = dir.entryList(QDir::Files);
   //Удаляем файлы
   foreach (QString entry, lstFiles)
   {
      QString entryAbsPath = dir.absolutePath() + "/" + entry;
      QFile::remove(entryAbsPath);
   }
   //Для папок делаем рекурсивный вызов
   foreach (QString entry, lstDirs)
   {
      QString entryAbsPath = dir.absolutePath() + "/" + entry;
      Converter::Interface::removeFolder(QDir(entryAbsPath));
   }
   //Удаляем обрабатываемую папку
   if (!QDir().rmdir(dir.absolutePath()))
   {
      res = 1;
   }
   return res;
}
