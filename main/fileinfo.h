#ifndef FILEINFO_H
#define FILEINFO_H

#include <QString>
#include <QDateTime>
#include "converter.h"
#include "settings.h"
#include <QDir>

namespace Converter {
    namespace Interface {

    int removeFolder(QDir dir);

    class ResourceInfo
    {
    public:
        ResourceInfo():
            type(Converter::_ConvNone),
            source(QString()),
            sql(SqlProperties()),
            date(QList<uint>()),
            oldDate(QList<uint>()),
            isSelected(false),
            isExist(false)
        {}

        ResourceInfo(ConvertType t,const QString &s,const SqlProperties &p):
            type(t),
            source(s),
            sql(p),
            date(QList<uint>()),
            oldDate(QList<uint>()),
            isSelected(false),
            isExist(false)
        {
            loadDate();
            loadOldDate();
            isSelected = isExist;
        }

        void loadDate();
        void loadOldDate();
        void upadateOldDate();

        bool check(bool reload=false);

        bool exists() {
            return isExist;
        }

        bool &selected() {
            return isSelected;
        }

        void remove();
    private:
        ConvertType type;
        QString source;
        SqlProperties sql;

        QList<uint> date;
        QList<uint> oldDate;

        bool isSelected;

        bool isExist;

        void loadFileOldDates();
        void loadSQLOldDates();
        void loadTxtOldDates();

        void loadFileDates();
        void loadSQLDates();
        void loadTxtDates();

        void clearVerison();

        void clearFileVerison();
        void clearTxtVerison();
        void clearSQLVerison();


    };


    class FileInfo
    {
    public:

       int headerIndex;
       bool isInEdf;
       bool isOutEdf;
       bool isSelected;

       QString inPath;
       QString outPath;
       QString name;

       QString options[Converter::SourceNum];

       ResourceInfo resourceInfo[Converter::SourceNum];
    public:
       FileInfo():
           headerIndex(-1),
           isInEdf(false),
           isOutEdf(false),
           isSelected(true)
       {}

       FileInfo(const QString &path,const QString &iPath,const QString &oPath,
		const QString &n, int hIndex,bool isEdf,const SqlProperties &p,
		const QString &sqlPath):
           headerIndex(hIndex),
           isInEdf(isEdf),
           isOutEdf(isEdf),
           isSelected(true),
           inPath(iPath),
           outPath(oPath),
           name(n)
       {

           QString sqlName = sqlPath+"_"+name;
           sqlName.replace(".","_");

           options[Converter::Dat] = path+"/"+name;
           options[Converter::Txt] = path+"/"+name;
           options[Converter::SQL] = sqlName+ "\t" +p.toString();
           options[Converter::Excel] = path;

           resourceInfo[Converter::Dat] = ResourceInfo(Dat,inPath,p);
           resourceInfo[Converter::Txt] = ResourceInfo(Txt,options[Converter::Txt],p);
           resourceInfo[Converter::SQL] = ResourceInfo(SQL,sqlName,p);
           resourceInfo[Converter::Excel] = ResourceInfo(Excel,options[Converter::Excel]+"/"+name+".xlsx",p);
       }

       ResourceInfo &operator [] (ConvertType type) {
           return resourceInfo[type];
       }

       void checkStates() {
           for(int i=0;i<SourceNum;++i)
                resourceInfo[i].check(true);
       }

//       FileInfoNode(const QString &dp,const QString &cp,const QString &n,ConvertType t,bool isEdf,SqlProperties prop = SqlProperties()):
//           datFilePath(dp),
//           datDate(getRealDatDate()),
//           datOldDate(getOldDatDate()),
//           fileName(n),
//           type(t),
//           resFile(alloc(n,cp,prop)),
//           headerIndex(-1),
//           isInEdf(isEdf),
//           isOutEdf(isEdf),
//           isSelected(true)
//       {}

       QString &getFileName() {
           return name;
       }

//       QString getDatPath() const{
//           return datFilePath;
//       }

       int &getHeaderIndex() {
           return headerIndex;
       }

//       ConvertType getType() {
//           return type;
//       }

//       void setType(ConvertType t,SqlProperties prop = SqlProperties()) {
//           type = t;
//           ResultFileInfo *newRes = alloc(fileName,resFile->getPath(),prop);
//           delete resFile;
//           resFile = newRes;
//       }

       bool &getState() {
           return isSelected;
       }

//       QDateTime getRealDatDate();
//       QDateTime getOldDatDate();

//       ~FileInfoNode() {
//          /* if(resFile)
//               delete resFile;*/
//       }
    };

    }//end namespace Converter::Interface
}//end namespace Converter
#endif // FILEINFO_H
