#include "fileinfodelegate.h"

#include <QtWidgets>
#include <QPainter>
using namespace Converter::Interface;

FileInfoDelegate::FileInfoDelegate(QObject *parent) :
    QItemDelegate(parent),
    mGroup(0)
{
}

void FileInfoDelegate::setGroup(Group *group)
{
//    if(group==mGroup)
//        return;

    mGroup=group;

}


//! [1]
QWidget *FileInfoDelegate::createEditor(QWidget *parent,
    const QStyleOptionViewItem & option ,
    const QModelIndex & index ) const
{
    if(mGroup==0)
        return QItemDelegate::createEditor(parent, option, index);;

    switch (index.column()) {
    case 1:
    {
        QComboBox *editor = new QComboBox(parent);
        QStringList list;

        list << "None";
        for(int i=0; i < mGroup->getHeaderList().size(); ++i) {
            list.append(mGroup->getHeaderList().keys().at(i));
        }
        editor->addItems(list);
        editor->setCurrentIndex(mGroup->getFileAt(index.row()).getHeaderIndex()+1);
        return editor;
    }
    default:return QItemDelegate::createEditor(parent, option, index);
    }
}
//! [1]

//! [2]
void FileInfoDelegate::setEditorData(QWidget *editor,
                                    const QModelIndex &index) const
{
    if(mGroup==0) {
        QItemDelegate::setEditorData(editor, index);
        return;
    }

    switch (index.column()) {
    case 2:case 3:
    {
        QComboBox *comboBox = qobject_cast<QComboBox *>(editor);

        comboBox->setCurrentIndex(index.data().toInt());
    }

    default: QItemDelegate::setEditorData(editor, index);
    }

}
//! [2]

//! [3]
void FileInfoDelegate::setModelData(QWidget *editor, QAbstractItemModel *model,
                                   const QModelIndex &index) const
{
    if(mGroup==0) {
        QItemDelegate::setModelData(editor, model, index);
        return;
    }

    switch (index.column()) {
    case 1:
    {
        QComboBox *comboBox = qobject_cast<QComboBox *>(editor);
        int hIndex = comboBox->currentIndex();
        model->setData(index,hIndex);
        break;
    }

    default: QItemDelegate::setModelData(editor, model, index);
    }

    //model->setData(index, qVariantFromValue(starEditor->starRating()));
//    model->setData(index, value, Qt::EditRole);

//    if (qVariantCanConvert<StarRating>(index.data())) {
//        StarEditor *starEditor = qobject_cast<StarEditor *>(editor);
//        model->setData(index, qVariantFromValue(starEditor->starRating()));
//    } else {
       // QItemDelegate::setModelData(editor, model, index);
    //}
}
//! [3]

//! [4]
void FileInfoDelegate::updateEditorGeometry(QWidget *editor,
    const QStyleOptionViewItem &option, const QModelIndex &/* index */) const
{
    editor->setGeometry(option.rect);
}


