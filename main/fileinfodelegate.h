#ifndef FILEINFODELEGATE_H
#define FILEINFODELEGATE_H

#include <QItemDelegate>

#include "group.h"

namespace Converter {
    namespace Interface {

    class FileInfoDelegate : public QItemDelegate
    {
        Q_OBJECT
    public:
        FileInfoDelegate(QObject *parent = 0);


        QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option,
                              const QModelIndex &index) const;

        void setEditorData(QWidget *editor, const QModelIndex &index) const;
        void setModelData(QWidget *editor, QAbstractItemModel *model,
                          const QModelIndex &index) const;

        void updateEditorGeometry(QWidget *editor,
            const QStyleOptionViewItem &option, const QModelIndex &index) const;

        void setGroup(Group *mGroup);
    private:
        Group *mGroup;

    public slots:


    };

    }//end namespace Converter::Interface
}//end namespace Converter
#endif // FILELISTDELEGATE_H
