#include "fileinfomodel.h"
#include <QSize>

using namespace Converter::Interface;

int FileInfoModel::rowCount (const QModelIndex &parent) const
{
    return (mGroup != 0) ? mGroup->getFileList().size():0;
}

int FileInfoModel::columnCount ( const QModelIndex & parent) const
{
    return (mGroup != 0)?7:0;
}

void FileInfoModel::setGroup(Group *group)
{
//    if(group == mGroup)
//        return;
    beginResetModel();
    mGroup = group;
    endResetModel();
}


QVariant FileInfoModel::data ( const QModelIndex & index, int role) const
{
    if (index.row() < 0 || index.column() < 0)
        return QVariant();

    switch (role) {
    case Qt::DisplayRole: return getColumnData(index);break;
    case Qt::EditRole: return getColumnData(index);break;
    case Qt::TextAlignmentRole : return Qt::AlignCenter; break;
//    case Qt::SizeHintRole : return QSize(20,100);break;
    case Qt::DecorationRole: break;
    case Qt::CheckStateRole:
    {
         if (index.column() == 0) {//add a checkbox to cell(x,0)
            return mGroup->getFileAt(index.row()).getState() ? Qt::Checked : Qt::Unchecked;
         } else if (index.column() == 2){
             return mGroup->getFileAt(index.row())[Converter::Txt].selected() ? Qt::Checked : Qt::Unchecked;
         }  else if (index.column() == 3){
             return mGroup->getFileAt(index.row())[Converter::SQL].selected() ? Qt::Checked : Qt::Unchecked;
         } else if (index.column() == 4){
             return mGroup->getFileAt(index.row())[Converter::Excel].selected() ? Qt::Checked : Qt::Unchecked;
         } else if (index.column() == 5){
             return mGroup->getFileAt(index.row()).isInEdf ? Qt::Checked : Qt::Unchecked;
         } else if (index.column() == 6){
             return mGroup->getFileAt(index.row()).isOutEdf ? Qt::Checked : Qt::Unchecked;
         }
    }
    default:
        return QVariant();
    }

    return QVariant();
}

bool FileInfoModel::setData ( const QModelIndex & index, const QVariant & value, int role)
{
    if (role == Qt::EditRole) {
       return setColumnData(index,value);
    } else if(role == Qt::CheckStateRole) {
        Qt::CheckState c = static_cast<Qt::CheckState>(value.toInt());
        if(index.column() == 0) {
            mGroup->getFileAt(index.row()).getState() = (c==Qt::Checked);
        } else if (index.column() == 2){
            mGroup->getFileAt(index.row())[Converter::Txt].selected()   =  (c==Qt::Checked);
        } else if (index.column() == 3){
            mGroup->getFileAt(index.row())[Converter::SQL].selected()   =  (c==Qt::Checked);
        } else if (index.column() == 4){
            mGroup->getFileAt(index.row())[Converter::Excel].selected()   =  (c==Qt::Checked);
        } else if (index.column() == 2){
            mGroup->getFileAt(index.row()).isInEdf   =  (c==Qt::Checked);
        } else if (index.column() == 2){
            mGroup->getFileAt(index.row()).isOutEdf   =  (c==Qt::Checked);
        }

        return true;
    }
    return false;
}

QVariant FileInfoModel::getColumnData(const QModelIndex & index) const
{

    switch(index.column()){
    case 0: return mGroup->getFileAt(index.row()).getFileName();
    case 1:
    {
        int hIndex = mGroup->getFileAt(index.row()).getHeaderIndex();
        if(hIndex == -1)
            return QString("None");

        return mGroup->getHeaderList().keys().at(hIndex);
    }
    }

    return QVariant();
}

bool FileInfoModel::setColumnData(const QModelIndex & index,const QVariant & value)
{

    switch(index.column()){
    case 1:
    {
       int hIndex = value.toInt();
       mGroup->getFileAt(index.row()).getHeaderIndex() = hIndex - 1;
       return true;
    }
    }
    return false;
}

QVariant FileInfoModel::headerData ( int section, Qt::Orientation orientation, int role) const
{
    if (role == Qt::DisplayRole && orientation == Qt::Horizontal) {
        switch (section) {
        case 0: return tr("Имя"); break;
        case 1: return tr("Структура"); break;
        case 2: return tr("Txt"); break;
        case 3: return tr("SQL"); break;
        case 4: return tr("Excel"); break;
        case 5: return tr("In EDF"); break;
        case 6: return tr("Out EDF"); break;
        }
    }
    return QVariant();
}

Qt::ItemFlags FileInfoModel::flags ( const QModelIndex & index ) const
{
    if (!index.isValid())
        return 0;
    if(index.column() == 0 || index.column() == 2 || index.column() == 3 || index.column() == 4 || index.column() == 5 || index.column() == 6)
        return Qt::ItemIsUserCheckable | Qt::ItemIsEnabled; //| Qt::ItemIsSelectable | Qt::ItemIsEditable;
    else if(index.column() == 1)
        return Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsEditable;
    else
        return Qt::ItemIsEnabled;
}

bool FileInfoModel::insertRows ( int row, int count, const QModelIndex & parent)
{
    beginInsertRows(parent,mGroup->getFileList().size(),mGroup->getFileList().size());
    //combTable->insert();
    endInsertRows();
    return true;
}

bool FileInfoModel::removeRows ( int row, int count, const QModelIndex & parent)
{
    beginRemoveRows(parent,mGroup->getFileList().size()-1,mGroup->getFileList().size()-1);
    //combTable->remove();
    endRemoveRows();
    return true;
}
