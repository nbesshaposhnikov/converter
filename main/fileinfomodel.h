#ifndef FILEINFOMODEL_H
#define FILEINFOMODEL_H

#include <QAbstractTableModel>
#include "Group.h"

namespace Converter {
    namespace Interface {

    class FileInfoModel:public QAbstractTableModel
    {
        Q_OBJECT
    public:

        FileInfoModel(QObject *parent = 0):
            QAbstractTableModel(parent),
            mGroup(0)
        {}

        void setGroup(Group *group);
    private:
        Group* mGroup;

        int rowCount ( const QModelIndex & parent = QModelIndex() ) const;
        int columnCount ( const QModelIndex & parent = QModelIndex() ) const;
        QVariant data ( const QModelIndex & index, int role = Qt::DisplayRole ) const;
        QVariant headerData ( int section, Qt::Orientation orientation, int role = Qt::DisplayRole ) const;
        bool setData ( const QModelIndex & index, const QVariant & value, int role = Qt::EditRole );
        Qt::ItemFlags flags ( const QModelIndex & index ) const;
        bool insertRows ( int row, int count, const QModelIndex & parent = QModelIndex() );
        bool removeRows ( int row, int count, const QModelIndex & parent = QModelIndex() );

        //Get Column data
        QVariant getColumnData( const QModelIndex & index) const;
        //Set
        bool setColumnData(const QModelIndex & index,const QVariant & value);
    };

    }//end namespace Converter::Interface
}//end namespace Converter
#endif // FILELISTMODEL_H
