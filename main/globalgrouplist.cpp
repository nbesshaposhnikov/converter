#include "globalgrouplist.h"


using namespace Converter::Interface;

void GlobalGroupList::load(const Settings &settings)
{
    //QCoreApplication::applicationDirPath()
    for(int i=0; i < settings.groupNum(); ++i) {
        append(GroupList(settings.groupNameAt(i),settings.groupPathAt(i)+"/"+settings.groupNameAt(i),settings));
    }

}
