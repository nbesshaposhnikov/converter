#ifndef GLOBALGROUPLIST_H
#define GLOBALGROUPLIST_H

#include "grouplist.h"

namespace Converter {
    namespace Interface {

    class GlobalGroupList:public QList<GroupList>
    {
    private:

    public:
        GlobalGroupList()
        {}

        void load(const Settings &settings);

        GroupList &at(int i) {
            return operator [] (i);
        }
    };

    }//end namespace Converter::Interface
}//end namespace Converter
#endif // GLOBALGROUPLIST_H
