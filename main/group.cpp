﻿#include "group.h"

#include <QDir>
#include <QMessageBox>
#include "grouplist.h"
#include "sqlmanager.h"

const char *OUT_FOLDER= "/Out";
const char *DATIN_FOLDER= "/DatIn";
const char *DATOUT_FOLDER= "/DatOut";

using namespace Converter::Interface;

void Group::loadFileList()
{
    QDir dir= QDir(path+DATIN_FOLDER,"*.dat",QDir::Name,QDir::Files);

    QStringList filters;
    filters << "*.dat" << "*.edf";
    dir.setNameFilters(filters);

    QFileInfoList datFileList = dir.entryInfoList();

    fileList.clear();

    for(int i=0; i < datFileList.size(); ++i) {
        QString fileName = datFileList.at(i).fileName();
        QString ext = datFileList.at(i).suffix();

        QString inPath = datFileList.at(i).filePath();
        QString outPath = path + DATOUT_FOLDER + "/" +fileName;
        QString sqlPath = parentName+"_"+name;
        QString fPath = path + OUT_FOLDER;
        fileList.insert(fileName,FileInfo(fPath,inPath,outPath,fileName,getHeaderIndex(fileName),ext=="edf",sqlProperties,sqlPath));
	//fileList[fileName][Converter::Dat].source = datFileList.at(i).filePath();
	//fileList[fileName][Converter::Dat].destinition = path + DATOUT_FOLDER + "/" +fileName;
	//fileList[fileName][Converter::Dat].options = path + OUT_FOLDER + "/" +fileName;
    }

    loadTxt();
    loadSQL();
    loadExcel();
}

void Group::loadTxt()
{
    QString folderName;
    QDir dir= QDir(path+OUT_FOLDER,"*",QDir::Name,QDir::Dirs|QDir::NoDotAndDotDot);

    QFileInfoList folderList = dir.entryInfoList();

    for(int i=0; i < folderList.size(); ++i) {
        folderName = QDir(folderList.at(i).filePath()).dirName();

        if(folderName == "Versions")
            continue;

        QString ext = QFileInfo(folderName).suffix();

        QString inPath = path + DATIN_FOLDER + "/" + folderName;
        QString outPath = path + DATOUT_FOLDER + "/" + folderName;
        QString sqlPath = parentName+"_"+name;
        QString fPath = path + OUT_FOLDER;

        if(!fileList.contains(folderName))
	    fileList.insert(folderName,FileInfo(fPath,inPath,outPath,folderName,getHeaderIndex(folderName),ext=="edf",sqlProperties,sqlPath));

	//fileList[folderName].setTxt(folderList.at(i).filePath());
	//fileList[fileName][Converter::Txt].source = folderList.at(i).filePath();
	//fileList[fileName][Converter::Txt].destinition = path + DATOUT_FOLDER + "/" +fileName;
	//fileList[fileName][Converter::Txt].options = path + OUT_FOLDER + "/" +fileName;
    }
}

void Group::loadSQL()
{
    if(sqlProperties == SqlProperties())
        return;

    SQLManager manager(0,sqlProperties);
    QStringList dbList = manager.getDatabaseList();

    QString prefix = parentName+"_"+name+"_";

    for(int i=0;i<dbList.size();++i) {
	QString n = dbList.at(i);

        if(n.startsWith(prefix)) {
            QString postfix = n.right(n.size() - prefix.size());
            postfix.replace(postfix.lastIndexOf("_"),1,".");

            QString ext = QFileInfo(postfix).suffix();

            QString inPath = path + DATIN_FOLDER + "/" + postfix;
            QString outPath = path + DATOUT_FOLDER + "/" +postfix;
            QString sqlPath = parentName+"_"+name;
            QString fPath = path + OUT_FOLDER;


            if(!fileList.contains(postfix))
                fileList.insert(postfix,FileInfo(fPath,inPath,outPath,postfix,getHeaderIndex(postfix),ext=="edf",sqlProperties,sqlPath));

        }
    }

}

void Group::loadExcel()
{

}


void Group::addFile(const QString &filePath)
{
    QString fileName = QFileInfo(filePath).fileName();
    QString ext = QFileInfo(filePath).suffix();
    QString inPath = path+DATIN_FOLDER;

    if(QFile::exists(inPath+"/"+fileName)) {
        if(QMessageBox::information(0,QObject::tr("Ошибка"),QObject::tr("Заменить файл ")+fileName+" ?"
                                    ,QMessageBox::Ok|QMessageBox::Cancel,QMessageBox::Cancel) == QMessageBox::Ok)
        {
            QFile::remove(inPath+"/"+fileName);
            if(!QFile::copy(filePath,inPath+"/"+fileName)) {
                QMessageBox::information(0,QObject::tr("Ошибка"),QObject::tr("Возникла ошибка при копировании файла!"));
                return;
            }
        } else {
            return;
        }
    } else {
        QFile::copy(filePath,inPath+"/"+fileName);
    }


    QString outPath = path + DATOUT_FOLDER + "/" +fileName;
    QString sqlPath = parentName+"_"+name;
    QString fPath = path + OUT_FOLDER;

    if(!fileList.contains(fileName))
	fileList.insert(fileName,FileInfo(fPath,inPath+"/"+fileName,outPath,fileName,getHeaderIndex(fileName),ext=="edf",sqlProperties,sqlPath));
}

void Group::deleteFileAt(int index) {
    QString fileName = fileList.keys().at(index);
    FileInfo fInfo = fileList.find(fileName).value();

    for(int i=0;i<Converter::SourceNum; ++i) {
        fInfo[(Converter::ConvertType)i].remove();
    }

    fileList.erase(fileList.find(fileName));
}

void Group::setHeaderList(const HeaderList & hList)
{
    headerList = hList;

    for(int i=0; i<fileList.size(); ++i) {
        QString fileName  = getFileNames().at(i);

        getFileAt(i).headerIndex = getHeaderIndex(fileName);
    }

}


void Group::saveSQLIndex()
{
    QFile file(path+"/Info.txt");

    if(!file.open(QIODevice::WriteOnly)) {
        return;
    }

    file.write(QByteArray(QByteArray::number(sqlProperties.index)));

    file.close();

    file.close();
}
