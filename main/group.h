#ifndef GROUP_H
#define GROUP_H

#include "headerlist.h"
#include "fileinfo.h"
#include "settings.h"

#include <QMap>
#include <QDir>
#include <QStringList>

namespace Converter {
    namespace Interface {




    class Group
    {
    private:
        QString name;
        QString path;
        HeaderList headerList;
        QMap <QString,FileInfo> fileList;
//        FileList fileList;
//        HeaderList headerList;
        SqlProperties sqlProperties;
        QString parentName;

    public:
        Group():
            name(QString()),
            path(QString()),
            headerList(HeaderList()),
            fileList(QMap <QString,FileInfo>()),
            sqlProperties(SqlProperties()),
            parentName(QString())
        {}

        Group(const QString &n,const QString &p,const HeaderList &h,const SqlProperties &s,const QString &par):
            name(n),
            path(p),
            headerList(h),
            fileList(QMap <QString,FileInfo>()),
            sqlProperties(s),
            parentName(par)
        {
            loadFileList();
        }

        void loadFileList();
        void loadTxt();
        void loadSQL();
        void loadExcel();
        void addFile(const QString &filePath);

        void deleteFileAt(int index);

        void saveSQLIndex();

        int getHeaderIndex(const QString &fileName) {
            return headerList.findFilter(fileName);
        }

        QList <QString> getFileNames() {
            return fileList.keys();
        }

        FileInfo &getFileAt(int i) {
            return fileList[fileList.keys().at(i)];
        }

        void setSqlProperties(const SqlProperties &prop) {
            if(sqlProperties == prop)
                return;

            sqlProperties = prop;
            saveSQLIndex();

            loadFileList();
        }

        QMap <QString,FileInfo> &getFileList() {
            return fileList;
        }

        SqlProperties getSqlProperties() {
            return sqlProperties;
        }

        HeaderList &getHeaderList() {
            return headerList;
        }

        void setHeaderList(const HeaderList & hList);

        QString &getName() {
            return name;
        }
        QString &getPath() {
            return path;
        }
        int size() const {
            return fileList.size();
        }

    };

    }//end namespace Converter::Interface
}//end namespace Converter
#endif // GROUP_H
