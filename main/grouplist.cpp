#include "grouplist.h"


#include <QFile>


using namespace Converter::Interface;


void GroupList::load(const Settings &settings)
{
    QDir *dir= new QDir(path,"*",QDir::Name,QDir::Dirs|QDir::NoDotAndDotDot);
    QFileInfoList folderList = dir->entryInfoList();

    QString baseName;

    for(int i=0; i < folderList.size(); ++i) {
        baseName = QDir(folderList.at(i).filePath()).dirName();

        if(baseName=="Headers")
                continue;

        addGroup(baseName,settings);
    }

}

void GroupList::addGroup(const QString &baseName,const Settings &settings,bool crFolder)
{
    if(crFolder)
        createGroupFolders(baseName);

    QFile file(path+"/"+baseName+"/Info.txt");

    if(!file.open(QIODevice::ReadOnly)) {
        return;
    }

    long long int n = QString(file.readLine(100)).toLongLong();

    file.close();

    SqlProperties s;

    if(n==-1 || n>=settings.connectionNum()) {
        s = SqlProperties();
    } else {
        s = settings.sqlAt(n);
    }

    insert(baseName,Group(baseName,path+"/"+baseName,headerList,s,name));
}

void GroupList::createGroupFolders(const QString &baseName)
{

    QDir(path).mkdir(baseName);
    QDir newFolder(path+"/"+baseName);

    newFolder.mkdir("DatIn");
    newFolder.mkdir("DatOut");
    newFolder.mkdir("Out");

    QFile file(path+"/"+baseName+"/Info.txt");

    if(!file.open(QIODevice::WriteOnly)) {
        return;
    }

    file.write(QByteArray("-1"));

    file.close();
}


void GroupList::deleteGroupAt(const int index) {
    QString baseName = keys().at(index);
    erase(find(baseName));
    removeFolder(QDir(path+"/"+baseName));
}

bool GroupList::renameAt(int i,const QString &newName)
{
    if(contains(newName) || newName.trimmed().isEmpty())
        return false;
    //Changing folder name
    QDir d = QDir(path);
    d.rename(keys().at(i),newName);

    //Renaming..
    Group g = operator [] (keys().at(i));

    erase(find(keys().at(i)));
    insert(newName,Group(newName,path+"/"+newName,headerList,g.getSqlProperties(),name));

    return true;
}

int GroupList::getGroupIndex(const QString &baseName)
{
    for(int i=0; i < size(); ++i) {
        if(at(i).getName() == baseName)
            return i;
    }
    return -1;
}

void GroupList::reloadHeaders()
{
    for(int i=0;i < size(); ++i) {
        at(i).setHeaderList(headerList);
    }
}

