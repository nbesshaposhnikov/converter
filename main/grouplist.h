#ifndef GROUPLIST_H
#define GROUPLIST_H

#include <QDir>

#include "group.h"

namespace Converter {
    namespace Interface {

    class Group;

    class GroupList:public QMap <QString,Group>
    {
    private:
        QString name;
        QString path;
        HeaderList headerList;

        void createGroupFolders(const QString &baseName);
    public:
        GroupList():
            name(QString()),
            path(QString()),
            headerList(HeaderList())
        {}

        GroupList(const QString &n,const QString &p,const Settings &settings):
            name(n),
            path(p),
            headerList(HeaderList(p+"/Headers"))
        {
            headerList.load();
            load(settings);
        }

        Group &at(int i) {
            return operator [](keys().at(i));
        }

        void load(const Settings &settings);

        QString &getPath() {
            return path;
        }

        QString &getName() {
            return name;
        }

        HeaderList &getHeaderList() {
            return headerList;
        }

        HeaderList getHeaderList() const {
            return headerList;
        }

        void reloadHeaders();

        int getGroupIndex(const QString &baseName);

        void addGroup(const QString &baseName,const Settings &settings,bool crFolder = false);

        void deleteGroupAt(const int index);

        bool renameAt(int i,const QString &newName);
    };

    }//end namespace Converter::Interface
}//end namespace Converter
#endif // GROUPLIST_H
