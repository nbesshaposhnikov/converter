#include "grouplistmodel.h"

using namespace Converter::Interface;

GroupListModel::GroupListModel(QObject *parent):
    QAbstractListModel(parent),
    mGList(0)
{
}

void GroupListModel::setGroupList(GroupList *newGList)
{
//    if(mGList == newGList)
//        return;
    beginResetModel();
    mGList = newGList;
    endResetModel();
}

int GroupListModel::rowCount(const QModelIndex &parent) const
{
    return parent.isValid() ? 0 : (mGList ? mGList->size() : 0);
}

QVariant GroupListModel::data(const QModelIndex &index, int role) const
{
    const int groupIndex = index.row();
    if (groupIndex < 0)
        return QVariant();

    switch (role) {
    case Qt::DisplayRole: return mGList->at(groupIndex).getName();
    case Qt::EditRole:
    case Qt::DecorationRole:
    default:
        return QVariant();
    }
}

bool GroupListModel::setData (const QModelIndex & index, const QVariant & value, int role)
{
    if (role == Qt::EditRole) {
       bool res= mGList->renameAt(index.row(),value.toString());
       emit groupRenamed(value.toString());
       return res;
    }
    return false;
}


Qt::ItemFlags GroupListModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return 0;

    return Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsEditable;
}

QVariant GroupListModel::headerData(int section, Qt::Orientation orientation,
                                int role) const
{
    if (role == Qt::DisplayRole && orientation == Qt::Horizontal) {
        switch (section) {
        case 0: return tr("Header");
        }
    }
    return QVariant();
}
