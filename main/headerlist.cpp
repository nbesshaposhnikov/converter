﻿#include "headerlist.h"
#include <QDir>
#include <QStringList>
#include <QStringList>
#include <QMessageBox>

using namespace Converter::Interface;

void HeaderList::load()
{
    QDir *dir= new QDir(folder,"*.h",QDir::Name,QDir::Files);
    QFileInfoList fileList = dir->entryInfoList();

    clear();

    for(int i=0; i < fileList.size(); i++) {
        QString baseName = fileList.at(i).fileName();
        insert(baseName,HeaderInfo(fileList.at(i).filePath()));
    }
    delete dir;
}


void HeaderInfo::loadFilters()
{
    QFile *f = new QFile(filePath);
    if(!f->open(QIODevice::ReadOnly)) {
        return;
    }

    QString line;

    while(!f->atEnd()) {
        line = QString(f->readLine(100000));
        if(line.trimmed().startsWith("filemask")) {
            QStringList fL = line.toLower().trimmed().split('=').at(1).split(';',QString::SkipEmptyParts);
            for(int i = 0; i < fL.size();++i ) {
                filterList.insert(fL.at(i).trimmed());
            }
	    break;
        }
    }

    f->close();
    delete f;

    QString baseName = QFileInfo(filePath).baseName().toLower();

    if(filterList.isEmpty()) {
        filterList.insert(baseName+".dat");
        filterList.insert(baseName+".edf");
    }
}

void HeaderList::addHeader(const QString &filePath)
{
    QString baseName = QFileInfo(filePath).fileName();
    QString newFilePath = folder+"/"+baseName;

    if(QFile::exists(newFilePath)) {
        if(QMessageBox::information(0,QObject::tr("Ошибка"),QObject::tr("Заменить файл ")+baseName+" ?"
                                    ,QMessageBox::Ok|QMessageBox::Cancel,QMessageBox::Cancel) == QMessageBox::Ok)
        {
            QFile::remove(newFilePath);
            if(!QFile::copy(filePath,newFilePath)) {
                QMessageBox::information(0,QObject::tr("Ошибка"),QObject::tr("Возникла ошибка при копировании файла!"));
                return;
            }
        } else {
            return;
        }
    } else {
        QFile::copy(filePath,newFilePath);
    }

    insert(baseName,HeaderInfo(newFilePath));
}

void HeaderList::removeHeader(const QString &name)
{
    QString fileName = operator [] (name).getFilePath();

    QFile::remove(fileName);

    erase(find(name));
}
