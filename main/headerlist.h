#ifndef HEADERLIST_H
#define HEADERLIST_H

#include <QMap>
#include <QSet>
#include <QString>
#include <QRegExp>
#include <QDebug>
namespace Converter {
    namespace Interface {

    struct HeaderInfo
    {
    private:
        QString filePath;
        QSet <QString> filterList;
    public:
        HeaderInfo ():
            filePath(QString())
        {}

        HeaderInfo (const QString &f):
            filePath(f)
        {
            loadFilters();
        }

        HeaderInfo &operator =(const HeaderInfo &h) {
            filePath = h.filePath;
            filterList = h.filterList;

            return *this;
        }

        void loadFilters();
        QSet <QString> getFilterList() const {
            return filterList;
        }

        QString getFilePath() const{
            return filePath;
        }
    };

    class HeaderList:public QMap <QString,HeaderInfo>
    {
    private:
        QString folder;

    public:
        HeaderList():
            folder(".\\")
        {}
        HeaderList(QString f):
            folder(f)
        {}

        const HeaderInfo &at(const int i) const{
            return values().at(i);
        }

        HeaderInfo &at(int i){
            return operator [](keys().at(i));
        }

        QList <QString> getHeaderNames() {
            return keys();
        }

        int findFilter(const QString &name) const{
            QMap <QString,HeaderInfo>::const_iterator i;
            int j = 0;
            for(i = begin(),j=0;i!=end();++i,++j) {
                QSet <QString> filterList = i.value().getFilterList();
                QRegExp validator("",Qt::CaseInsensitive);
                validator.setPatternSyntax(QRegExp::Wildcard);

                for(QSet <QString>::const_iterator k = filterList.begin(); k !=filterList.end(); ++k) {
                    validator.setPattern(*k);
                    if(validator.exactMatch(name.toLower()))
                        return j;

                    qDebug()<<validator.errorString();
                }

            }
            return -1;
        }


        void load();

        void addHeader(const QString &filePath);
        void removeHeader(const QString &name);
    };

    }//end namespace Converter::Interface
}//end namespace Converter
#endif // HEADERLIST_H
