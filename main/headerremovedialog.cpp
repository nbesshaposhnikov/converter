#include "headerremovedialog.h"
#include "ui_headerremovedialog.h"


using namespace Converter::Interface;

HeaderRemoveDialog::HeaderRemoveDialog(const HeaderList &hList,QWidget *parent) :
    QDialog(parent),
    ui(new Ui::HeaderRemoveDialog),
    headerList(hList)
{
    ui->setupUi(this);
    ui->headerList->setSortingEnabled(false);
    ui->headerList->addItems(headerList.getHeaderNames());

    for(int i=0; i< ui->headerList->count(); ++i) {
        ui->headerList->item(i)->setCheckState(Qt::Unchecked);
    }
}

HeaderRemoveDialog::~HeaderRemoveDialog()
{
    delete ui;
}

QList <QString> HeaderRemoveDialog::getStates()
{
    QList <QString> retList;

    for(int i=0; i< ui->headerList->count(); ++i) {
        if(ui->headerList->item(i)->checkState()==Qt::Checked)
            retList.append(ui->headerList->item(i)->text());
    }

    return retList;
}
