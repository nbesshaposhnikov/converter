#ifndef HEADERREMOVEDIALOG_H
#define HEADERREMOVEDIALOG_H

#include <QDialog>
#include "headerlist.h"

namespace Ui {
class HeaderRemoveDialog;
}

namespace Converter {

namespace Interface {

    class HeaderRemoveDialog : public QDialog
    {
        Q_OBJECT

    public:
        explicit HeaderRemoveDialog(const HeaderList &hList,QWidget *parent = 0);
        ~HeaderRemoveDialog();

    private:
        Ui::HeaderRemoveDialog *ui;
        HeaderList headerList;

    public:
        QList <QString> getStates();
    };
}
}


#endif // HEADERREMOVEDIALOG_H
