#include "linkerdialog.h"
#include "ui_linkerdialog.h"

LinkerDialog::LinkerDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::LinkerDialog)
{
    ui->setupUi(this);
}

LinkerDialog::~LinkerDialog()
{
    delete ui;
}
