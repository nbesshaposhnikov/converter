#ifndef LINKERDIALOG_H
#define LINKERDIALOG_H

#include <QDialog>

namespace Ui {
class LinkerDialog;
}

class LinkerDialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit LinkerDialog(QWidget *parent = 0);
    ~LinkerDialog();
    
private:
    Ui::LinkerDialog *ui;
};

#endif // LINKERDIALOG_H
