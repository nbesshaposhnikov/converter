#include <QtWidgets/QApplication>
#include "mainwindow.h"
#include <QTextCodec>
#include <QStyleFactory>

using namespace Converter;
int main(int argc, char *argv[])
{
//    qDebug()<<QStyleFactory::keys();
//    QApplication::setStyle("macintosh");

    setlocale(LC_ALL, "Russian");

#ifdef WIN32
    QTextCodec *wincodec = QTextCodec::codecForName("Windows-1251");
    QTextCodec::setCodecForLocale(wincodec);
#endif

    QApplication a(argc, argv);

    a.setOrganizationName("[scaR]");
    a.setApplicationName("Converter");
    a.setApplicationVersion("4.0.5");

    MainWindow w;

    w.show();

    return a.exec();
}
