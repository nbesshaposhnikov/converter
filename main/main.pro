#-------------------------------------------------
#
# Project created by QtCreator 2011-08-05T21:19:06
#
#-------------------------------------------------

include(main.pri)
include(../core/core.pri)

CONFIG(release,declarative_debug|release):{
    DEFINES += DEBUG
    QT   += axcontainer
}
#else DEFINES -=DEBUG
macx:LIBS+= -lodbc
#unix:LIBS += -lpq
#win32:LIBS += QAxContainer.lib
#QMAKE_LFLAGS_RELEASE+= -Map


TARGET = Converter
TEMPLATE = app

#target.path = $${PREFIX}/bin
#INSTALLS += target
#win32 {
#    DESTDIR = ../..
#} else {
#    DESTDIR = ../../bin
#}

target.path = $${PREFIX}/bin
INSTALLS += target
win32 {    
    DESTDIR = ../bin
} else {
    DESTDIR = ../../bin
}

QT       += core sql widgets xml

macx {
    DEFINES += MAC_X

    QMAKE_LIBDIR_FLAGS += -L$$OUT_PWD/../../bin/Converter.app/Contents/Frameworks
} else:win32 {
    DEFINES += WIN32

    LIBS += -L$$OUT_PWD/../bin
} else {
    QMAKE_LIBDIR_FLAGS += -L$$OUT_PWD/../bin
}



MOC_DIR = .moc
UI_DIR = .uic
RCC_DIR = .rcc
OBJECTS_DIR = .obj

SOURCES += main.cpp\
        mainwindow.cpp \
    settings.cpp \
    grouplist.cpp \
    group.cpp \
    globalgrouplist.cpp \
    grouplistmodel.cpp \
    fileinfo.cpp \
    headerlist.cpp \
    settingsdialog/settingsdialog.cpp \
    settingsdialog/globalgrouplistmodel.cpp \
    settingsdialog/connectionlistmodel.cpp \
    convertdialog.cpp \
    fileinfodelegate.cpp \
    headerremovedialog.cpp \
    edftools.cpp \
    settingsdialog/connectionlistdelegate.cpp \
    sqlmanager.cpp \
    linkerdialog.cpp \
    fileinfomodel.cpp

HEADERS  += mainwindow.h \
    settings.h \
    grouplist.h \
    group.h \
    globalgrouplist.h \
    grouplistmodel.h \
    fileinfo.h \
    headerlist.h \
    settingsdialog/settingsdialog.h \
    settingsdialog/globalgrouplistmodel.h \
    settingsdialog/connectionlistmodel.h \
    convertdialog.h \
    fileinfomodel.h \
    fileinfodelegate.h \
    headerremovedialog.h \
    edftools.h \
    settingsdialog/connectionlistdelegate.h \
    sqlmanager.h \
    linkerdialog.h

FORMS    += mainwindow.ui \
    settingsdialog/settingsdialog.ui \
    convertdialog.ui \
    headerremovedialog.ui \
    linkerdialog.ui

#win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../bin/ -lcore
#else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../bin/ -lcore
#else:symbian: LIBS += -lcore
#else:unix: LIBS += -L$$OUT_PWD/../bin/ -lcore




macx {
    TARGET = Converter
    DEFINES += MAC_X
    #QMAKE_INFO_PLIST = Info.plist
    ICON = images/converter.icns
}
INCLUDEPATH += $$PWD/../core
DEPENDPATH += $$PWD/../core

win32 {
    RC_FILE = converter.rc
}

contains(CONFIG, static) {
    DEFINES += STATIC_BUILD
}

RESOURCES += \
    converter.qrc \



































