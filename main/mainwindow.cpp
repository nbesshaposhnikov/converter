#include <stdio.h>

#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QInputDialog>
#include <QFileDialog>
#include <QMessageBox>
#include <QDir>

#include "converter.h"
/*NEED TO DO
Оптимизации:
1)функция проверки полей,в функции чтения - Done
    а)Посчитать обратную польскую записть
    б)Ссылки на элементы вместо имен(lN || gN)
2)В поиске по имени добавить return bool(на неудачу) -Not need
3)TxtStructure && Structure изменить конструкторы и добавить - what?!
4)Убрать проверки из функций филдата на проверку(проверено все будет на этапе построения структуры) -- Done
5)Поправить текст исключений(+ QString) - Done
--------------------------------------------
6)Emitter-емитер состояния конветировани для логирования - Done - Deleted 08.08.2012

7) Убить все ворнинги! - done
8) Сделать свой криптор едфок - done

9)Добавить в FieldData перменную за то,что число или нет+ заменить все функции toLongLong -done
10)поправить перезагрузку списка скл коннектов при Применениии настроек - Done

11)первести сам конвертре на простые типы данных QT - done
12)сделать вывод линии при ошибке совпадения полей и сделать более информативные ошибки -done
13)сделать конку принудительного конверта - done
14)Добавить открывать папки по завершению. - done
*/

#include <QObject>

using namespace Converter;
using namespace Converter::Interface;


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    //init checkstates
    for(int i=0;i < 7;++i)
        states[i]=false;

    ui->centralWidget->setLayout(ui->mainLayout);

    //Load Settings
    mSettings = new Settings;
    mSettings->load();


    mGlobalGroupList = new GlobalGroupList();
    mGlobalGroupList->load(*mSettings);
    loadGlobalGroupList();
    ui->globalGroupList->setCurrentIndex(-1);
    connect(ui->globalGroupList,SIGNAL(currentIndexChanged(int)),this,SLOT(onGlobalGroupChange(int)));

    mGroupListModel = new GroupListModel();
    //mGroupListModel->setGroupList(&mGlobalGroupList->at(mSettings->getMainGroup()));
    ui->groupListView->setModel(mGroupListModel);
    connect(ui->groupListView->selectionModel(),SIGNAL(currentRowChanged(QModelIndex,QModelIndex)),
            this,SLOT(onGroupChange(QModelIndex)));

    connect(mGroupListModel,SIGNAL(groupRenamed(QString)),
            this,SLOT(onGroupRename(QString)));

    mFileInfoModel = new FileInfoModel();
    mFileInfoDelegate = new FileInfoDelegate();
    ui->fileInfoList->setModel(mFileInfoModel);
    ui->fileInfoList->setItemDelegate(mFileInfoDelegate);

    connect(ui->fileInfoList->selectionModel(),SIGNAL(currentRowChanged(QModelIndex,QModelIndex)),
            this,SLOT(onFileChange(QModelIndex)));
    connect(ui->fileInfoList->horizontalHeader(),SIGNAL(sectionClicked(int)),this,SLOT(onColumnClicked(int)));

    loadConnectionGroupList();
    connect(ui->connectionList,SIGNAL(currentIndexChanged(int)),this,SLOT(onConnectionChange(int)));

    //connect add & delete
    connect(ui->addGroup,SIGNAL(clicked()),this,SLOT(onAddGroup()));
    connect(ui->removeGroup,SIGNAL(clicked()),this,SLOT(onDeleteGroup()));

    connect(ui->addFile,SIGNAL(clicked()),this,SLOT(onAddFile()));
    connect(ui->removeFile,SIGNAL(clicked()),this,SLOT(onDeleteFile()));

    connect(ui->addStruct,SIGNAL(clicked()),this,SLOT(onAddHeader()));
    connect(ui->removeStruct,SIGNAL(clicked()),this,SLOT(onDeleteHeader()));

    connect(ui->checkStates,SIGNAL(clicked()),this,SLOT(onCheckStates()));
    //Menu
    connect(ui->preferences,SIGNAL(triggered()),this,SLOT(openSettings()));
    connect(ui->exit,SIGNAL(triggered()),this,SLOT(exitApplication()));
    connect(ui->about_conv,SIGNAL(triggered()),this,SLOT(aboutDialog()));
    //convertion
    connect(ui->convert,SIGNAL(clicked()),this,SLOT(convert()));
    connect(ui->deconvert,SIGNAL(clicked()),this,SLOT(deconvert()));

    //Select Main Group
    ui->globalGroupList->setCurrentIndex(mSettings->getMainGroup());


    //change buttons sizes for MAC X

#ifdef MAC_X
    ui->convert->setMaximumWidth(90);
    ui->deconvert->setMaximumWidth(90);
#endif
    //setWindowIcon(QIcon(":/images/converter.ico"));
}

MainWindow::~MainWindow()
{
    delete ui;
    delete mFileInfoModel;
    delete mFileInfoDelegate;
    delete mGroupListModel;
    delete mGlobalGroupList;
    delete mSettings;
}

void MainWindow::exitApplication()
{
    exit(0);
}

void MainWindow::loadGlobalGroupList()
{
    ui->globalGroupList->clear();

    for(int i=0; i < mGlobalGroupList->size(); ++i) {
        ui->globalGroupList->addItem(mGlobalGroupList->at(i).getName());
    }


}

void MainWindow::onColumnClicked(int index)
{
    int gGroup = ui->globalGroupList->currentIndex();
    int group = ui->groupListView->selectionModel()->currentIndex().row();

    if(gGroup==-1||group==-1)
        return;

    switch(index) {
    case 0: {
        for(int i=0;i<mGlobalGroupList->at(gGroup).at(group).size();++i) {
            bool &state = mGlobalGroupList->at(gGroup).at(group).getFileAt(i).getState();
            state = states[index];
        }
        states[index] = !states[index];
        onGroupChange(ui->groupListView->selectionModel()->currentIndex(),false);
    }break;

    case 2:case 3:case 4: {
        for(int i=0;i<mGlobalGroupList->at(gGroup).at(group).size();++i) {
            bool &state = mGlobalGroupList->at(gGroup).at(group).getFileAt(i)[(ConvertType)(index-1)].selected();
            state = states[index];
        }
        states[index] = !states[index];
        onGroupChange(ui->groupListView->selectionModel()->currentIndex(),false);
    }break;

    case 5: {
        for(int i=0;i<mGlobalGroupList->at(gGroup).at(group).size();++i) {
            bool &state = mGlobalGroupList->at(gGroup).at(group).getFileAt(i).isInEdf;
            state = states[index];
        }
        states[index] = !states[index];
        onGroupChange(ui->groupListView->selectionModel()->currentIndex(),false);
    }break;

    case 6: {
        for(int i=0;i<mGlobalGroupList->at(gGroup).at(group).size();++i) {
            bool &state = mGlobalGroupList->at(gGroup).at(group).getFileAt(i).isOutEdf;
            state = states[index];
        }
        states[index] = !states[index];
        onGroupChange(ui->groupListView->selectionModel()->currentIndex(),false);
    }break;

    }


}

void MainWindow::onGlobalGroupChange(int index,int groupIndex)
{
    if(index == -1)
        return;

    mGroupListModel->setGroupList(&mGlobalGroupList->at(index));

    if(groupIndex != -1) {
        QModelIndex m = mGroupListModel->index(groupIndex,0);
        onGroupChange(m);

        ui->groupListView->setCurrentIndex(m);
    } else {
        mFileInfoModel->setGroup(0);
        mFileInfoDelegate->setGroup(0);
        onFileChange(QModelIndex());
    }


}

void MainWindow::onGroupChange(QModelIndex index,bool reloadConnection)
{
    if(index==QModelIndex())
        return;

    int row = index.row();
    int group = ui->globalGroupList->currentIndex();

    if(group==-1)
        return;

    mFileInfoModel->setGroup(&mGlobalGroupList->at(group).at(row));
    mFileInfoDelegate->setGroup(&mGlobalGroupList->at(group).at(row));
    //Column sizes!
    ui->fileInfoList->horizontalHeader()->setSectionResizeMode(2,QHeaderView::ResizeToContents);
    ui->fileInfoList->horizontalHeader()->setSectionResizeMode(3,QHeaderView::ResizeToContents);
    ui->fileInfoList->horizontalHeader()->setSectionResizeMode(4,QHeaderView::ResizeToContents);
    ui->fileInfoList->horizontalHeader()->setSectionResizeMode(5,QHeaderView::ResizeToContents);
    ui->fileInfoList->horizontalHeader()->setSectionResizeMode(6,QHeaderView::ResizeToContents);
    ui->fileInfoList->horizontalHeader()->resizeSection(0,140);
    ui->fileInfoList->horizontalHeader()->resizeSection(1,140);
    clearFileInfo();

    if(reloadConnection)
        loadConnection();
}

void MainWindow::onFileChange(QModelIndex index)
{
    if(index == QModelIndex()) {
        clearFileInfo();
        return;
    }

    int row = index.row();
    int globalGroup = ui->globalGroupList->currentIndex();
    int group = ui->groupListView->currentIndex().row();

    if(index==QModelIndex()||globalGroup==-1)
        return;

    FileInfo fInfo = mGlobalGroupList->at(globalGroup).at(group).getFileAt(row);

    ui->datStatus->setText(fInfo[Converter::Dat].check() ? tr("Ок") : tr("Изменен"));
    ui->txtStatus->setText(fInfo[Converter::Txt].check() ? tr("Ок") : tr("Изменен"));
    ui->sqlStatus->setText(fInfo[Converter::SQL].check() ? tr("Ок") : tr("Изменен"));
    ui->excelStatus->setText(fInfo[Converter::Excel].check() ? tr("Ок") : tr("Изменен"));

    ui->datStatus->setText(fInfo[Converter::Dat].exists() ? ui->datStatus->text() : tr("Не существует"));
    ui->txtStatus->setText(fInfo[Converter::Txt].exists() ? ui->txtStatus->text() : tr("Не существует"));
    ui->sqlStatus->setText(fInfo[Converter::SQL].exists() ? ui->sqlStatus->text() : tr("Не существует"));
    ui->excelStatus->setText(fInfo[Converter::Excel].exists() ? ui->excelStatus->text() : tr("Не существует"));

}

void MainWindow::clearFileInfo()
{

    ui->datStatus->setText(tr("None"));
    ui->txtStatus->setText(tr("None"));
    ui->sqlStatus->setText(tr("None"));
    ui->excelStatus->setText(tr("None"));

}


void MainWindow::loadConnectionGroupList()
{
    ui->connectionList->clear();

    ui->connectionList->addItem(QString(tr("None")));

    for(int i=0; i < mSettings->connectionNum(); ++i) {
        ui->connectionList->addItem(mSettings->sqlAt(i).name);
    }

    loadConnection();
}

void MainWindow::loadConnection()
{
    int group = ui->globalGroupList->currentIndex();
    QModelIndex index = ui->groupListView->selectionModel()->currentIndex();

    if(group ==-1 || index == QModelIndex())
        return;

    ui->connectionList->setCurrentIndex(mGlobalGroupList->at(group).at(index.row()).getSqlProperties().index + 1);
    //Make Check Sql Connection
}

void MainWindow::onConnectionChange(int newIndex)
{
    int group = ui->globalGroupList->currentIndex();
    QModelIndex index = ui->groupListView->selectionModel()->currentIndex();

    if(group ==-1 || index == QModelIndex() || newIndex < 0) {
        ui->connectionList->setCurrentIndex(0);
        return;
    }

    mFileInfoModel->setGroup(0);
    mFileInfoDelegate->setGroup(0);

    if(newIndex!=0)
        mGlobalGroupList->at(group).at(index.row()).setSqlProperties(mSettings->sqlAt(newIndex-1));
    else
        mGlobalGroupList->at(group).at(index.row()).setSqlProperties(SqlProperties());

    mFileInfoModel->setGroup(&mGlobalGroupList->at(group).at(index.row()));
    mFileInfoDelegate->setGroup(&mGlobalGroupList->at(group).at(index.row()));

    onGroupChange(index,false);
}

void MainWindow::onGroupRename(QString name)
{
    int gGroup = ui->globalGroupList->currentIndex();
    int group = (*mGlobalGroupList)[gGroup].getGroupIndex(name);

    onGlobalGroupChange(gGroup,group);

    //ui->fileListView->selectionModel()->setCurrentIndex(m,QItemSelectionModel::Select);
    //ui->fileListView->clearSelection();

}

void MainWindow::onAddGroup()
{
    bool ok;
    QString name = QInputDialog::getText(this, tr("Введите название группы"),
                                        tr("Название :"), QLineEdit::Normal,
                                         QString(), &ok);

    if(ok && !name.trimmed().isEmpty()) {
        int gGroup = ui->globalGroupList->currentIndex();

        if(!mGlobalGroupList->at(gGroup).contains(name) && name !="Headers") {

            mGlobalGroupList->at(gGroup).addGroup(name,*mSettings,true);

            int group = mGlobalGroupList->at(gGroup).getGroupIndex(name);

            onGlobalGroupChange(gGroup,group);

        } else {
            QMessageBox::information(this,tr("Ошибка"),tr("Группа уже сущетсвует!"));
        }
    }

}

void MainWindow::onDeleteGroup()
{
    int gGroup = ui->globalGroupList->currentIndex();
    int group = ui->groupListView->selectionModel()->currentIndex().row();

    if(group == -1)
        return;

    if(QMessageBox::information(this,QObject::tr("Внимание"),QObject::tr("Будет удалено все содежримое группы!\nПродолжить?")
                                ,QMessageBox::Ok|QMessageBox::Cancel,QMessageBox::Cancel) != QMessageBox::Ok)
    {
        return;
    }
    mGlobalGroupList->at(gGroup).deleteGroupAt(group);

    onGlobalGroupChange(gGroup);
}

void MainWindow::onAddFile()
{
    QStringList files = QFileDialog::getOpenFileNames(this,
                            tr("Open DAT|EDF Files"), QString(), tr("Convert Files (*.dat *.edf)"));

    //QFile::copy()
    int gGroup = ui->globalGroupList->currentIndex();
    int group = ui->groupListView->selectionModel()->currentIndex().row();

    for(int i=0; i < files.size(); ++i) {
        mGlobalGroupList->at(gGroup).at(group).addFile(files.at(i));
    }

    onGroupChange(ui->groupListView->selectionModel()->currentIndex(),false);
}

void MainWindow::onDeleteFile()
{
    int gGroup = ui->globalGroupList->currentIndex();
    int group = ui->groupListView->selectionModel()->currentIndex().row();
    int fileIndex = ui->fileInfoList->currentIndex().row();

    mGlobalGroupList->at(gGroup).at(group).deleteFileAt(fileIndex);

    onGroupChange(ui->groupListView->selectionModel()->currentIndex(),false);
}

void MainWindow::onAddHeader()
{
    int gGroup = ui->globalGroupList->currentIndex();

    QStringList files = QFileDialog::getOpenFileNames(this,
                            tr("Open Header Files"), QString(), tr("Header Files (*.h)"));

    for(int i=0; i < files.size(); ++i) {
        mGlobalGroupList->at(gGroup).getHeaderList().addHeader(files.at(i));
    }
    mGlobalGroupList->at(gGroup).reloadHeaders();

    onGroupChange(ui->groupListView->selectionModel()->currentIndex(),false);
}

void MainWindow::onDeleteHeader()
{
    int gGroup = ui->globalGroupList->currentIndex();
    QList <QString> retList;

    HeaderRemoveDialog *d = new HeaderRemoveDialog(mGlobalGroupList->at(gGroup).getHeaderList(),this);

    d->exec();
    retList = d->getStates();

    for(int i=0; i < retList.size(); ++i) {
            mGlobalGroupList->at(gGroup).getHeaderList().removeHeader(retList.at(i));
    }

    if(retList.size() > 0) {
        mGlobalGroupList->at(gGroup).reloadHeaders();
        onGroupChange(ui->groupListView->selectionModel()->currentIndex(),false);
    }
}


void MainWindow::onCheckStates()
{
    int gGroup = ui->globalGroupList->currentIndex();
    int group = ui->groupListView->selectionModel()->currentIndex().row();
    int fileIndex = ui->fileInfoList->currentIndex().row();

    if(gGroup==-1||group==-1||fileIndex==-1)
        return;

    mGlobalGroupList->at(gGroup).at(group).getFileAt(fileIndex).checkStates();

    onFileChange(ui->fileInfoList->selectionModel()->currentIndex());
}

void MainWindow::openSettings()
{
    SettingsDialog *d = new SettingsDialog(*mSettings,this);

    if(d->exec()) {
        delete mSettings;
        mSettings  = new Settings(d->getSettings());
        mSettings->save();

        int group = ui->groupListView->selectionModel()->currentIndex().row();

        mFileInfoModel->setGroup(0);
        mFileInfoDelegate->setGroup(0);
        mGroupListModel->setGroupList(0);

        delete mGlobalGroupList;
        mGlobalGroupList = new GlobalGroupList();
        mGlobalGroupList->load(*mSettings);

        loadConnectionGroupList();
        loadGlobalGroupList();
        ui->globalGroupList->setCurrentIndex(mSettings->getMainGroup());
        onGlobalGroupChange(mSettings->getMainGroup(),group);

//        mGroupListModel->setGroupList(&mGlobalGroupList->at(gGroup));
//        if(group !=-1) {
//            mFileInfoModel->setGroup(&mGlobalGroupList->at(gGroup).at(group));
//            mFileInfoDelegate->setGroup(&mGlobalGroupList->at(gGroup).at(group));
//            onGroupChange(mGroupListModel->index(group));
//        }


    }

    delete d;
}

void MainWindow::convert()
{
    int gGroup = ui->globalGroupList->currentIndex();
    int group = ui->groupListView->selectionModel()->currentIndex().row();

    if(gGroup==-1||group==-1)
        return;

    ConvertDialog *d = new ConvertDialog(&mGlobalGroupList->at(gGroup).at(group),0,this);
    d->exec();
    delete d;
}

void MainWindow::deconvert()
{
    int gGroup = ui->globalGroupList->currentIndex();
    int group = ui->groupListView->selectionModel()->currentIndex().row();

    if(gGroup==-1||group==-1)
        return;

    ConvertDialog *d = new ConvertDialog(&mGlobalGroupList->at(gGroup).at(group),1,this);
    d->exec();
    delete d;
}

void MainWindow::aboutDialog()
{
    QMessageBox::about(this, tr("О Converter"),
        tr("Программа <b>Converter</b> предназначена для конвертирования "
           "бинарных файлов(типа .dat) в формат структуруированных текстовых "
           "файлов или баз данных MSSQL,PostgreSQL,SQLITE. <br /><br />"
           "Отдельное спасибо <b>4erepaxa</b> за предоставление идей и <b>na3x</b> "
           "за активный поиск багов и недочетов. "
           "<p align=right>Created by <b>[scaR]</b> <br />"
           "ICQ: 2626846 <br />"
           "Skype: megoscar <br />"
           "e-mail: scar2012@me.com <br /></p>"));
}
