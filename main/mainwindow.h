#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "settings.h"
#include "globalgrouplist.h"
#include "grouplistmodel.h"
#include "fileinfomodel.h"
#include "fileinfodelegate.h"

#include "settingsdialog/settingsdialog.h"
#include "convertdialog.h"
#include "headerremovedialog.h"

namespace Ui {
    class MainWindow;
}
namespace Converter {

class TConverter;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;

    Interface::Settings *mSettings;
    Interface::GlobalGroupList *mGlobalGroupList;

    Interface::GroupListModel *mGroupListModel;
    Interface::FileInfoModel *mFileInfoModel;
    Interface::FileInfoDelegate *mFileInfoDelegate;

    bool states[7];

    void loadGlobalGroupList();
    void loadConnectionGroupList();
    void loadConnection();
    void clearFileInfo();

private slots:
    void onGlobalGroupChange(int index,int groupIndex = -1);
    void onConnectionChange(int newIndex);
    void onGroupChange(QModelIndex index,bool reloadConnection = true);
    void onFileChange(QModelIndex index);
    void onGroupRename(QString name);
    void onColumnClicked(int index);

    void onAddGroup();
    void onDeleteGroup();

    void onAddFile();
    void onDeleteFile();

    void onAddHeader();
    void onDeleteHeader();
    //States
    void onCheckStates();
    //Menu
    void openSettings();
    void aboutDialog();
    void exitApplication();
    //convert
    void convert();
    void deconvert();



};

}

#endif // MAINWINDOW_H
