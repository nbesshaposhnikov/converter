#include "settings.h"
#include <QFile>
#include <QTextStream>

using namespace Converter::Interface;

#include <QDebug>

void Settings::load()
{
    QDomDocument doc;

    //qDebug() << appDir;
    QFile *xmlFile = new QFile(getAppDir()+"/Settings.xml");

    if(!xmlFile->open(QIODevice::ReadOnly) || xmlFile->error() != QFile::NoError) {
	return;
    }
//    qDebug() << xmlFile->error();
//    qDebug() << xmlFile->fileName() ;
//    qDebug() << xmlFile->errorString();
    QString error = QString();
    int row,col;
    if(!doc.setContent(xmlFile,&error,&row,&col)) {
        xmlFile->close();
        delete xmlFile;
        return;
    }
    QDomElement settings;
    QDomElement groups;
    QDomElement connections;

    settings = doc.elementsByTagName("settings").at(0).toElement();
    groups = settings.elementsByTagName("groups").at(0).toElement();
    connections = settings.elementsByTagName("connections").at(0).toElement();

    mainGroup = groups.attribute("main","0").toInt();

    QDomNode n = groups.firstChild();

    while(!n.isNull()) {
     if(n.isElement() && n.toElement().nodeName() == "group") {
         QString p =  n.toElement().attribute("path").trimmed();
         if(p.isEmpty())
	    p = getAppDir();
         groupList.insert(n.toElement().firstChild().toText().data().trimmed(),p);
     }
     n = n.nextSibling();
    }

    n = connections.firstChild();
    int i=0;
    while(!n.isNull()) {
        if(n.isElement() && n.toElement().nodeName() == "connection") {
            QDomElement e = n.toElement();
            connectionList.append(SqlProperties(i,e.firstChild().toText().data().trimmed(),
                                                e.attribute("login"),
                                                e.attribute("password"),
						e.attribute("host"),
						(SQLType)e.attribute("type").toInt()));
            ++i;
        }
        n = n.nextSibling();
    }

    xmlFile->close();


    delete xmlFile;

}

void Settings::save()
{
    QDomDocument doc;

    QFile *xmlFile = new QFile(getAppDir()+"/Settings.xml");

    if(!xmlFile->open(QIODevice::WriteOnly) || xmlFile->error() != QFile::NoError) {
        return;
    }

    QDomElement settings;
    QDomElement groups;
    QDomElement connections;

    settings = doc.createElement("settings");
    groups = doc.createElement("groups");
    connections = doc.createElement("connections");

    groups.setAttribute("main",QString::number(mainGroup));

    QDomElement n;

    for(int i=0;i<groupList.size();++i) {
        n = doc.createElement("group");
        n.appendChild(doc.createTextNode(groupList.keys().at(i)));
        if(groupList.values().at(i) == QCoreApplication::applicationDirPath())
            n.setAttribute("path","");
        else
            n.setAttribute("path",groupList.values().at(i));
        groups.appendChild(n);
    }

    for(int i=0;i<connectionList.size();++i) {
        n = doc.createElement("connection");
        n.appendChild(doc.createTextNode(connectionList.at(i).name));
        n.setAttribute("login",connectionList.at(i).login);
        n.setAttribute("password",connectionList.at(i).password);
        n.setAttribute("host",connectionList.at(i).host);
	n.setAttribute("type",QString::number((int)connectionList.at(i).type));
        connections.appendChild(n);
    }

    settings.appendChild(groups);
    settings.appendChild(connections);
    doc.appendChild(settings);

    QDomNode xmlNode = doc.createProcessingInstruction("xml",
                                "version=\"1.0\" encoding=\"UTF-8\"");

    doc.insertBefore(xmlNode, doc.firstChild());

    QTextStream t(xmlFile);

    doc.save(t,4);
}


void Settings::renameGroupAt(int index,const QString &newName)
{
    QString oldName = groupList.keys().at(index);
    QString path = groupList[oldName];

    groupList.erase(groupList.find(oldName));
    groupList.insert(newName,path);
}
