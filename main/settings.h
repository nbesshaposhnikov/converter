#ifndef SETTINGS_H
#define SETTINGS_H

#include <QString>
#include <QDomDocument>
#include <QList>
#include <QMap>
#include <QCoreApplication>
#include <QDir>

namespace Converter {
    namespace Interface {

    enum SQLType {
	QODBC,
	QPSQL,
	QSQLITE
    };

    static QString getAppDir() {
	QString appDir = QCoreApplication::applicationDirPath();

	#ifdef MAC_X
	QDir dir(appDir);
	dir.cdUp();dir.cdUp();dir.cdUp();
	appDir = dir.absolutePath();
	#endif

	return appDir;
    }

    struct SqlProperties
    {
        SqlProperties ():
            index(-1),
            name(QString()),
            login(QString()),
            password(QString()),
            host(QString()),
            type(QODBC),
            typeString(QString()),
            dbString(QString())
        {}

        SqlProperties (const SqlProperties &p):
            index(p.index),
            name(p.name),
            login(p.login),
            password(p.password),
            host(p.host),
            type(p.type),
            typeString(p.typeString),
            dbString(p.dbString)
        {}

	SqlProperties (int i,QString n,QString l, QString p, QString h,SQLType t):
            index(i),
            name(n),
            login(l),
            password(p),
            host(h),
            type(t),
            typeString(getTypeString(t))
        {
            if(type==QODBC)
                dbString = "DRIVER={SQL SERVER};SERVER=" + host + ";";
            else if(type==QSQLITE) {
                dbString = getAppDir()+"/"+host+".sqlite";
            }
        }

        SqlProperties &operator =(const SqlProperties &p) {
            index = p.index;
            name = p.name;
            login = p.login;
            password = p.password;
            host = p.host;
            type = p.type;
            typeString = p.typeString;
            dbString = p.dbString;

            return *this;
        }

        bool operator == (const SqlProperties& p) {
            return (name==p.name&&login==p.login&&password==p.password&&host==p.host&&dbString == p.dbString&&type==p.type);
        }

        QString toString() const {
            return login+"\t"+password+"\t"+host+"\t"+dbString+"\t"+typeString;
        }

	QString getTypeString(SQLType t) {
	    switch(t) {
	    case QODBC:return "QODBC";break;
	    case QPSQL:return "QPSQL";break;
	    case QSQLITE:return "QSQLITE";break;
	    }
	    return QString();
	}

	QString getType() {
	    switch(type) {
	    case QODBC:return "MSSQL";break;
	    case QPSQL:return "PostgreSQL";break;
	    case QSQLITE:return "SQLITE";break;
	    }
	    return QString();
	}

	void setType(SQLType t) {
	    type=t;
	    typeString = getTypeString(t);

	    if(type==QODBC)
            dbString = "DRIVER={SQL SERVER};SERVER=" + host + ";";
	    else if(type==QSQLITE) {
            dbString = getAppDir()+"/"+host+".sqlite";
	    }
	    else
            dbString = "";
	}

        int index;
        QString name;
        QString login;
        QString password;
        QString host;
        SQLType type;
        QString typeString;

        QString dbString;

    };

    class Settings
    {
    private:
        int mainGroup;
        QMap <QString,QString> groupList;
        QList<SqlProperties> connectionList;

    public:
        Settings():
            mainGroup(0),
            groupList(QMap <QString,QString> ()),
            connectionList(QList<SqlProperties> ())
        {}

        Settings(const Settings &s):
            mainGroup(s.mainGroup),
            groupList(s.groupList),
            connectionList(s.connectionList)
        {}

        Settings &operator =(const Settings &s) {
            mainGroup = s.mainGroup;
            groupList = s.groupList;
            connectionList = s.connectionList;

            return *this;
        }

        //Load and save
        void load();
        void save();
        //Get elements of document
        int groupNum() const{
            return groupList.size();
        }

        void addGroup(const QString &name,const QString &path) {
            groupList.insert(name,path);
        }

        void addConnection(const QString &name) {
	    connectionList.append(SqlProperties(connectionList.size(),name,QString(),QString(),QString(),QODBC));
        }

        void removeGroupAt(int index) {
            groupList.erase(groupList.find(groupList.keys().at(index)));
        }

        void removeConnectionAt(int index) {
            connectionList.takeAt(index);
            //reindexing
            for(int i=0;i<connectionList.size();++i)
                connectionList[i].index = i;

        }

        void renameGroupAt(int index,const QString &newName);

        void changePathAt(int index,const QString &newPath) {
            groupList[groupList.keys().at(index)]  = newPath;
        }

        QString groupNameAt(const int i) const {
            return groupList.keys().at(i);
        }

        QList <QString> getNameList() const {
            return groupList.keys();
        }

        QString groupPathAt(const int i) const {
            return groupList.values().at(i);
        }

        void setPathAt(int i,const QString &newPath) {
            groupList[groupList.keys()[i]] = newPath;
        }

        int connectionNum() const {
            return connectionList.size();
        }

        const SqlProperties &sqlAt(const int i) const{
            return connectionList.at(i);
        }

        SqlProperties &sqlAt(const int i){
            return connectionList[i];
        }

        int &getMainGroup() {
            return mainGroup;
        }

        void setMainGroup(const QString &name) {
            for(int i=0;i<groupList.size();++i) {
                if(groupList.keys().at(i) == name)
                    mainGroup = i;
            }

        }


        int getMainGroup() const{
            return mainGroup;
        }
    };

    }//end namespace Converter::Interface
}//end namespace Converter

#endif // SETTINGS_H
