﻿#include "connectionlistdelegate.h"

#include <QtWidgets>
#include <QPainter>
using namespace Converter::Interface;

ConnectionListDelegate::ConnectionListDelegate(QObject *parent) :
    QItemDelegate(parent),
    settings(0)
{
}

void ConnectionListDelegate::setSettings(Settings *mSet)
{
//    if(group==mGroup)
//        return;

    settings=mSet;

}


//! [1]
QWidget *ConnectionListDelegate::createEditor(QWidget *parent,
    const QStyleOptionViewItem & option ,
    const QModelIndex & index ) const
{
    if(settings==0)
	return QItemDelegate::createEditor(parent, option, index);;

    switch (index.column()) {
    case 4:
    {
	QComboBox *editor = new QComboBox(parent);
	QStringList list;

	list << "MSSQL";
	list << "PostgreSQL";
	list << "SQLITE";

    #ifndef MAC_X

    #endif

	editor->addItems(list);
	editor->setCurrentIndex(settings->sqlAt(index.row()).type);
	return editor;
    }
    default:return QItemDelegate::createEditor(parent, option, index);
    }
}
//! [1]

//! [2]
void ConnectionListDelegate::setEditorData(QWidget *editor,
				    const QModelIndex &index) const
{
    if(settings==0) {
	QItemDelegate::setEditorData(editor, index);
	return;
    }

    switch (index.column()) {
    case 4:
    {
	QComboBox *comboBox = qobject_cast<QComboBox *>(editor);

	comboBox->setCurrentIndex(index.data().toInt());
    }

    default: QItemDelegate::setEditorData(editor, index);
    }

}
//! [2]

//! [3]
void ConnectionListDelegate::setModelData(QWidget *editor, QAbstractItemModel *model,
				   const QModelIndex &index) const
{
    if(settings==0) {
	QItemDelegate::setModelData(editor, model, index);
	return;
    }

    switch (index.column()) {
    case 4:
    {
	QComboBox *comboBox = qobject_cast<QComboBox *>(editor);
	model->setData(index,comboBox->currentIndex());
	break;
    }

    default: QItemDelegate::setModelData(editor, model, index);
    }

    //model->setData(index, qVariantFromValue(starEditor->starRating()));
//    model->setData(index, value, Qt::EditRole);

//    if (qVariantCanConvert<StarRating>(index.data())) {
//        StarEditor *starEditor = qobject_cast<StarEditor *>(editor);
//        model->setData(index, qVariantFromValue(starEditor->starRating()));
//    } else {
       // QItemDelegate::setModelData(editor, model, index);
    //}
}
//! [3]

//! [4]
void ConnectionListDelegate::updateEditorGeometry(QWidget *editor,
    const QStyleOptionViewItem &option, const QModelIndex &/* index */) const
{
    editor->setGeometry(option.rect);
}



