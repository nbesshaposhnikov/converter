#ifndef CONNECTIONLISTDELEGATE_H
#define CONNECTIONLISTDELEGATE_H

#include <QItemDelegate>

#include "../settings.h"

namespace Converter {
    namespace Interface {

    class ConnectionListDelegate : public QItemDelegate
    {
	Q_OBJECT
    public:
	ConnectionListDelegate(QObject *parent = 0);


	QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option,
			      const QModelIndex &index) const;

	void setEditorData(QWidget *editor, const QModelIndex &index) const;
	void setModelData(QWidget *editor, QAbstractItemModel *model,
			  const QModelIndex &index) const;

	void updateEditorGeometry(QWidget *editor,
	    const QStyleOptionViewItem &option, const QModelIndex &index) const;

	void setSettings(Settings *mSet);
    private:
	Settings *settings;

    public slots:


    };

    }//end namespace Converter::Interface
}//end namespace Converter

#endif // CONNECTIONLISTDELEGATE_H
