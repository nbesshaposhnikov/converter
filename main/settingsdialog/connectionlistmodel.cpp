#include "connectionlistmodel.h"

using namespace Converter::Interface;

ConnectionListModel::ConnectionListModel(QObject *parent) :
    QAbstractTableModel(parent),
    settings(0)
{
}

int ConnectionListModel::rowCount (const QModelIndex &parent) const
{
    return (settings != 0)?settings->connectionNum():0;
}

int ConnectionListModel::columnCount ( const QModelIndex & parent) const
{
    return (settings != 0)?5:0;
}

void ConnectionListModel::setSettings(Settings *newSet)
{
//    if(group == mGroup)
//        return;
    beginResetModel();
    settings = newSet;
    endResetModel();
}


QVariant ConnectionListModel::data ( const QModelIndex & index, int role) const
{
    if (index.row() < 0 || index.column() < 0)
        return QVariant();

    switch (role) {
    case Qt::DisplayRole: return getColumnData(index);break;
    case Qt::EditRole: return getColumnData(index);break;
    case Qt::DecorationRole:
    default:
        return QVariant();
    }
}

bool ConnectionListModel::setData ( const QModelIndex & index, const QVariant & value, int role)
{
    if (role == Qt::EditRole) {
       return setColumnData(index,value);
    }

    return false;
}

QVariant ConnectionListModel::getColumnData(const QModelIndex & index) const
{
    switch(index.column()){
    case 0: return settings->sqlAt(index.row()).name;
    case 1: return settings->sqlAt(index.row()).login;
    case 2: return settings->sqlAt(index.row()).password;
    case 3: return settings->sqlAt(index.row()).host;
    case 4: return settings->sqlAt(index.row()).getType();
    default: return QVariant();
    }

    return QVariant();
}

bool ConnectionListModel::setColumnData(const QModelIndex & index,const QVariant & value)
{
    switch(index.column()){
    case 0:
        settings->sqlAt(index.row()).name = value.toString();
        return true;
    case 1:
        settings->sqlAt(index.row()).login = value.toString();
        return true;
    case 2:
        settings->sqlAt(index.row()).password = value.toString();
        return true;
    case 3:
        settings->sqlAt(index.row()).host = value.toString();
        return true;
    case 4:
	settings->sqlAt(index.row()).setType((SQLType)value.toInt());
	return true;
    }
    return false;
}

QVariant ConnectionListModel::headerData ( int section, Qt::Orientation orientation, int role) const
{
    if (role == Qt::DisplayRole && orientation == Qt::Horizontal) {
        switch (section) {
        case 0: return tr("Имя"); break;
        case 1: return tr("Логин"); break;
        case 2: return tr("Пароль"); break;
        case 3: return tr("Хост"); break;
        case 4: return tr("Тип БД"); break;
        }
    }
    return QVariant();
}

Qt::ItemFlags ConnectionListModel::flags ( const QModelIndex & index ) const
{
    if (!index.isValid())
        return 0;

    return Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsEditable;

}
