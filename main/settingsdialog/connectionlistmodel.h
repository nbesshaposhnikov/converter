#ifndef CONNECTIONLISTMODEL_H
#define CONNECTIONLISTMODEL_H

#include <QAbstractTableModel>
#include "../settings.h"

namespace Converter {
    namespace Interface {


    class ConnectionListModel : public QAbstractTableModel
    {
        Q_OBJECT
    public:
        explicit ConnectionListModel(QObject *parent = 0);

        void setSettings(Settings *newSet);

    private:
        int rowCount ( const QModelIndex & parent = QModelIndex() ) const;
        int columnCount ( const QModelIndex & parent = QModelIndex() ) const;
        QVariant data ( const QModelIndex & index, int role = Qt::DisplayRole ) const;
        QVariant headerData ( int section, Qt::Orientation orientation, int role = Qt::DisplayRole ) const;
        bool setData ( const QModelIndex & index, const QVariant & value, int role = Qt::EditRole );
        Qt::ItemFlags flags ( const QModelIndex & index ) const;

        //Get Column data
        QVariant getColumnData( const QModelIndex & index) const;
        //Set
        bool setColumnData(const QModelIndex & index,const QVariant & value);


        Settings *settings;
    signals:

    public slots:

    };

    }//end namespace Converter::Interface
}//end namespace Converter
#endif // CONNECTIONLISTMODEL_H
