#include "globalgrouplistmodel.h"

using namespace Converter::Interface;

GlobalGroupListModel::GlobalGroupListModel(QObject *parent) :
    QAbstractTableModel(parent),
    settings(0)
{}

int GlobalGroupListModel::rowCount (const QModelIndex &parent) const
{
    return (settings != 0)?settings->groupNum():0;
}

int GlobalGroupListModel::columnCount ( const QModelIndex & parent) const
{
    return (settings != 0)?2:0;
}

void GlobalGroupListModel::setSettings(Settings *newSet)
{
//    if(group == mGroup)
//        return;
    beginResetModel();
    settings = newSet;
    endResetModel();
}


QVariant GlobalGroupListModel::data ( const QModelIndex & index, int role) const
{
    if (index.row() < 0 || index.column() < 0)
        return QVariant();

    switch (role) {
    case Qt::DisplayRole: return getColumnData(index);break;
    case Qt::EditRole: return getColumnData(index);break;
    case Qt::DecorationRole:
    default:
        return QVariant();
    }
}

bool GlobalGroupListModel::setData ( const QModelIndex & index, const QVariant & value, int role)
{
    if (role == Qt::EditRole) {
       return setColumnData(index,value);
    }

    return false;
}

QVariant GlobalGroupListModel::getColumnData(const QModelIndex & index) const
{
    switch(index.column()){
    case 0: return settings->groupNameAt(index.row());
    case 1: return settings->groupPathAt(index.row());
    default: return QVariant();
    }

    return QVariant();
}

bool GlobalGroupListModel::setColumnData(const QModelIndex & index,const QVariant & value)
{
    switch(index.column()){
    case 0:
    {
        QString mainName = settings->groupNameAt(settings->getMainGroup());

        settings->renameGroupAt(index.row(),value.toString());

        settings->setMainGroup(mainName);
        emit groupNameChanged();
        return true;
    }
    case 1:
    {
        settings->setPathAt(index.row(),value.toString());
        return true;
    }

    }
    return false;
}

QVariant GlobalGroupListModel::headerData ( int section, Qt::Orientation orientation, int role) const
{
    if (role == Qt::DisplayRole && orientation == Qt::Horizontal) {
        switch (section) {
        case 0: return tr("Имя"); break;
        case 1: return tr("Путь"); break;
        }
    }
    return QVariant();
}

Qt::ItemFlags GlobalGroupListModel::flags ( const QModelIndex & index ) const
{
    if (!index.isValid())
        return 0;

    return Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsEditable;

}

