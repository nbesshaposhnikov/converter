#include "settingsdialog.h"
#include "ui_settingsdialog.h"
#include <QInputDialog>
#include <QFileDialog>
#include <QtSql>
#include <QMessageBox>

using namespace Converter;
using namespace Converter::Interface;

SettingsDialog::SettingsDialog(const Interface::Settings &s,QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SettingsDialog),
    settings(s)
{
    ui->setupUi(this);
    groupListModel= new GlobalGroupListModel(this);
    groupListModel->setSettings(&settings);

    connectionListModel = new ConnectionListModel(this);
    connectionListModel->setSettings(&settings);

    connectionListDelegate = new ConnectionListDelegate(this);
    connectionListDelegate->setSettings(&settings);

    ui->groupList->setModel(groupListModel);
    ui->groupList->setColumnWidth(1,400);

    ui->connectionListView->setModel(connectionListModel);
    ui->connectionListView->setItemDelegate(connectionListDelegate);
    //Groups
    loadGroupList();
    connect(ui->addGroup,SIGNAL(clicked()),this,SLOT(onGroupAdd()));
    connect(ui->removeGroup,SIGNAL(clicked()),this,SLOT(onGroupRemove()));
    connect(ui->editPath,SIGNAL(clicked()),this,SLOT(onEditPath()));
    connect(ui->mainGroup,SIGNAL(currentIndexChanged(int)),this,SLOT(onMainGroupChange(int)));
    connect(groupListModel,SIGNAL(groupNameChanged()),this,SLOT(loadGroupList()));
    //Connections
    connect(ui->addConnection,SIGNAL(clicked()),this,SLOT(onConnectionAdd()));
    connect(ui->removeConnection,SIGNAL(clicked()),this,SLOT(onConnectionRemove()));
    connect(ui->testConnection,SIGNAL(clicked()),this,SLOT(onTestConnection()));
}

SettingsDialog::~SettingsDialog()
{
    delete ui;
    delete groupListModel;
    delete connectionListModel;
}


void SettingsDialog::loadGroupList()
{
    ui->mainGroup->clear();

    ui->mainGroup->addItems(settings.getNameList());
    ui->mainGroup->setCurrentIndex(settings.getMainGroup());
}

void SettingsDialog::onMainGroupChange(int index)
{
    if(index == -1)
        return;

    settings.getMainGroup() = index;
}

void SettingsDialog::onGroupAdd()
{
    bool ok;
    QString name = QInputDialog::getText(this, tr("Введите название группы"),
                                        tr("Название :"), QLineEdit::Normal,
                                         QString(), &ok);

    QString mainName = settings.groupNameAt(settings.getMainGroup());

    if(ok && !name.trimmed().isEmpty()) {
        QString folder = QDir(QFileDialog::getExistingDirectory(this,tr("Выберите директорию"))).absolutePath();
        if(!folder.isEmpty()) {
            QDir(folder).mkdir(name);
            QDir(folder+"/"+name).mkdir("Headers");
            settings.addGroup(name,folder);
        }
    }

    settings.setMainGroup(mainName);
    int groupIndex  = settings.getMainGroup();

    groupListModel->setSettings(&settings);
    loadGroupList();


    settings.getMainGroup() = groupIndex;
    ui->mainGroup->setCurrentIndex(settings.getMainGroup());
}

void SettingsDialog::onGroupRemove()
{
    int index = ui->groupList->currentIndex().row();

    if(index==-1)
        return;

    if(settings.groupNum() == 1) return;

    if(index == settings.getMainGroup())
        settings.getMainGroup() = 0;

    settings.removeGroupAt(index);

    groupListModel->setSettings(&settings);
    loadGroupList();
}

void SettingsDialog::onEditPath()
{
    int index = ui->groupList->currentIndex().row();

    if(index==-1)
        return;

    QString folder = QFileDialog::getExistingDirectory(this,tr("Выберите директорию"));

    if(!folder.isEmpty()) {
        QDir(folder).mkdir(settings.getNameList().at(index));
        QDir(folder+"/"+settings.getNameList().at(index)).mkdir("Headers");
        settings.setPathAt(index,folder);
    }

    groupListModel->setSettings(&settings);
}

void SettingsDialog::onConnectionAdd()
{
    bool ok;
    QString name = QInputDialog::getText(this, tr("Введите название соединения"),
                                        tr("Название :"), QLineEdit::Normal,
                                         QString(), &ok);

    if(ok && !name.trimmed().isEmpty()) {
        settings.addConnection(name);
    }

    connectionListModel->setSettings(&settings);
}

void SettingsDialog::onConnectionRemove()
{
    int index = ui->connectionListView->currentIndex().row();

    if(index==-1)
        return;

    settings.removeConnectionAt(index);

    connectionListModel->setSettings(&settings);
}
#include <QDebug>
void SettingsDialog::onTestConnection()
{
    int index = ui->connectionListView->currentIndex().row();

    if(index==-1)
        return;

    if(settings.sqlAt(index) == SqlProperties())
        return;

    SQLManager manager(this,settings.sqlAt(index));

    manager.testConnection();


}
