#ifndef SETTINGSDIALOG_H
#define SETTINGSDIALOG_H

#include <QDialog>
#include "../settings.h"
#include "../sqlmanager.h"
#include "globalgrouplistmodel.h"
#include "connectionlistmodel.h"
#include "connectionlistdelegate.h"

namespace Ui {
class SettingsDialog;
}
namespace Converter {

    class SettingsDialog : public QDialog
    {
        Q_OBJECT

    public:
        explicit SettingsDialog(const Interface::Settings &s,QWidget *parent = 0);
        ~SettingsDialog();

        Interface::Settings getSettings() {
            return settings;
        }

    private:
        Ui::SettingsDialog *ui;
        Interface::Settings settings;

        Interface::GlobalGroupListModel *groupListModel;
        Interface::ConnectionListModel *connectionListModel;
	Interface::ConnectionListDelegate *connectionListDelegate;
    public slots:
        void onGroupAdd();
        void onGroupRemove();
        void onConnectionAdd();
        void onConnectionRemove();
        void onEditPath();
        void loadGroupList();
        void onMainGroupChange(int index);
        void onTestConnection();
    };

}
#endif // SETTINGSDIALOG_H
