#include "sqlmanager.h"
#include <QMessageBox>
#include <QFile>
#include <QDir>

using namespace Converter::Interface;

void SQLManager::testConnection()
{
{
    QSqlDatabase db = QSqlDatabase::addDatabase(properties.typeString,"CheckState");//properties.typeString="MSSQL" for MSSQL
    db.setUserName(properties.login);
    db.setPassword(properties.password);
    db.setHostName(properties.host);
    db.setDatabaseName(properties.dbString);//properties.dbString="DRIVER={SQL SERVER};SERVER=ip;PORT=port;" for MSSQL

    //db.setDatabaseName("DRIVER={FreeTDS};SERVER=192.168.1.2;PORT=1433;TDS_Version=8.0");

    if(!db.open())
        QMessageBox::critical(parent,QObject::tr("Ошибка"),
                    QObject::tr("Не установлено соединение с бд ")+properties.name+
                    QObject::tr("!\nОшибка: ")+ db.lastError().text());
    else
        QMessageBox::information(parent,QObject::tr("Инфо"),
                    QObject::tr("Соедние установлено!"));

    lastErrorString = db.lastError().text();

    db.close();
}
    QSqlDatabase::removeDatabase("CheckState");

}

bool SQLManager::databaseExists(const QString &name)
{
    if(properties.type ==QSQLITE) {
    if(QFile::exists(getAppDir()+"/"+name+".sqlite"))
        return true;
    return false;
    }

    bool res;

    {

    QSqlDatabase db = QSqlDatabase::addDatabase(properties.typeString,"CheckState");
    db.setUserName(properties.login);
    db.setPassword(properties.password);
    db.setHostName(properties.host);
    db.setDatabaseName(properties.dbString);

    if(!db.open()) {
        QMessageBox::critical(parent,QObject::tr("Ошибка"),
                      QObject::tr("Не установлено соединение с бд:")+properties.name);
        lastErrorString = db.lastError().text();
    }

    QSqlQuery q;

    if(properties.type == QODBC) {
        q = db.exec("SELECT count(1) FROM sysdatabases WHERE name='"+name+"'");

    } else if(properties.type == QPSQL) {
        q = db.exec("select count(1) from pg_catalog.pg_database where datname ='"+name.toLower()+"'");
    }

    q.next();
    res = false;

    if(q.value(0).toInt())
        res = true;

    q.finish();
    db.close();
    }

    QSqlDatabase::removeDatabase("CheckState");

    return res;

}

void SQLManager::removeDatabase(const QString &name)
{

    if(properties.type ==QSQLITE) {
        if(QFile::exists(getAppDir()+"/"+name+".sqlite"))
            QFile::remove(getAppDir()+"/"+name+".sqlite");
        return;
    }

    QSqlDatabase db = QSqlDatabase::addDatabase(properties.typeString);
    db.setUserName(properties.login);
    db.setPassword(properties.password);
    db.setHostName(properties.host);
    db.setDatabaseName(properties.dbString);

    if(!db.open()) {
        QMessageBox::critical(parent,QObject::tr("Ошибка"),
                      QObject::tr("Не установлено соединение с бд:")+properties.name);
        lastErrorString = db.lastError().text();
    }



    if(properties.type == QODBC) {
        db.exec("IF EXISTS (SELECT * FROM sysdatabases WHERE name='"+name+"')"+
            "DROP DATABASE "+name);

    } else if(properties.type == QPSQL) {
        QSqlQuery q = db.exec("select count(1) from pg_catalog.pg_database where datname ='"+name.toLower()+"'");
        q.next();
        if(q.value(0).toInt())
            db.exec("DROP DATABASE "+name.toLower());
        q.finish();
    }

    db.close();

}


QStringList SQLManager::getDatabaseList()
{
    QStringList res;

    if(properties.type == QSQLITE) {
    QDir dir= QDir(getAppDir(),"*.sqlite",QDir::Name,QDir::Files);

    QFileInfoList fileList = dir.entryInfoList();

        for(int i=0;i<fileList.size();++i)
            res << fileList.at(i).baseName();

        return res;
    }

    {

    QSqlDatabase db = QSqlDatabase::addDatabase(properties.typeString,"CheckState");
    db.setUserName(properties.login);
    db.setPassword(properties.password);
    db.setHostName(properties.host);
    db.setDatabaseName(properties.dbString);

    if(!db.open()) {
        QMessageBox::critical(parent,QObject::tr("Ошибка"),
                      QObject::tr("Не установлено соединение с бд:")+properties.name);
        lastErrorString = db.lastError().text();
    }

    QSqlQuery q;

    if(properties.type == QODBC) {
        q = db.exec("SELECT name FROM sysdatabases");

    } else if(properties.type == QPSQL) {
        q = db.exec("select datname from pg_catalog.pg_database");
    }

    while(q.next()) {
        res<<q.value(0).toString();
    }
    q.finish();
    db.close();
    }

    QSqlDatabase::removeDatabase("CheckState");

    return res;

}
