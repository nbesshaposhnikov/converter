#ifndef SQLMANAGER_H
#define SQLMANAGER_H

#include <QtSql>
#include "settings.h"

namespace Converter {
    namespace Interface {

    class SQLManager
    {
    private:
        QWidget *parent;
        SqlProperties properties;
        QString lastErrorString;
    public:
        SQLManager():
            parent(0),
            properties(SqlProperties()),
            lastErrorString(QString())
        {}
        SQLManager(QWidget *parent,const SqlProperties &props):
            parent(parent),
            properties(props),
            lastErrorString(QString())
        {}

        void testConnection();
        bool databaseExists(const QString &name);
        void removeDatabase(const QString &name);
        QStringList getDatabaseList();

        QString lastError() {
            return lastErrorString;
        }
        };

    }
}
#endif // SQLMANAGER_H
